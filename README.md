
## Usage

1. Clone/Download a repo.
2. Copy `.env.example` file to `.env` & Setup your environment variables
3. Run `composer install`
4. Generate application key by running `php artisan key:generate`
5. Run `php artisan migrate --seed`
6. Run `php artisan serve`
7. Login account:

# dev
```bash
npm run watch

```
