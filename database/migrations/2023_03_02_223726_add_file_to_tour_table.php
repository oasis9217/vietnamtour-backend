<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFileToTourTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('tours','map_image'))
        {
            Schema::table('tours', function (Blueprint $table) {
                $table->string('map_image')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('tours','map_image'))
        {
            Schema::table('tours', function (Blueprint $table) {
                $table->dropColumn('map_image');
            });
        }
    }
}
