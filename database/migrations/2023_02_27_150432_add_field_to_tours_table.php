<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldToToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('tours','is_highlight'))
        {
            Schema::table('tours', function (Blueprint $table) {
                $table->boolean('is_highlight')->default(0);
            });
        }
        if (!Schema::hasColumn('tours','is_similar'))
        {
            Schema::table('tours', function (Blueprint $table) {
                $table->boolean('is_similar')->default(0);
            });
        }

        if (!Schema::hasColumn('tours','thumbnail'))
        {
            Schema::table('tours', function (Blueprint $table) {
                $table->string('thumbnail')->nullable();
            });
        }

        if (!Schema::hasColumn('tours','header_image'))
        {
            Schema::table('tours', function (Blueprint $table) {
                $table->string('header_image')->nullable();
            });
        }
        if (!Schema::hasColumn('tours','is_show_in_home'))
        {
            Schema::table('tours', function (Blueprint $table) {
                $table->boolean('is_show_in_home')->default(0);
            });
        }

        if (!Schema::hasColumn('tours','is_show_in_category_home'))
        {
            Schema::table('tours', function (Blueprint $table) {
                $table->boolean('is_show_in_category_home')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('tours','is_highlight'))
        {
            Schema::table('tours', function (Blueprint $table) {
                $table->dropColumn('is_highlight');
            });
        }
        if (Schema::hasColumn('tours','is_similar'))
        {
            Schema::table('tours', function (Blueprint $table) {
                $table->dropColumn('is_similar');
            });
        }

        if (Schema::hasColumn('tours','thumbnail'))
        {
            Schema::table('tours', function (Blueprint $table) {
                $table->dropColumn('thumbnail');
            });
        }

        if (Schema::hasColumn('tours','header_image'))
        {
            Schema::table('tours', function (Blueprint $table) {
                $table->dropColumn('header_image');
            });
        }
        if (Schema::hasColumn('tours','is_show_in_home'))
        {
            Schema::table('tours', function (Blueprint $table) {
                $table->dropColumn('is_show_in_home');
            });
        }

        if (Schema::hasColumn('tours','is_show_in_category_home'))
        {
            Schema::table('tours', function (Blueprint $table) {
                $table->boolean('is_show_in_category_home');
            });
        }
    }
}
