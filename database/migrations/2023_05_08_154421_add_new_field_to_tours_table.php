<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldToToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tours', function (Blueprint $table) {
            $table->string('itinerary')->nullable()->default('The trip brings you from the vibrant Hanoi to the far most area There are more things to delve into each traveling day. There are more things to delve into each traveling day.');
            $table->string('accommodation')->nullable()->default('All hotel nights (overnight on train/cruise per itinerary)');
            $table->string('meals')->nullable()->default('All breakfasts and lunches in tour days');
            $table->string('transport')->nullable()->default('Private car with A/C, internal flights');
            $table->string('travel_team')->nullable()->default('Private tour guide, private drivers');
            $table->string('experiences')->nullable()->default('10 unique travel experiences >>');
            $table->string('visa_arrangement')->nullable()->default('We`’ll help you to arrange the visa');
            $table->string('international_flights')->nullable()->default('Flights to/ from the destination');
            $table->string('travel_insurance')->nullable()->default('For the time you travel');
            $table->string('meals_not_included')->nullable()->default('Drinks and meals not mentioned');
            $table->string('personal_expenses')->nullable()->default('Shopping or personal purchases');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tours', function (Blueprint $table) {
            //
        });
    }
}
