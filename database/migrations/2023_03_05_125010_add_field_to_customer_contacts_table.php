<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldToCustomerContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_contacts', function (Blueprint $table) {
            $table->string('country')->nullable();
            $table->boolean('international_flight')->nullable();
            $table->boolean('is_flexible_date')->nullable();
            $table->smallInteger('hotel_room_quantity')->default(0);
            $table->string('hotel_class')->nullable();
            $table->smallInteger('adult')->default(0);
            $table->smallInteger('child')->default(0);
            $table->smallInteger('child_baby')->default(0);
            $table->date('departure_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_contacts', function (Blueprint $table) {
            //
        });
    }
}
