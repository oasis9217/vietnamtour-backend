<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToursTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->id();
            $table->text('name')->nullable();
            $table->text('slug')->nullable();
            $table->text('desc')->nullable();
            $table->string('image')->nullable();
            $table->float('price')->nullable();
            $table->longText('content')->nullable();
            $table->boolean('is_public')->default(0);
            $table->longText('meta')->nullable();
            $table->integer('day')->nullable();
            $table->string('keyword')->nullable();
            $table->float('sort')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('tour_category', function (Blueprint $table) {
            $table->unsignedBigInteger('tour_id');
            $table->unsignedBigInteger('tour_category_id');

            $table->foreign('tour_category_id')
                ->on('tour_categories')
                ->references('id')
                ->onDelete('cascade');

            $table->foreign('tour_id')
                ->on('tours')
                ->references('id')
                ->onDelete('cascade');
        });

        Schema::create('tour_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('tour_id');
            $table->text('name')->nullable();
            $table->text('slug')->nullable();
            $table->text('desc')->nullable();
            $table->float('lat')->nullable();
            $table->float('lpg')->nullable();
            $table->longText('content')->nullable();
            $table->float('sort')->nullable();

            $table->foreign('tour_id')
                ->on('tours')
                ->references('id')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tour_category');
        Schema::drop('tour_details');
        Schema::drop('tours');
    }
}
