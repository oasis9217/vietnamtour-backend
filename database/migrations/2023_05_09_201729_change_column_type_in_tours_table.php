<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnTypeInToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tours', function (Blueprint $table) {
            $table->text('itinerary')->change();
            $table->text('accommodation')->change();
            $table->text('meals')->change();
            $table->text('transport')->change();
            $table->text('travel_team')->change();
            $table->text('experiences')->change();
            $table->text('visa_arrangement')->change();
            $table->text('international_flights')->change();
            $table->text('travel_insurance')->change();
            $table->text('meals_not_included')->change();
            $table->text('personal_expenses')->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tours', function (Blueprint $table) {
            //
        });
    }
}
