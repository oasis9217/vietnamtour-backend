<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldToPostsTable extends Migration
{
    public function up()
    {
        if (!Schema::hasColumn('posts','is_inspiration'))
        {
            Schema::table('posts', function (Blueprint $table) {
                $table->boolean('is_inspiration')->default(0);
            });
        }
        if (!Schema::hasColumn('posts','is_travel_article'))
        {
            Schema::table('posts', function (Blueprint $table) {
                $table->boolean('is_travel_article')->default(0);
            });
        }

        if (!Schema::hasColumn('posts','thumbnail'))
        {
            Schema::table('posts', function (Blueprint $table) {
                $table->string('thumbnail')->nullable();
            });
        }

        if (!Schema::hasColumn('posts','header_image'))
        {
            Schema::table('posts', function (Blueprint $table) {
                $table->string('header_image')->nullable();
            });
        }
        if (!Schema::hasColumn('posts','is_show_in_home'))
        {
            Schema::table('posts', function (Blueprint $table) {
                $table->boolean('is_show_in_home')->default(0);
            });
        }

        if (!Schema::hasColumn('posts','is_in_first_home'))
        {
            Schema::table('posts', function (Blueprint $table) {
                $table->boolean('is_in_first_home')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('posts','is_inspiration'))
        {
            Schema::table('posts', function (Blueprint $table) {
                $table->dropColumn('is_inspiration');
            });
        }
        if (Schema::hasColumn('posts','is_travel_article'))
        {
            Schema::table('posts', function (Blueprint $table) {
                $table->dropColumn('is_travel_article');
            });
        }

        if (Schema::hasColumn('posts','thumbnail'))
        {
            Schema::table('posts', function (Blueprint $table) {
                $table->dropColumn('thumbnail');
            });
        }

        if (Schema::hasColumn('posts','header_image'))
        {
            Schema::table('posts', function (Blueprint $table) {
                $table->dropColumn('header_image');
            });
        }
        if (Schema::hasColumn('posts','is_show_in_home'))
        {
            Schema::table('posts', function (Blueprint $table) {
                $table->dropColumn('is_show_in_home');
            });
        }

        if (Schema::hasColumn('posts','is_in_first_home'))
        {
            Schema::table('posts', function (Blueprint $table) {
                $table->boolean('is_in_first_home');
            });
        }
    }
}
