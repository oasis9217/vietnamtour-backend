<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\TourCategory;
use App\Models\Tours;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class TourCategoryDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            "Travel Styles",
            "Multi Country Tours",

        ];


        foreach ( $categories as $category)
        {
           $cate =  TourCategory::updateOrCreate([
               'name'=>$category
            ]);
            $cate->updateSlug($category);
        }


//        $categoryFirstId = TourCategory::first()->id;
//        $categoriesSub = [
//            "Vietnam Family Tours",
//            "Vietnam Luxury Tours",
//            "Vietnam Culture Tours",
//            "Vietnam Honeymoon Tours",
//            "Vietnam Beach & Island Tours",
//            "Vietnam Trekking Adventures"
//        ];
//
//        foreach ( $categoriesSub as $category)
//        {
//           $cate = TourCategory::updateOrCreate([
//                'name'=>$category,
//                'parent_id' => $categoryFirstId
//            ]);
//            $cate->createSlug($category);
//        }
        Post::all()->each(function ($item){
            $desc = str_replace(['<p>', '</p>'], '', $item->desc);
            $desc = strip_tags($desc);
            $item->update(['desc'=>$desc]);
        });


//       $a =  \App\Models\Tours::all();
//
//       foreach ($a as $b)
//       {
//           $b->update([
//               'accommodation' => 'All hotel nights (overnight on train/cruise per itinerary)',
//               'meals' => 'All breakfasts and lunches in tour days',
//               'transport' => 'Private car with A/C, internal flights',
//               'travel_team' => 'Private tour guide, private drivers',
//               'experiences' => '10 unique travel experiences >>',
//               'visa_arrangement' => 'We`’ll help you to arrange the visa',
//               'international_flights' => 'Flights to/ from the destination',
//               'travel_insurance' => 'For the time you travel',
//               'meals_not_included' => 'Drinks and meals not mentioned',
//               'personal_expenses'=>'Shopping or personal purchases'
//           ]);
//       }
    }
}
