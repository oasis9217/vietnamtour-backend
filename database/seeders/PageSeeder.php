<?php

namespace Database\Seeders;

use App\Models\Page;
use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages = [
            'Term',
            'Privacy Policy',
            'About us',
            'Contact us',
            'For Travel Agent'
        ];

        foreach ($pages as $item) {
            $page = Page::updateOrCreate(['name' => $item]);
        }

    }
}
