<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\PostCategory;
use App\Models\TourCategory;
use App\Models\Tours;
use Illuminate\Database\Seeder;
use PhpOffice\PhpSpreadsheet\Calculation\Category;

class SyncSlugDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = Post::all();
        $categories = TourCategory::all();
        $categoryPost = PostCategory::all();
        $tours = Tours::all();

        foreach ($posts as $item)
        {
            $item->createSlug();
        }
        foreach ($categories as $item)
        {
            $item->createSlug();
        }
        foreach ($categoryPost as $item)
        {
            $item->createSlug();
        }

        foreach ($tours as $item)
        {
            $item->createSlug();
        }
    }
}
