<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::updateOrCreate(
            ['name' => 'footer'],
            ['content' =>
                '<p>&nbsp;</p><figure class="table"><table><tbody><tr><td><p>OUR DESTINATIONS</p><p><a href="">Vietnam &gt;</a></p><p>&nbsp;<a href="">Cambodia &gt;</a>&nbsp;</p><p><a href="">Laos &gt;</a>&nbsp;</p><p><a href="">Thailand &gt;</a>&nbsp;</p><p><a href="">China &gt;</a>&nbsp;</p><p><a href="">Multi Countries Tours &gt;</a></p></td><td><p>OUR VIETNAM TOURS</p><p><a href="">Vietnam Culture Tours &gt;</a>&nbsp;</p><p><a href="">Vietnam Luxury Tours &gt;</a>&nbsp;</p><p><a href="">Vietnam Beach &amp; Island Tours &gt;</a>&nbsp;</p><p><a href="">Vietnam Family Tours &gt;</a>&nbsp;</p><p><a href="">Vietnam Honeymoon Tours &gt;</a>&nbsp;</p><p><a href="">Vietnam Daily Tours &gt;</a></p></td></tr></tbody></table></figure><p>&nbsp;</p><p>&nbsp;</p>'
            ]
        );
        Setting::updateOrCreate(
            ['name' => 'multiple_tour'],
            ['content' =>
                '<p>Travel throuthe spectacular countries of Indochina peninsula or even further Thailand or China. We are here to make your Vietnam tour as well Southeast Asia tour smooth.</p><p><a href="">The highlights of Vietnam and Laos - 10 days&nbsp;</a></p><p><a href="">Vietnam and Cambodia through Mekong - 12 days&nbsp;</a></p><p><a href="">The impression of Indochina - 12 days&nbsp;</a></p><p><a href="">The waterways of Mekong and its life - 15 days&nbsp;</a></p><p><a href="">Road Adventure Laos and North Vietnam - 9 days&nbsp;</a></p>	'
            ]
        );
    }
}
