<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use App\Repositories\RoleRepository;
use App\Repositories\PermissionRepository;
use App\Repositories\UserRepository;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /** @var  PermissionRepository */
    private $permissionRepository;
    /** @var  RoleRepository */
    private $roleRepository;
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(RoleRepository $roleRepo, PermissionRepository $permissionRepo, UserRepository $userRepo)
    {
        $this->roleRepository = $roleRepo;
        $this->permissionRepository = $permissionRepo;
        $this->userRepository = $userRepo;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Artisan::call('config:clear');
        // load permission from router name
        $exitCode = Artisan::call('router:permission');

        // Supper Admin
        $roleSupperAdmin = $this->roleRepository->getModel()->updateOrCreate([
            'name' =>    Role::SUPPER_ADMIN,
            'title' => 'Supper Admin',
            'guard_name' => 'api'
        ]);

        $roleSupperAdmin = $this->roleRepository->getModel()->updateOrCreate([
            'name' =>    Role::SUPPER_ADMIN,
            'title' => 'Supper Admin',
            'guard_name' => 'api'
        ]);

     $this->roleRepository->getModel()->updateOrCreate([
            'name' =>  Role::ADMIN,
            'title' => 'Admin',
            'guard_name' => 'api'
        ]);

        if (!$this->userRepository->getModel()->whereEmail('admin@admin.com')->exists())
        {
            $userSupperAdmin = $this->userRepository->getModel()->updateOrCreate([
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'password' => Hash::make('123456789'),
            ]);

            $userSupperAdmin->assignRole($roleSupperAdmin);
        }

    }
}
