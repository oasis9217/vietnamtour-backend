<?php

namespace Database\Seeders;

use App\Models\PostCategory;
use App\Models\TourCategory;
use Illuminate\Database\Seeder;

class PostCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            "Travel Inspiration",
        ];

        foreach ( $categories as $category)
        {
          $cate =   PostCategory::updateOrCreate([
                'name'=>$category
            ]);
            $cate->createSlug($category);
        }

    }
}
