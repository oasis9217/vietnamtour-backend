<?php

return array (
  'singular' => 'Cài đặt',
  'plural' => 'Cài đặt',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Tên',
    'content' => 'Nội dung',
    'created_at' => 'Ngày tạo',
    'updated_at' => 'Ngày cập nhật',
  ),
);
