<?php

return array (
  'singular' => 'Thể loại bài viết',
  'plural' => 'Thể loại bài viết',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Tên',
    'slug' => 'Slug',
    'created_at' => 'Ngày tạo',
    'updated_at' => 'Ngày cập nhật',
  ),
);
