<?php

return array (
  'singular' => 'Ngày tour',
  'plural' => 'Ngày tour',
  'fields' =>
  array (
    'id' => 'Id',
    'tour_id' => 'Tour',
    'name' => 'Tên',
    'desc' => 'Mô tả',
    'lat' => 'Lat (Vĩ độ)',
    'lpg' => 'Lpg (Kinh độ)',
    'content' => 'Nội dung',
    'sort' => 'Sắp xếp',
  ),
);
