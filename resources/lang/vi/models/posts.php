<?php

return array (
  'singular' => 'Bài viết',
  'plural' => 'Bài viết',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Tên',
    'slug' => 'Slug',
    'desc' => 'Mô tả',
    'image' => 'Ảnh',
    'created_at' => 'Ngày tạo',
    'updated_at' => 'Ngày cập nhật',
    'content' => 'Nội dung',
  ),
);
