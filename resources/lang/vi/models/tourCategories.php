<?php

return array (
  'singular' => 'Thể loại Tour',
  'plural' => 'Thể loại Tour',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Tên',
    'slug' => 'Slug',
    'parent_id' => 'Parent Name',
    'sort' => 'Sắp xếp',
  ),
);
