<?php

return array (
  'singular' => 'Slide Trang Chủ',
  'plural' => 'Slide Trang Chủ',
  'fields' =>
  array (
    'id' => 'Id',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'is_public' => 'Is Public',
    'image' => 'Image',
    'content' => 'Nội dung',
  ),
);
