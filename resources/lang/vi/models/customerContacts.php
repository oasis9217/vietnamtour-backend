<?php

return array (
  'singular' => 'Thông tin khách hàng',
  'plural' => 'Thông tin khách hàng',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Tên',
    'email' => 'Email',
    'desc' => 'Mô tả',
    'phone_number' => 'Số điện thoại',
    'is_paid' => 'Đã trả phí',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
