<?php

return array (
  'singular' => 'Page',
  'plural' => 'Pages',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Tên',
    'title' => 'Tiêu đề',
    'slug' => 'Slug',
    'content' => 'Nội dung',
    'created_at' => 'Ngày tạo',
    'updated_at' => 'Ngày cập nhật',
  ),
);
