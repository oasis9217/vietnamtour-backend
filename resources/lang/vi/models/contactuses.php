<?php

return array (
  'singular' => 'Contact Us',
  'plural' => 'Contact Us',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Họ tên',
    'email' => 'Email',
    'phone' => 'Điện thoại',
    'country' => 'Nước',
      'city' => 'Thành phố',
      'social_media' => 'Link mạng xã hội',
      'subject' => 'Chủ đề',
      'special_request'=>'Yêu cầu đặc biệt',
      'gender' => 'Giới tính'
  ),
);
