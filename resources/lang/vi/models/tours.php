<?php

return array (
  'singular' => 'Tours',
  'plural' => 'Tours',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Tên',
    'slug' => 'Slug',
    'desc' => 'Mô tả',
    'image' => 'Ảnh',
    'price' => 'Giá',
    'content' => 'Nội dung',
    'is_public' => 'Được public',
    'meta' => 'Meta',
    'day' => 'Ngày',
    'keyword' => 'Keyword',
          'is_highlight' => 'Is High Light',
          'is_similar' => 'Is Similar',

  ),
);
