<?php

return [

    'add_new'      => 'Thêm mới',
    'cancel'       => 'Huỷ',
    'save'         => 'Lưu',
    'edit'         => 'Cập nhật',
    'detail'       => 'Chi tiết',
    'back'         => 'Trở về',
    'action'       => 'Action',
    'id'           => 'Id',
    'created_at'   => 'Ngày tạo',
    'updated_at'   => 'Ngày cập nhật',
    'deleted_at'   => 'Ngày xoá',
    'are_you_sure' => 'Bạn có chắc chắn muốn thực hiện?',
];
