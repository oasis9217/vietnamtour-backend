<?php

return array (
  'singular' => 'Tours',
  'plural' => 'Tours',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Name',
    'slug' => 'Slug',
    'desc' => 'Desc',
    'image' => 'Image',
    'price' => 'Price',
    'content' => 'Content',
    'is_public' => 'Is Public',
    'meta' => 'Meta',
    'day' => 'Day',
    'keyword' => 'Keyword',
      'is_highlight' => 'Is High Light',
      'is_similar' => 'Is Similar',
  ),
);
