<?php

return array (
  'singular' => 'TourCategory',
  'plural' => 'TourCategories',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'slug' => 'Slug',
    'parent_id' => 'Parent Id',
    'sort' => 'Sort',
  ),
);
