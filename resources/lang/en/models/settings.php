<?php

return array (
  'singular' => 'Setting',
  'plural' => 'Settings',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'content' => 'Content',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
