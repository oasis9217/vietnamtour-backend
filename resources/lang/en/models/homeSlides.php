<?php

return array (
  'singular' => 'HomeSlide',
  'plural' => 'HomeSlides',
  'fields' => 
  array (
    'id' => 'Id',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'is_public' => 'Is Public',
    'image' => 'Image',
    'content' => 'Content',
  ),
);
