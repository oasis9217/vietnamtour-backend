<?php

return array (
  'singular' => 'Post',
  'plural' => 'Posts',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'slug' => 'Slug',
    'desc' => 'Desc',
    'content' => 'Content',
    'image' => 'Image',
  ),
);
