<?php

return array (
  'singular' => 'TourDetail',
  'plural' => 'TourDetails',
  'fields' => 
  array (
    'id' => 'Id',
    'tour_id' => 'Tour Id',
    'name' => 'Name',
    'desc' => 'Desc',
    'lat' => 'Lat',
    'lpg' => 'Lpg',
    'content' => 'Content',
    'sort' => 'Sort',
  ),
);
