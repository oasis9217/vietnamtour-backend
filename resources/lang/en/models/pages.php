<?php

return array (
  'singular' => 'Page',
  'plural' => 'Pages',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'title' => 'Title',
    'slug' => 'Slug',
    'content' => 'Content',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
