<?php

return array (
  'singular' => 'ContactUs',
  'plural' => 'Contactuses',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'email' => 'Email',
    'phone' => 'Phone',
    'country' => 'Country',
  ),
);
