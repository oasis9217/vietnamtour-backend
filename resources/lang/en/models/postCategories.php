<?php

return array (
  'singular' => 'Post Category',
  'plural' => 'Post Categories',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Name',
    'slug' => 'Slug',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
