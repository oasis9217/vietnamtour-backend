<?php

return array (
  'singular' => 'CustomerContact',
  'plural' => 'CustomerContacts',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'email' => 'Email',
    'desc' => 'Desc',
    'phone_number' => 'Phone Number',
    'is_paid' => 'Is Paid',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
