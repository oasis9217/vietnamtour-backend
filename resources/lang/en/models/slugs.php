<?php

return array (
  'singular' => 'Slug',
  'plural' => 'Slugs',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'slugable_id' => 'Slugable Id',
    'slugable_type' => 'Slugable Type',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
