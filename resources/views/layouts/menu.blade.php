@php
    $urlAdmin=config('fast.admin_prefix');
@endphp

@can('dashboard')
    @php
        $isDashboardActive = Request::is($urlAdmin . '*dashboard*');
    @endphp
    <li class="nav-item">
        <a href="{{ route('dashboard') }}" class="nav-link {{ $isDashboardActive ? 'active' : '' }}">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>@lang('menu.dashboard')</p>
        </a>
    </li>
@endcan

{{--@if(auth()->user()->hasRole('super-admin'))--}}
{{--@can('generator_builder.index')--}}
{{--    @php--}}
{{--        $isUserActive = Request::is($urlAdmin.'*generator_builder*');--}}
{{--    @endphp--}}
    <li class="nav-item">
        <a href="{{ route('generator_builder.index') }}" class="nav-link ">
            <i class="nav-icon fas fa-coins"></i>
            <p>@lang('menu.generator_builder.title')</p>
        </a>
    </li>
{{--@endcan--}}
{{--@endif--}}

{{--@can('attendances.index')--}}
{{--    @php--}}
{{--        $isUserActive = Request::is($urlAdmin.'*attendances*');--}}
{{--    @endphp--}}

{{--    <li class="nav-item">--}}
{{--        <a href="{{ route('attendances.index') }}" class="nav-link {{ $isUserActive ? 'active' : '' }}">--}}
{{--            <i class="nav-icon fas fa-calendar-alt"></i>--}}

{{--            <p>@lang('menu.attendances.title')</p>--}}
{{--        </a>--}}
{{--    </li>--}}
{{--@endcan--}}

@canany(['users.index','roles.index','permissions.index'])
    @php
        $isUserActive = Request::is($urlAdmin.'*users*');
        $isRoleActive = Request::is($urlAdmin.'*roles*');
        $isPermissionActive = Request::is($urlAdmin.'*permissions*');
    @endphp
    <li class="nav-item {{($isUserActive||$isRoleActive||$isPermissionActive)?'menu-open':''}} ">
        <a href="#" class="nav-link">
            <i class="nav-icon fas fa-shield-virus"></i>
            <p>
                @lang('menu.user.title')
                <i class="fas fa-angle-left right"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            @can('users.index')
                <li class="nav-item">
                    <a href="{{ route('users.index') }}" class="nav-link {{ $isUserActive ? 'active' : '' }}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            @lang('menu.user.users')
                        </p>
                    </a>
                </li>
            @endcan
            @can('roles.index')
                <li class="nav-item">
                    <a href="{{ route('roles.index') }}" class="nav-link {{ $isRoleActive ? 'active' : '' }}">
                        <i class="nav-icon fas fa-user-shield"></i>
                        <p>
                            @lang('menu.user.roles')
                        </p>
                    </a>
                </li>
            @endcan
            @can('permissions.index')
                <li class="nav-item ">
                    <a href="{{ route('permissions.index') }}"
                       class="nav-link {{ $isPermissionActive ? 'active' : '' }}">
                        <i class="nav-icon fas fa-shield-alt"></i>
                        <p>
                            @lang('menu.user.permissions')
                        </p>
                    </a>
                </li>
            @endcan
        </ul>
    </li>
@endcan
{{--@can('fileUploads.index')--}}
{{--    <li class="nav-item">--}}
{{--        <a href="{{ route('fileUploads.index') }}" class="nav-link {{ Request::is('fileUploads*') ? 'active' : '' }}">--}}
{{--            <i class="nav-icon fas fa-file-alt"></i>--}}
{{--            <p>@lang('models/fileUploads.plural')</p>--}}
{{--        </a>--}}
{{--    </li>--}}
{{--@endcan--}}
@php
    $isPostCategoryActive = Request::is($urlAdmin.'/postCategories*');
    $isPostActive = Request::is($urlAdmin.'/posts*');
@endphp
<li class="nav-item {{($isPostCategoryActive||$isPostActive)?'menu-open':''}}">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-shield-virus"></i>
        <p>
            Quản lý bài viết
            <i class="fas fa-angle-left right"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="{{ route('postCategories.index') }}"
               class="nav-link {{ $isPostCategoryActive ? 'active' : '' }}">
                <i class="nav-icon fas fa-list"></i>
                <p>@lang('models/postCategories.plural')</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('posts.index') }}"
               class="nav-link {{ $isPostActive ? 'active' : '' }}">
                <i class="nav-icon fas fa-book"></i>
                <p>@lang('models/posts.plural')</p>
            </a>
        </li>
    </ul>
</li>

@php
    $isTourCategoryActive = Request::is($urlAdmin.'/tourCategories*');
    $isTourActive = Request::is($urlAdmin.'/tours*');
    $isTourDetailActive = Request::is($urlAdmin.'/tourDetail*');
@endphp
<li class="nav-item {{($isTourCategoryActive||$isTourActive|| $isTourDetailActive)?'menu-open':''}}">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-map-marked-alt"></i>
        <p>
            Quản lý tours
            <i class="fas fa-angle-left right"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="{{ route('tourCategories.index') }}"
               class="nav-link {{ $isTourCategoryActive ? 'active' : '' }}">
                <i class="nav-icon fas fa-list"></i>
                <p>@lang('models/tourCategories.plural')</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('tours.index') }}"
               class="nav-link {{ $isTourActive ? 'active' : '' }}">
                <i class="nav-icon fas fa-book"></i>
                <p>@lang('models/tours.plural')</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('tourDetails.index') }}"
               class="nav-link {{ $isTourDetailActive ? 'active' : '' }}">
                <i class="nav-icon fad fa-list-alt"></i>
                <p>@lang('models/tourDetails.plural')</p>
            </a>
        </li>
    </ul>
</li>

@php
    $isCustomerContact = Request::is($urlAdmin.'*customerContacts*');
@endphp

<li class="nav-item">
    <a href="{{ route('customerContacts.index') }}" class="nav-link {{ $isCustomerContact ? 'active' : '' }}">
        <i class="nav-icon fas fa-calendar-alt"></i>

        <p>@lang('models/customerContacts.plural')</p>
    </a>
</li>


@php
    $isSettings = Request::is($urlAdmin.'*settings*');
@endphp
<li class="nav-item">
    <a href="{{ route('settings.index') }}"
       class="nav-link {{ $isSettings ? 'active' : '' }}">
        <i class="nav-icon fas fa-cog"></i>
        <p>@lang('models/settings.plural')</p>
    </a>
</li>


@php
    $isPages = Request::is($urlAdmin.'*pages*');
@endphp
<li class="nav-item">
    <a href="{{ route('pages.index') }}"
       class="nav-link {{ $isPages ? 'active' : '' }}">
        <i class="nav-icon fas fa-pager"></i>
        <p>@lang('models/pages.plural')</p>
    </a>
</li>

@php
    $isSlugActive = Request::is($urlAdmin.'*slugs*');
@endphp
<li class="nav-item">
    <a href="{{ route('slugs.index') }}"
       class="nav-link {{ $isSlugActive ? 'active' : '' }}">
        <i class="nav-icon fas fa-link"></i>
        <p>Slug</p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('homeSlides.index') }}"
       class="nav-link {{ Request::is('homeSlides*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-coins"></i>
        <p>@lang('models/homeSlides.plural')</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('contactuses.index') }}"
       class="nav-link {{ Request::is('contactuses*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-link"></i>
        <p>@lang('models/contactuses.plural')</p>
    </a>
</li>

