<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/postCategories.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<!-- Meta Field -->
@include('meta_example')

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('meta') !!}
    {!! Form::textarea('meta', isset($mode) && $mode != 'edit' ? config('data.meta_default') : null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('Title') !!}
    {!! Form::textarea('title', null, ['class' => 'form-control']) !!}
</div>
