@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                   @lang('models/postCategories.plural')
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-primary float-right"
                       href="{{ route('postCategories.create') }}">
                         @lang('crud.add_new')
                    </a>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-body p-0 table-responsive">
                <form id="search_form" action="{{ route('postCategories.index') }}" method="get">
                    {{-- Setting --}}
                    <div class="row p-3">
                        <div class="col-sm-3">
                            <input type="text" name="name" value="{{ request('name') }}" class="form-control"
                                   placeholder="Tìm kiếm theo tên ...">
                        </div>
                        <div class="col-sm-3 form-group">
                            <button class="btn btn-primary" type="submit">Tìm kiếm</button>
                        </div>
                    </div>
                </form>
                <table class="table table-hover">
                    <tr>
                        <th>Id</th>
                        <th>Tên</th>
                        <th>Slug</th>
                        <th>Sắp xếp</th>
                        <th>Action</th>
                    </tr>
                    @foreach($postCategories as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->slug_custom_name }}</td>
                            <td>{{ $item->sort }}</td>

                            <td>
                                <form method="POST" action="{{ route('postCategories.destroy', $item->id) }}"
                                      accept-charset="UTF-8">
                                    @csrf
                                    @method('DELETE')
                                    <div class="btn-group">
                                        <a href="{{ route('postCategories.show', $item->id) }}"
                                           class="btn btn-default btn-xs">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="{{ route('postCategories.edit', $item->id) }}"
                                           class="btn btn-default btn-xs">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        @unless(in_array($item->id,[1,2]))
                                            <button type="submit" class="btn btn-danger btn-xs"
                                                    onclick="return confirm(&quot;Are you sure?&quot;)"><i
                                                    class="fa fa-trash"></i></button>
                                        @endunless
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
                {!! $postCategories->appends(Request::all())->links() !!}
                <div class="card-footer clearfix float-right">
                    <div class="float-right">

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


