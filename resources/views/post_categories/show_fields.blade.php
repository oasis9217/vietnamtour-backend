<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/postCategories.fields.id').':') !!}
    <p>{{ $postCategory->id }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/postCategories.fields.name').':') !!}
    <p>{{ $postCategory->name }}</p>
</div>

<!-- Slug Field -->
<div class="col-sm-12">
    {!! Form::label('slug', __('models/postCategories.fields.slug').':') !!}
    <p>{{ $postCategory->slug }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/postCategories.fields.created_at').':') !!}
    <p>{{ $postCategory->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/postCategories.fields.updated_at').':') !!}
    <p>{{ $postCategory->updated_at }}</p>
</div>

