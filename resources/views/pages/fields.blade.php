<!-- Name Field -->
{{--<div class="form-group col-sm-12">--}}
{{--    {!! Form::label('name', __('models/pages.fields.name').':') !!}--}}
{{--    {!! Form::text('name', null, ['class' => 'form-control']) !!}--}}
{{--</div>--}}

<!-- Title Field -->
<div class="form-group col-sm-12">
    {!! Form::label('title', __('models/pages.fields.title').':') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Content Field -->
<div class="form-group col-sm-12">
    {!! Form::label('content', __('models/pages.fields.content').':') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control editor']) !!}
</div>
