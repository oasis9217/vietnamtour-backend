@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                   @lang('models/pages.plural')
                </div>
{{--                <div class="col-sm-6">--}}
{{--                    <a class="btn btn-primary float-right"--}}
{{--                       href="{{ route('pages.create') }}">--}}
{{--                         @lang('crud.add_new')--}}
{{--                    </a>--}}
{{--                </div>--}}
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-body p-0">
                <div class="card">
                    <div class="card-body p-0 table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Tên</th>
                                <th>Tiêu đề</th>
                                <th>Slug</th>
                                <th>Action</th>
                            </tr>
                            @foreach($pages as $page)
                                <tr>
                                    <td>{{ $page->id }}</td>
                                    <td>{{ $page->name }}</td>
                                    <td>{{ $page->title }}</td>
                                    <td>{{ $page->slug }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('pages.edit', $page->id) }}" class="btn btn-default btn-xs">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        {!! $pages->appends(Request::all())->links() !!}
                        <div class="card-footer clearfix float-right">
                            <div class="float-right">

                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer clearfix float-right">
                    <div class="float-right">

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


