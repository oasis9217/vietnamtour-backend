<div class="modal fade" id="createTourDetailModal" tabindex="-1" role="dialog" aria-labelledby="createTourDetailModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Thêm mới ngày Tour </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="messageCreateWrap" class="alert alert-warning alert-dismissible fade show" role="alert">
                    <p id="message-create"></p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('tours.create_detail')}}" method="post" id="createFormTourDetail">
                   @csrf
                    <div class="form-group col-sm-12">
                        <input type="hidden" name="tour_id" id="tourCreateId">
                        <label for="post_category_id">Tour</label>
                        <p id="createTourName"></p>
                    </div>

                    <!-- Name Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::label('name', __('models/tourDetails.fields.name').':') !!}
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                        <p class="text-danger errors error-name"></p>
                    </div>

                    <!-- Desc Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::label('Nội dung ngày tour') !!}
                        {!! Form::textarea('desc', null, ['class' => 'form-control','rows'=>2]) !!}
                        <p class="text-danger errors error-desc"></p>
                    </div>

                    <!-- Lat Field -->
                   <div class="row">
                       <div class="form-group col-6">
                           {!! Form::label('lat', __('models/tourDetails.fields.lat').':') !!}
                           {!! Form::text('lat', null, ['class' => 'form-control lat']) !!}
                       </div>

                       <!-- Lpg Field -->
                       <div class="form-group col-6">
                           {!! Form::label('lpg', __('models/tourDetails.fields.lpg').':') !!}
                           {!! Form::text('lpg', null, ['class' => 'form-control lpg']) !!}
                       </div>
                   </div>

                    <div class="form-group col-sm-12">
                        <label for="post_category_id">Vị trí Toạ độ </label>
                        <select class="form-control selectpicker" id="tourDetailLat" data-live-search="true">
                            <option  value="">Chọn Vị trí Toạ độ </option>
                            @foreach(config('data.tour_detail_lat') as $item)
                                <option  value="{{ $item['lat'].'-'.$item['lpg'] }}" class="text-capitalize">{{ $item['name'] }}</option>
                            @endforeach
                        </select>
                    </div>

                    <!-- Content Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::label('content', __('models/tourDetails.fields.content').'(hiển thị trên bản đồ google api):') !!}
                        {!! Form::textarea('content', null, ['class' => 'form-control editor']) !!}
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-create-tour-detail">Save changes</button>
            </div>
        </div>
    </div>
</div>
