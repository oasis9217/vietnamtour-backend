

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/tours.fields.name').':') !!}
    <p>{{ $tours->name }}</p>
</div>

<!-- Slug Field -->
<div class="col-sm-12">
    {!! Form::label('slug', __('models/tours.fields.slug').':') !!}
    <p>{{ $tours->slug }}</p>
</div>

<!-- Desc Field -->
<div class="col-sm-12">
    {!! Form::label('desc', __('models/tours.fields.desc').':') !!}
    <p>{{ $tours->desc }}</p>
</div>

<!-- Price Field -->
<div class="col-sm-12">
    {!! Form::label('price', __('models/tours.fields.price').':') !!}
    <p>{{ $tours->price }}</p>
</div>

<!-- Content Field -->
<div class="col-sm-12">
    {!! Form::label('content', __('models/tours.fields.content').':') !!}
    <p>{!! $tours->content !!}</p>
</div>

<!-- Is Public Field -->
<div class="col-sm-12">
    {!! Form::label('is_public', __('models/tours.fields.is_public').':') !!}
    <p>{{ $tours->is_public }}</p>
</div>

<!-- Meta Field -->
<div class="col-sm-12">
    {!! Form::label('meta', __('models/tours.fields.meta').':') !!}
    <p>{{ $tours->meta }}</p>
</div>

<!-- Day Field -->
<div class="col-sm-12">
    {!! Form::label('day', __('models/tours.fields.day').':') !!}
    <p>{{ $tours->day }}</p>
</div>

<!-- Keyword Field -->
<div class="col-sm-12">
    {!! Form::label('keyword', __('models/tours.fields.keyword').':') !!}
    <p>{{ $tours->keyword }}</p>
</div>

<!-- Image Field -->
<div class="col-sm-12">
    {!! Form::label('image', __('models/posts.fields.image').':') !!}
    <p><img src="{{ asset('storage/tours/'. $tours->image) }}"></p>
</div>
<div class="col-sm-12">
    {!! Form::label('Thumbnail', __('Thumbnail').':') !!}
    <p><img src="{{ asset('storage/tours/'. $tours->thumbnail) }}"></p>
</div>
<div class="col-sm-12">
    {!! Form::label('Header Image', __('Header Image').':') !!}
    <p><img src="{{ asset('storage/tours/'. $tours->header_image) }}"></p>
</div>


<div class="col-sm-12">
    {!! Form::label('Map Image', __('Map Image').':') !!}
    <p><img src="{{ asset('storage/tours/'. $tours->map_image) }}"></p>
</div>
