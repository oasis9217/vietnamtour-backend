<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', __('models/tours.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    <label for="post_category_id">Thể loại</label>
    <select  class="form-control selectpicker" name="tour_category_id[]" id="tour_category_id" data-live-search="true">
        @foreach($categories as $category)
            <option
                {{ isset($tour) ? in_array($category->id, $tour->categories->pluck('id')->toArray()) ? 'selected' : '' : '' }}
                value="{{ $category->id }}">
                {{ $category->name }}
            </option>
        @endforeach
    </select>
</div>

<!-- Desc Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Excerpt') !!}
    {!! Form::textarea('desc', null, ['class' => 'form-control']) !!}
</div>

<!-- Content Field -->
<div class="form-group col-sm-12">
    {!! Form::label('content', __('models/tours.fields.content').':') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control editor']) !!}
</div>

<!-- Meta Field -->
@include('meta_example')

<div class="form-group col-sm-12">
    {!! Form::label('meta', __('models/tours.fields.meta').':') !!}
    {!! Form::textarea('meta',  isset($mode) && $mode != 'edit' ? config('data.meta_default') : null, ['class' => 'form-control']) !!}
</div>

<!-- Day Field -->
<div class="form-group col-sm-6">
    {!! Form::label('day', __('models/tours.fields.day').':') !!}
    {!! Form::number('day', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', __('models/tours.fields.price').':') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Keyword Field -->
<div class="form-group col-sm-12">
    {!! Form::label('keyword', __('models/tours.fields.keyword').':') !!}
    {!! Form::text('keyword', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Public Field -->
<p class="col-12 font-weight-bold">Setting</p>
<div class="form-group col-sm-6 col-md-6 col-lg-3">
    <div class="form-check">
        {!! Form::hidden('is_public', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('is_public', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('is_public', __('models/tours.fields.is_public'), ['class' => 'form-check-label']) !!}
    </div>
</div>

<div class="form-group col-sm-6 col-md-6 col-lg-3">
    <div class="form-check">
        {!! Form::hidden('is_highlight', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('is_highlight', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('Tour Highlight', __('models/tours.fields.is_highlight'), ['class' => 'form-check-label']) !!}
    </div>
</div>

<div class="form-group col-sm-6 col-md-6 col-lg-3">
    <div class="form-check">
        {!! Form::hidden('is_show_in_home', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('is_show_in_home', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('is_show_in_home', __('Is Show in Home (8 tour)'), ['class' => 'form-check-label']) !!}
    </div>
</div>


<div class="form-group col-sm-6 col-md-6 col-lg-3">
    <div class="form-check">
        {!! Form::hidden('is_show_in_category_home', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('is_show_in_category_home', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('is_show_in_category_home', __('Is show In Category Home'), ['class' => 'form-check-label']) !!}
    </div>
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', __('models/posts.fields.image').':') !!}
    <p>(291 X 394)</p>
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('image', ['class' => 'custom-file-input']) !!}
            {!! Form::label('image', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('thumbnail', __('Thumbnail').':') !!}
    <p>(316 X 208)</p>
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('thumbnail', ['class' => 'custom-file-input']) !!}
            {!! Form::label('thumbnail', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('header_image', __('Header Image').':') !!}
    <p>(1366 X 620)</p>
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('header_image', ['class' => 'custom-file-input']) !!}
            {!! Form::label('header_image', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>

<div class="form-group col-sm-6">
    {!! Form::label('map_image', __('Map Image').':') !!}
    <p>(1359 X 472)</p>
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('map_image', ['class' => 'custom-file-input']) !!}
            {!! Form::label('map_image', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>



<div class="form-group col-sm-12">
    {!! Form::label('Itinerary')!!}
    {!! Form::textarea('itinerary', isset($mode) && $mode != 'edit' ? 'The trip brings you from the vibrant Hanoi to the far most area There are more things to delve into each traveling day. There are more things to delve into each traveling day.' : null, ['class' => 'form-control']) !!}
</div>

<div class="row col-sm-12">
    <div class="col-12 col-sm-6">
        <p>WHAT`S INCLUDED</p>
        <div class="form-group col-sm-12">
            {!! Form::label('Accommodation')!!}
            {!! Form::textarea('accommodation', isset($mode) && $mode != 'edit' ? 'All hotel nights (overnight on train/cruise per itinerary)' : null, ['class' => 'form-control', 'rows' =>'2']) !!}
        </div>

        <div class="form-group col-sm-12">
            {!! Form::label('meals',null,['class' => 'text-capitalize'])!!}
            {!! Form::textarea('meals', isset($mode) && $mode != 'edit' ? 'All breakfasts and lunches in tour days' : null, ['class' => 'form-control','rows' =>'2']) !!}
        </div>
        <div class="form-group col-sm-12">
            {!! Form::label('Transport',null,['class' => 'text-capitalize'])!!}
            {!! Form::textarea('transport', isset($mode) && $mode != 'edit' ? 'Private car with A/C, internal flights' : null, ['class' => 'form-control','rows' =>'2']) !!}
        </div>
        <div class="form-group col-sm-12">
            {!! Form::label('Travel team',null,['class' => 'text-capitalize'])!!}
            {!! Form::textarea('travel_team', isset($mode) && $mode != 'edit' ? 'Private tour guide, private drivers' : null, ['class' => 'form-control','rows' =>'2']) !!}
        </div>
        <div class="form-group col-sm-12">
            {!! Form::label('Experiences',null,['class' => 'text-capitalize'])!!}
            {!! Form::textarea('experiences', isset($mode) && $mode != 'edit' ? '10 unique travel experiences >>' : null, ['class' => 'form-control','rows' =>'2']) !!}
        </div>
        <div class="form-group col-sm-12">
            {!! Form::label('Visa Arrangement',null,['class' => 'text-capitalize'])!!}
            {!! Form::textarea('visa_arrangement', isset($mode) && $mode != 'edit' ? 'We`’ll help you to arrange the visa' : null, ['class' => 'form-control','rows' =>'2']) !!}
        </div>
    </div>

    <div class="col-12 col-sm-6">
        <p>WHAT`S NOT INCLUDED
        </p>
        <div class="form-group col-sm-12">
            {!! Form::label('International Flights',null,['class' => 'text-capitalize'])!!}
            {!! Form::textarea('international_flights', isset($mode) && $mode != 'edit' ? 'Flights to/ from the destination' : null, ['class' => 'form-control','rows' =>'2']) !!}
        </div>
        <div class="form-group col-sm-12">
            {!! Form::label('Travel Insurance ',null,['class' => 'text-capitalize'])!!}
            {!! Form::textarea('travel_insurance', isset($mode) && $mode != 'edit' ? 'For the time you travel' : null, ['class' => 'form-control','rows' =>'2']) !!}
        </div>

        <div class="form-group col-sm-12">
            {!! Form::label('Meals not included in the itinerary ',null,['class' => 'text-capitalize'])!!}
            {!! Form::textarea('meals_not_included', isset($mode) && $mode != 'edit' ? 'Drinks and meals not mentioned' : null, ['class' => 'form-control','rows' =>'2']) !!}
        </div>
        <div class="form-group col-sm-12">
            {!! Form::label('Personal Expenses',null,['class' => 'text-capitalize'])!!}
            {!! Form::textarea('personal_expenses', isset($mode) && $mode != 'edit' ? 'Shopping or personal purchases' : null, ['class' => 'form-control','rows' =>'2']) !!}
        </div>
    </div>
</div>

