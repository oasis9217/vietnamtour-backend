@extends('layouts.app')
@push('page_css')
    <link rel="stylesheet" href="{{asset('admin/ckeditor/css/ckeditor.css')}}">
@endpush
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                   @lang('models/tours.plural')
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-primary float-right"
                       href="{{ route('tours.create') }}">
                         @lang('crud.add_new')
                    </a>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-body p-0 table-responsive">
                <form id="search_form" action="{{ route('tours.index') }}" method="get">
                    {{-- Setting --}}
                    <div class="row p-3">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <select class="form-control selectpicker" name="tour_category_id" data-live-search="true">
                                    <option {{ request('tour_category_id') ? 'selected' : '' }}value="">Thể loại</option>
                                    @foreach($categories as $category)
                                        <option {{ request('tour_category_id') == $category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" name="name" class="form-control" placeholder="Tìm kiếm theo tên ..." value="{{ request('name') }}">
                        </div>
                        <div class="col-sm-3 form-group">
                            <button class="btn btn-primary" type="submit">Tìm kiếm</button>
                        </div>
                    </div>
                </form>
                <table class="table table-hover">
                    <tr>
                        <th>Id</th>
                        <th>Tên</th>
                        <th>Ngày</th>
                        <th>Giá</th>
                        <th class="text-center">Chi tiết tour</th>
                        <th>Ảnh</th>
                        <th>Số Ngày Tour</th>
                        <th>Thêm Ngày Tour</th>
                        <th class="text-center">Được public</th>

                        <th>Action</th>
                    </tr>
                    @foreach($tours as $tour)
                        <tr>
                            <td>{{ $tour->id }}</td>
                            <td>{{ $tour->name }}</td>
                            <td>{{ $tour->day }}</td>
                            <td>{{ $tour->price }}</td>
                            <td class="text-center"><a href="{{ route('tourDetails.index', ['tour_id' => $tour->id]) }}">Chi tiết</a></td>
                            <td><a href="{{ $tour->image_path }}" target="_blank">{{ $tour->image }}</a></td>
                            <td>{{$tour->tour_details_count}}</td>
                            <td>
                                <button data-id="{{$tour->id}}" data-name="{{$tour->name}}" class="btn btn-primary add-tour-detail">Thêm Ngày Tour</button>
                            </td>
                            <td class="text-center">
                                @if($tour->is_public)
                                    <i class="text-success far fa-check-square"></i>
                                @else
                                    <i class="text-danger fas fa-ban"></i>
                                @endif
                            </td>
                            <td>
                                <form method="POST" action="{{ route('tours.destroy', $tour->id) }}" accept-charset="UTF-8">
                                    @csrf
                                    @method('DELETE')
                                    <div class="btn-group">
                                        <a href="{{ route('tours.show', $tour->id) }}" class="btn btn-default btn-xs">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="{{ route('tours.edit', $tour->id) }}" class="btn btn-default btn-xs">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <button type="submit" class="btn btn-danger btn-xs" onclick="return confirm(&quot;Are you sure?&quot;)"><i class="fa fa-trash"></i></button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
                {!! $tours->appends(Request::all())->links() !!}
                <div class="card-footer clearfix float-right">
                    <div class="float-right">

                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('tours.create_detail_modal')

@endsection

@push('page_scripts')
    <script src="{{ asset('ckeditor5-classic/build/ckeditor.js') }}"></script>
    <script src="{{ asset('ckfinder/ckfinder.js') }}"></script>
    <script src="{{ asset('admin/ckeditor/js/init.js') }}"></script>
    <script src="{{asset('admin/js/tour.js')}}"></script>

@endpush


