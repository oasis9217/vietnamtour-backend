<table  class="table table-bordered">
    <tr style="border: 1px solid">
        <th>Họ Tên</th>
        <td>{{$customerContact->name}}</td>
    </tr>
    <tr>
        <th>Email</th>
        <td>{{$customerContact->email}}</td>
    </tr>
    <tr>
        <th>Số điện  </th>
        <td>{{$customerContact->phone_number}}</td>
    </tr>
    <tr>
        <th>Thông tin </th>
        <td>{{$customerContact->desc}}</td>
    </tr>
    <tr>
        <th>Quốc </th>
        <td>{{$customerContact->country}}</td>
    </tr>
    @if($customerContact->tour)
        <tr>
            <th>Tên Tour</th>
            <td>{{$customerContact->tour->name}}</td>
        </tr>
    @endif

    <tr>
        <th>Bao gồm chuyến bay quốc tế</th>
        <td>{{$customerContact->international_flight == 1? 'Có':'Không' }}</td>
    </tr>
    <tr>
        <th>Ngày khởi hành</th>
        <td>{{$customerContact->departure_date }}</td>
    </tr>
    <tr>
        <th>Ngày linh hoạt</th>
        <td>{{$customerContact->is_flexible_date == 1? 'Có':'Không' }}</td>
    </tr>
    <tr>
        <th>Thành viên gia đình </th>
        <td>
            <p>{{$customerContact->adult }} Người lớn</p>
            <p>{{$customerContact->child }} trẻ con</p>
            <p>{{$customerContact->child_baby }} trẻ nhỏ</p>
        </td>
    </tr>

    <tr>
        <th>Khách sạn</th>
        <td>
            <p>{{$customerContact->hotel_room_quantity }} Phòng</p>
            <p>Loại phòng:{{$customerContact->hotel_class }}</p>
        </td>
    </tr>
    <tr>
        <th>Ngày đăng kí</th>
        <td>
            {{$customerContact->created_at}}
        </td>
    </tr>
</table>
