<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/contactuses.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', __('models/contactuses.fields.email').':') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', __('models/contactuses.fields.phone').':') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Country Field -->
<div class="form-group col-sm-6">
    {!! Form::label('country', __('models/contactuses.fields.country').':') !!}
    {!! Form::select('country', ['Vietnam' => 'Vietnam'], null, ['class' => 'form-control custom-select']) !!}
</div>
