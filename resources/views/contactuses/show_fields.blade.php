<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/contactuses.fields.id').':') !!}
    <p>{{ $contactUs->id }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/contactuses.fields.name').':') !!}
    <p>{{ $contactUs->name }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    {!! Form::label('email', __('models/contactuses.fields.email').':') !!}
    <p>{{ $contactUs->email }}</p>
</div>

<!-- Phone Field -->
<div class="col-sm-12">
    {!! Form::label('phone', __('models/contactuses.fields.phone').':') !!}
    <p>{{ $contactUs->phone }}</p>
</div>

<!-- Country Field -->
<div class="col-sm-12">
    {!! Form::label('country', __('models/contactuses.fields.country').':') !!}
    <p>{{ $contactUs->country }}</p>
</div>

