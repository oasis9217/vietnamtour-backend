@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                   @lang('models/settings.plural')
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-body p-0">
                <div class="card">
                    <div class="card-body p-0 table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Tên</th>
                                <th>Tên (Tiếng việt)</th>
                                <th>Nội dung</th>
                                <th>Action</th>
                            </tr>
                            @foreach($settings as $setting)
                                <tr>
                                    <td>{{ $setting->id }}</td>
                                    <td>{{ $setting->name }}</td>
                                    <td>{{ $setting->name_vi }}</td>
                                    <td>{!! $setting->content !!}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('settings.edit', $setting->id) }}" class="btn btn-default btn-xs">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        {!! $settings->appends(Request::all())->links() !!}
                        <div class="card-footer clearfix float-right">
                            <div class="float-right">

                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer clearfix float-right">
                    <div class="float-right">

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


