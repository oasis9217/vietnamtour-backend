<div class="form-group col-sm-12">
    {!! Form::label('name_vi', 'Tên Tiếng việt:') !!}
    {!! Form::text('name_vi', null, ['class' => 'form-control']) !!}
</div>
<!-- Content Field -->
<div class="form-group col-sm-12">
    {!! Form::label('content', __('models/settings.fields.content').':') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control editor']) !!}
</div>
