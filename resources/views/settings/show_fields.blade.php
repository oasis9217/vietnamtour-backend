<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/settings.fields.id').':') !!}
    <p>{{ $setting->id }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/settings.fields.name').':') !!}
    <p>{{ $setting->name }}</p>
</div>

<!-- Content Field -->
<div class="col-sm-12">
    {!! Form::label('content', __('models/settings.fields.content').':') !!}
    <p>{{ $setting->content }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/settings.fields.created_at').':') !!}
    <p>{{ $setting->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/settings.fields.updated_at').':') !!}
    <p>{{ $setting->updated_at }}</p>
</div>

