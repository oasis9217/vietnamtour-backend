<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/homeSlides.fields.id').':') !!}
    <p>{{ $homeSlide->id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/homeSlides.fields.created_at').':') !!}
    <p>{{ $homeSlide->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/homeSlides.fields.updated_at').':') !!}
    <p>{{ $homeSlide->updated_at }}</p>
</div>

<!-- Is Public Field -->
<div class="col-sm-12">
    {!! Form::label('is_public', __('models/homeSlides.fields.is_public').':') !!}
    <p>{{ $homeSlide->is_public }}</p>
</div>

<!-- Image Field -->
<div class="col-sm-12">
    {!! Form::label('image', __('models/homeSlides.fields.image').':') !!}
    <p>{{ $homeSlide->image }}</p>
</div>

<!-- Content Field -->
<div class="col-sm-12">
    {!! Form::label('content', __('models/homeSlides.fields.content').':') !!}
    <p>{{ $homeSlide->content }}</p>
</div>

