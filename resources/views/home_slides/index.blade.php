@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                   @lang('models/homeSlides.plural')
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-primary float-right"
                       href="{{ route('homeSlides.create') }}">
                         @lang('crud.add_new')
                    </a>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-body p-0 table-responsive">
                <form id="search_form" action="{{ route('homeSlides.index') }}" method="get">
                    {{-- Setting --}}

                </form>
                <table class="table table-hover">
                    <tr>
                        <th>Id</th>
                        <th>Ảnh</th>
                        <th>Public</th>
                        <th>Action</th>
                    </tr>
                    @foreach($homeSlides as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td><a href="{{$item->image_path}}">{{$item->image}}</a></td>
                           <td> @if($item->is_public)
                                   <i class="text-success far fa-check-square"></i>
                               @else
                                   <i class="text-danger fas fa-ban"></i>
                               @endif
                           </td>
                            <td>
                                <form method="POST" action="{{ route('homeSlides.destroy', $item->id) }}" accept-charset="UTF-8">
                                    @csrf
                                    @method('DELETE')
                                    <div class="btn-group">
                                        <a href="{{ route('homeSlides.show', $item->id) }}" class="btn btn-default btn-xs">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="{{ route('homeSlides.edit', $item->id) }}" class="btn btn-default btn-xs">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <button type="submit" class="btn btn-danger btn-xs" onclick="return confirm(&quot;Are you sure?&quot;)"><i class="fa fa-trash"></i></button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
                {!! $homeSlides->appends(Request::all())->links() !!}
                <div class="card-footer clearfix float-right">
                    <div class="float-right">

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


