<!-- Is Public Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', __('models/homeSlides.fields.image').':') !!} 1366 X 620
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('image', ['class' => 'custom-file-input']) !!}
            {!! Form::label('image', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>

<div class="form-group col-sm-12">
    <div class="form-check">
        {!! Form::hidden('is_public', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('is_public', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('is_public', __('models/homeSlides.fields.is_public').':', ['class' => 'form-check-label'])!!}
    </div>
</div>
<div class="clearfix"></div>


{{--<!-- Content Field -->--}}
{{--<div class="form-group col-sm-6">--}}
{{--    {!! Form::label('content', __('models/homeSlides.fields.content').':') !!}--}}
{{--    {!! Form::text('content', null, ['class' => 'form-control']) !!}--}}
{{--</div>--}}
