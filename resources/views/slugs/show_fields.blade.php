<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/slugs.fields.id').':') !!}
    <p>{{ $slug->id }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/slugs.fields.name').':') !!}
    <p>{{ $slug->name }}</p>
</div>

<!-- Slugable Id Field -->
<div class="col-sm-12">
    {!! Form::label('slugable_id', __('models/slugs.fields.slugable_id').':') !!}
    <p>{{ $slug->slugable_id }}</p>
</div>

<!-- Slugable Type Field -->
<div class="col-sm-12">
    {!! Form::label('slugable_type', __('models/slugs.fields.slugable_type').':') !!}
    <p>{{ $slug->slugable_type }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/slugs.fields.created_at').':') !!}
    <p>{{ $slug->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/slugs.fields.updated_at').':') !!}
    <p>{{ $slug->updated_at }}</p>
</div>

