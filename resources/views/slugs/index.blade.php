@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                   @lang('models/slugs.plural')
                </div>
{{--                <div class="col-sm-6">--}}
{{--                    <a class="btn btn-primary float-right"--}}
{{--                       href="{{ route('slugs.create') }}">--}}
{{--                         @lang('crud.add_new')--}}
{{--                    </a>--}}
{{--                </div>--}}
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('flash::message')

        <div class="clearfix"></div>


        <div class="card">
            <div class="card-body p-0 table-responsive">
                <form id="search_form" action="{{ route('slugs.index') }}" method="get">
                    {{-- Setting --}}
                    <div class="row p-3">
                        <div class="col-sm-3">
                            <input type="text" name="name" value="{{ request('name') }}" class="form-control" placeholder="Tìm kiếm theo tên ...">
                        </div>
                        <div class="col-sm-3 form-group">
                            <button class="btn btn-primary" type="submit">Tìm kiếm</button>
                        </div>
                    </div>
                </form>
                <table class="table table-hover">
                    <tr>
                        <th>Đường dẫn</th>
                        <th>Tên</th>
                        <th>Loại</th>
{{--                        <th>Action</th>--}}
                    </tr>
                    @foreach($slugs as $item)
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->name_type }}</td>
                            <td>{{ $item->type_str }}</td>
{{--                            <td>--}}
{{--                                <form method="POST" action="{{ route('slugs.destroy', $item->id) }}" accept-charset="UTF-8">--}}
{{--                                    @csrf--}}
{{--                                    @method('DELETE')--}}
{{--                                    <div class="btn-group">--}}
{{--                                        <a href="{{ route('slugs.show', $item->id) }}" class="btn btn-default btn-xs">--}}
{{--                                            <i class="fa fa-eye"></i>--}}
{{--                                        </a>--}}
{{--                                        <a href="{{ route('slugs.edit', $item->id) }}" class="btn btn-default btn-xs">--}}
{{--                                            <i class="fa fa-edit"></i>--}}
{{--                                        </a>--}}
{{--                                        <button type="submit" class="btn btn-danger btn-xs" onclick="return confirm(&quot;Are you sure?&quot;)"><i class="fa fa-trash"></i></button>--}}
{{--                                    </div>--}}
{{--                                </form>--}}
{{--                            </td>--}}
                        </tr>
                    @endforeach
                </table>
                {!! $slugs->appends(Request::all())->links() !!}
                <div class="card-footer clearfix float-right">
                    <div class="float-right">

                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection


