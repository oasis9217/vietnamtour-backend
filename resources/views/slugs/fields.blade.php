<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/slugs.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Slugable Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('slugable_id', __('models/slugs.fields.slugable_id').':') !!}
    {!! Form::text('slugable_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Slugable Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('slugable_type', __('models/slugs.fields.slugable_type').':') !!}
    {!! Form::text('slugable_type', null, ['class' => 'form-control']) !!}
</div>