@section('style')
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta3/dist/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="{{asset('clients/assets/css/contact_us.css')}}"/>

@endsection
@section('title')
   Contact Us
@endsection
@section('meta')
    <meta name="keywords" content="Contact and Customer Service for Vietnam Tours">
    <meta name="description" content="Access one of the best team who offer Vietnam Tours for private and group. Contact us for the best deals." />
@endsection

@extends('client.layouts.app')
@section('header_banner')
    <div class="header-banner header-banner-tour"></div>
@endsection
@section('main')

 <section class="contact-us">
     <div class="container about_view">
         <div class="row">
             @include('flash::message')

             <div class="col-md-3 hidden-xs">
                 <div class="panel panel-default bpt-panel">
                     <div class="panel-heading">
                         <h3 class="highlight padding-top-0 padding-bottom-0 margin-bottom-0"><b>About VietNam Tour</b></h3>
                     </div>
                     <div class="list-group">
                         <a href="{{route('client.slug','contact-us')}}" rel="nofollow" class="list-group-item active">
                             Contact Us            <span class="icon icon-arrow-right-blue"></span>
                         </a>
                         <a href="{{route('client.slug','about-us')}}" rel="nofollow" class="list-group-item ">
                             About us
                             <span class="icon icon-arrow-right-blue"></span>
                         </a>

                         <a href="{{route('client.slug','privacy-policy')}}" rel="nofollow" class="list-group-item ">
                             Privacy policy
                             <span class="icon icon-arrow-right-blue"></span>
                         </a>
                         <a href="{{route('client.slug','for-travel-agent')}}" rel="nofollow" class="list-group-item ">
                             For travel agent
                             <span class="icon icon-arrow-right-blue"></span>
                         </a>
                         <a href="{{route('client.slug','term')}}" rel="nofollow" class="list-group-item ">
                             Terms & condition
                             <span class="icon icon-arrow-right-blue"></span>
                         </a>


                     </div>

                 </div>		</div>
             <div class="col-xs-12 col-md-9">
                 <form class="form-horizontal" method="post" action="{{route('contactuses-store')}}">
                    @csrf
                     <div class="contact_form">
                         <h1>Contact Us</h1>
                         <div class="row form-group contact_form__row">
                             <div class="col-xs-12 col-sm-3 col-md-3 contact_form__row__label">
                                 Gender:
                             </div>
                             <div class="col-xs-12 col-sm-3 col-md-3 contact_form__row__input required">
                                 <select name="gender" class="form-control">
                                     <option {{old('gender') == 1?'selected' :''}} value="1">Mr</option>
                                     <option  {{old('gender') == 0?'selected' :''}} value="0">Ms</option>
                                 </select>
                                 @error('gender')
                                 <p class="text-danger">{{$message}}</p>
                                 @enderror
                             </div>
                         </div>

                         <div class="row form-group contact_form__row">
                             <div class="col-xs-12 col-sm-3 col-md-3 contact_form__row__label">
                                 Full Name:
                             </div>
                             <div class="col-xs-12 col-sm-6 col-md-6 contact_form__row__input required">
                                 <input class="form-control" placeholder="Enter your name" type="text" name="name" value="{{old('name')}}">
                                 @error('name')
                                 <p class="text-danger">{{$message}}</p>
                                 @enderror
                             </div>

                         </div>

                         <div class="row form-group contact_form__row">
                             <div class="col-xs-12 col-sm-3 col-md-3 contact_form__row__label">
                                 Email:
                             </div>
                             <div class="col-xs-12 col-sm-6 col-md-6 contact_form__row__input required">
                                 <input class="form-control required-contact req-email" placeholder="Enter your confirm email" type="text" name="email" value="{{old('email')}}">
                                 <div></div>
                                 @error('email')
                                 <p class="text-danger">{{$message}}</p>
                                 @enderror
                             </div>
                         </div>

                         <div class="row form-group contact_form__row">
                             <div class="col-xs-12 col-sm-3 col-md-3 contact_form__row__label">
                                 Phone Number:
                             </div>
                             <div class="col-xs-12 col-sm-6 col-md-6 contact_form__row__input">
                                 <input class="form-control" type="text" name="phone" placeholder="Enter your phone number" value="{{old('phone')}}">
                             </div>
                         </div>

                         <div class="row form-group contact_form__row">
                             <div class="col-xs-12 col-sm-3 col-md-3 contact_form__row__label">
                                 Country:
                             </div>
                             <div class="col-xs-12 col-sm-6 col-md-6 contact_form__row__input">
                                 <select name="country" class="form-control">
                                     @foreach(config('data.countries') as $item)
                                     <option {{old('country') == $item ?'selected' :''}} value="{{$item['name']}}">{{$item['name']}}</option>
                                     @endforeach
                                 </select>
                             </div>
                         </div>

                         <div class="row form-group contact_form__row">
                             <div class="col-xs-12 col-sm-3 col-md-3 contact_form__row__label">
                                 City:
                             </div>
                             <div class="col-xs-12 col-sm-6 col-md-6 contact_form__row__input">
                                 <input class="form-control" type="text" name="city" {{old('city')}} placeholder="Select your city" >
                             </div>
                         </div>

                         <div class="row form-group contact_form__row">
                             <div class="col-xs-12 col-sm-3 col-md-3 contact_form__row__label social d-flex flex-column align-content-end">
                                 Social Media:<div>(WhatsApp, Viber...)</div>			</div>
                             <div class="col-xs-12 col-sm-6 col-md-6 contact_form__row__input">
                                 <input class="form-control" type="text" name="social_media" value="{{old('social_media')}}">
                             </div>
                         </div>


                         <div class="row form-group contact_form__row">
                             <div class="col-xs-12 col-sm-3 col-md-3 contact_form__row__label social">
                                 Subject:
                             </div>
                             <div class="col-xs-12 col-sm-6 col-md-6 contact_form__row__input">
                                 @foreach(config('data.contact_us_subject') as $key =>$item)
                                 <div class="checkbox">
                                     <label>
                                         <input type="radio" name="subject" {{old('subject') ==  $key?'checked' :''}} value="{{$key}}" >   {{$item}}</label>
                                 </div>
                                     @endforeach
                             </div>
                         </div>

                         <div class="row form-group contact_form__row">
                             <div class="col-xs-12 col-sm-3 col-md-3 contact_form__row__label">
                                 Special Requests			</div>
                             <div class="col-xs-12 col-sm-6 col-md-6 contact_form__row__input required">
                                 <textarea class="form-control" name="special_request" cols="66" rows="5" tabindex="8">{{old('special_request')}}</textarea>
                                 @error('special_request')
                                 <p class="text-danger">{{$message}}</p>
                                 @enderror
                             </div>
                         </div>

                         @include('client.page.capcha')
                         <div class="form-group margin-top-20">
                             <div class="col-12 d-flex justify-content-center">
                                 <button type="submit" id="btn btn-primary" name="action" value="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
                             </div>
                         </div>


                     </div>


                 </form>

                 <style>
                     .bpv-search-waiting {
                         text-align: center;
                         margin: 15px 0;
                     }

                     .bpv-search-waiting .ms1 {
                         font-size: 20px;
                         margin-bottom: 15px;

                     }

                     .bpv-search-waiting .ms2 {
                         font-size: 16px;
                     }

                     .bpv-search-waiting img {
                         height: 48px;
                     }
                 </style>
                 <div class="modal fade" id="submit_booking_data_waiting" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="label_submit_data_waiting" aria-hidden="true">
                     <div class="modal-dialog modal-dialog-room">
                         <div class="modal-content">
                             <div class="bpv-search-waiting">
                                 <div class="ms1 text-highlight">Processing data...please wait!</div>
                                 <div class="ms2 text-highlight">
                                     <img style="margin-right:15px;" alt="loading" src="https://d13jio720g7qcs.cloudfront.net/assets/img/flight/loading.gif">
                                     <span>Please wait ... </span>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </section>
@endsection
@section('script')
    <script src="{{asset('clients/assets/js/capcha.js')}}"></script>

@endsection
