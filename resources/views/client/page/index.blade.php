
@section('style')
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta3/dist/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="{{asset('clients/assets/css/contact_us.css')}}"/>

@endsection
@section('title')
    {{$page->name}}
@endsection
@section('meta')
    <meta name="keywords" content="    {{$page->name}}">
    <meta name="description" content="    {{$page->name}}" />
@endsection

@extends('client.layouts.app')
@section('header_banner')
    <div class="header-banner header-banner-tour"></div>
@endsection
@section('main')

    <section class="contact-us">
        <div class="container about_view">
            <div class="row">
                @include('flash::message')

                <div class="col-md-3 hidden-xs">
                    <div class="panel panel-default bpt-panel">
                        <div class="panel-heading">
                            <h3 class="highlight padding-top-0 padding-bottom-0 margin-bottom-0"><b>About VietNam Tour</b></h3>
                        </div>
                        <div class="list-group">
                            <a href="{{route('client.slug','contact-us')}}" rel="nofollow" class="list-group-item ">
                                Contact Us            <span class="icon icon-arrow-right-blue"></span>
                            </a>

                            <a href="{{route('client.slug','about-us')}}" rel="nofollow" class="list-group-item {{request()->path() == 'about-us'?'active' :''}}">
                                About us
                                <span class="icon icon-arrow-right-blue"></span>
                            </a>

                            <a href="{{route('client.slug','privacy-policy')}}" rel="nofollow" class="list-group-item {{request()->path() == 'privacy-policy'?'active' :''}}">
                                Privacy policy
                                <span class="icon icon-arrow-right-blue"></span>
                            </a>
                            <a href="{{route('client.slug','for-travel-agent')}}" rel="nofollow" class="list-group-item {{request()->path() == 'for-travel-agent'?'active' :''}}">
                                For travel agent
                                <span class="icon icon-arrow-right-blue"></span>
                            </a>
                            <a href="{{route('client.slug','term')}}" rel="nofollow" class="list-group-item {{request()->path() == 'term'?'active' :''}}">
                                Terms & condition
                                <span class="icon icon-arrow-right-blue"></span>
                            </a>


                        </div>

                    </div>		</div>
                <div class="col-xs-12 col-md-9">
                    {!! $page->content !!}

                    <style>
                        .bpv-search-waiting {
                            text-align: center;
                            margin: 15px 0;
                        }

                        .bpv-search-waiting .ms1 {
                            font-size: 20px;
                            margin-bottom: 15px;

                        }

                        .bpv-search-waiting .ms2 {
                            font-size: 16px;
                        }

                        .bpv-search-waiting img {
                            height: 48px;
                        }
                    </style>
                    <div class="modal fade" id="submit_booking_data_waiting" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="label_submit_data_waiting" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-room">
                            <div class="modal-content">
                                <div class="bpv-search-waiting">
                                    <div class="ms1 text-highlight">Processing data...please wait!</div>
                                    <div class="ms2 text-highlight">
                                        <img style="margin-right:15px;" alt="loading" src="https://d13jio720g7qcs.cloudfront.net/assets/img/flight/loading.gif">
                                        <span>Please wait ... </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')

@endsection
