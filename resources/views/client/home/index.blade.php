@section('style')
    <link rel="stylesheet" href="{{asset('clients/assets/css/style.css')}}"/>
    <link href="
https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.css
" rel="stylesheet">
{{--    <link rel="prefetch" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.css" as="style">--}}

@endsection
@section('title')
    Vietnam Tours, Ultimate Private Vietnam Tour Packages
@endsection
@extends('client.layouts.app')
@section('meta')
    <meta name="description" content="Best Vietnam Tours for discerning travelers. Tailor Made Vietnam Tour Packages with real Vietnam Travel Experts" />
@endsection
@section('header_image')
   <div class="single-item">
        @if($homeSlides->count() > 0)
            @foreach($homeSlides as $item)
               <div class="header-image">
                   <img data-src="{{$item->image_path}}" class="img-response lazyload" alt=""/>
               </div>
           @endforeach
       @else
       <div class="header-image">
           <img data-src="{{asset('clients/assets/images/header_bg.png')}}" class="img-response lazyload" alt=""/>
       </div>

       @endif
   </div>
@endsection
@section('header_banner')
    <div class="header-banner">
        <p class="header-banner__slogan">See Vietnam Uniquely</p>
        <p class="header-banner__des">
            Fully tailor made tour to Vietnam and its neighboring countries
        </p>
        <div class="btn-group header-banner__btn" role="group">
            <a href="{{route('client.slug',$touCategoryFirst->slug_custom_name)}}" class="btn">Vietnam Tours</a>
            <a href="{{route('client.customize_tour')}}" class="btn">Tailor Made</a>
        </div>
    </div>
@endsection
@section('main')
    <section class="tour-service container">
        <div class="tour-service-head">
            <div class="tour-service-head__title">
                <h1>Vietnam Tours by local experts</h1>
                <img
                    class="tour-service-head__line"
                    src="{{asset('clients/assets/images/tours/tour-title-line.png')}}"
                    alt=""
                />
            </div>

            <div class="tour-service-head__des">
                <p>
                    The opening of Vietnam means you are free to explore more the
                    hiden gems of the country. From exotic markets to vibrant cities,
                    Vietnam is very tempting. Keep your best memory of Vietnam with
                    our carefully designed tours
                </p>
            </div>
        </div>

        <div class="tour-items row">

            @foreach($tours as $item)
                <div class="col-12 col-sm-4 col-md-3">
                    <div class="tour-item">
                        <a class="tour-item__img" href="{{route('client.slug',$item->slug_custom_name)}}">
                            <img data-src="{{$item->image_path}}" class="lazyload" alt="{{$item->name}}"/>
                        </a>
                        <p class="tour-item__name">{{$item->name}}</p>
                        <div class="tour-item-des">
                            <p class="tour-item-des__day">{{$item->day}} days</p>
                           <div style="height: 100px;">
                               <p class="tour-item-des__content text-over3">
                                   {{$item->desc}}
                               </p>
                           </div>
                            <a href="{{route('client.slug',$item->slug_custom_name)}}" class="tour-item-link">View this
                                tour</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>

    <section class="contact-service">
        <div class="contact-sevice-info">
            <div class="contact-avatar">
                <div class="contact-avatar__img">
                    <img
                        src="{{asset('clients/assets/images/avatar/avatar1.png')}}"
                        class="img-fluid img-round"
                        alt=""
                    />
                </div>
                <div class="contact-avatar__img">
                    <img
                        src="{{asset('clients/assets/images/avatar/avatar2.png')}}"
                        class="img-fluid img-round"
                        alt=""
                    />
                </div>
                <div class="contact-avatar__img">
                    <img
                        src="{{asset('clients/assets/images/avatar/avatar3.png')}}"
                        class="img-fluid img-round"
                        alt=""
                    />
                </div>
            </div>
            <p class="contact-service-title">
                Tailormade Tour with inside Travel Experts
            </p>
            <p class="contact-service-des">
                We are happy to join hands with you to plan your beautiful trip to
                Vietnam. We strongly believe
            </p>
            <div class="contact-service-ques">
                <p>Who are we ?</p>
                <p>Why Travel with us ?</p>
                <p>What makes us different?</p>
            </div>
        </div>
        <div class="contact-service-form">
            <form action="{{route('client.customer_contacts.store')}}" method="post" id="contactServiceForm">
                @csrf
                <div class="form-group">
                    <p>
                        Your Trip Expectation, planning time,...<span
                            class="text-danger"
                        >*</span
                        >
                    </p>
                    <textarea
                        class="form-control diable-resize"
                        rows="8"
                        cols="10"
                        name="desc"
                    >{{old('desc')}}</textarea>
                    @error('desc')
                    <p class="text-danger">
                        {{$message}}
                    </p>
                    @enderror
                </div>
                <div class="contact-service-form-name">
                    <div class="form-group">
                        <p>Your Name <span class="text-danger">*</span></p>
                        <input class="form-control" value="{{old('name')}}" name="name" type="text"/>
                        @error('name')
                        <p class="text-danger">
                            {{$message}}
                        </p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <p>Your Email <span class="text-danger">*</span></p>
                        <input class="form-control" value="{{old('email')}}" name="email" type="text"/>
                        @error('email')
                        <p class="text-danger">
                            {{$message}}
                        </p>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-submit-form-contact">
                        GET MY TRIP PLANNED
                    </button>
                </div>
            </form>
        </div>
    </section>

    <section class="style-service container d-flex flex-wrap">
        <div class="style-service-left col-12 col-sm-5">
            <p class="style-service-left__title" style="font-weight: bold">
                Vietnam Tour Packages by styles
            </p>
            <p class="style-service-left__des">
                Some suggestion for your Vietnam Tour in different way. You may find
                the most suitable option for your trip from beach relaxation to
                adventures.
            </p>
            <div class="style-list-tour">
                @foreach($tourCategoryWithTour as $item)
                    <div class="style-list-tour-item dropdown">
                        <p class="dropdown-toggle text-capitalize" type="button" data-bs-toggle="dropdown"
                           aria-expanded="false">

                            {{$item->name}}

                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                width="10px"
                                height="9px"
                            >
                                <path
                                    fill-rule="evenodd"
                                    fill="rgb(172, 89, 117)"
                                    d="M0.552,0.0 C1.14,0.0 9.117,0.0 9.436,0.0 C9.849,0.0 10.156,0.436 9.914,0.867 C9.721,1.213 5.816,8.124 5.481,8.714 C5.262,9.101 4.739,9.94 4.519,8.714 C4.274,8.289 0.349,1.352 0.76,0.850 C0.123,0.481 0.97,0.0 0.552,0.0 Z"
                                />
                            </svg>

                        </p>
                        <ul class="dropdown-menu">
                            @foreach($item->tours as $tour)
                                <li><a class="dropdown-item"
                                       href="{{route('client.slug',$item->slug_custom_name)}}">{{$tour->name}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-12 col-sm-1"></div>
        <div class="col-12 col-sm-6 style-service-right-wrap ">
            <div class="style-service-right ">
                <img data-src="{{asset('clients/assets/images/home_8_image.png')}}" alt="" class="img-response lazyload">
            </div>
        </div>
    </section>

    <section class="multi-tour container d-flex flex-wrap">
        <div class="multi-tour-left col-12 col-sm-6">
            <div class="multi-tour-left__img">
                <img data-src="{{asset('clients/assets/images/map.png')}}" class="lazyload" alt=""/>
            </div>
        </div>
        <div class="multi-tour-right col-12 col-sm-6">
            <div class="multi-tour-right-content">
                <p class="multi-tour-title">Multi Countries Tours</p>
                {!! $multiTourContent->content !!}
            </div>
        </div>
    </section>

    <section class="infomation-vietnam-tour ">
        <div class="infomation-vietnam-tour__title">
            <p>Useful Information to Plan your Vietnam Tour</p>
        </div>
        <div class="userfuls container d-flex flex-wrap">
            <div class="col-12 col-sm-12 col-md-6 userfuls-left">
                <div class="useful-item">
                    <div class="useful-item__icon">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink"
                            width="27px" height="26px">
                            <path fill-rule="evenodd" fill="rgb(250, 245, 245)"
                                  d="M15.331,12.683 L9.301,18.331 L9.545,23.564 L7.256,25.446 L5.946,20.265 C5.946,20.265 6.250,20.40 4.987,21.162 C3.796,22.222 3.526,21.322 4.313,20.434 C5.100,19.546 5.272,19.536 5.272,19.536 L0.440,17.262 L3.301,15.489 L7.161,16.709 L13.535,10.740 L1.398,5.62 L2.885,3.367 L19.123,5.657 C19.123,5.657 24.11,1.381 25.172,0.898 C26.333,0.414 27.126,0.578 26.295,2.113 C25.463,3.648 20.972,8.5 20.972,8.5 L21.293,22.964 L19.180,25.548 L15.331,12.683 Z"/>
                        </svg>
                    </div>
                    <div class="useful-item__des">
                        <p class="useful-item__des-title">How to Get to Vietnam</p>
                        <p class="useful-item__des-content">
                            Commercial airlines that offers direct flights to Vietnam. Flying
                            network and promotions
                        </p>
                    </div>
                </div>

                <div class="useful-item">
                    <div class="useful-item__icon">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink"
                            width="26px"
                            height="29px"
                        >
                            <path
                                fill-rule="evenodd"
                                fill="rgb(252, 246, 246)"
                                d="M20.817,14.500 C24.615,17.469 26.570,20.330 25.721,21.779 C24.910,23.263 21.371,22.901 16.834,21.90 C16.170,25.835 14.696,28.985 12.963,28.985 C11.229,28.985 9.754,25.908 9.53,21.127 C4.555,22.901 1.52,23.263 0.204,21.779 C0.644,20.330 1.310,17.505 5.182,14.500 C1.310,11.530 0.680,8.705 0.204,7.292 C1.52,5.808 4.555,6.98 9.53,7.872 C9.754,3.164 11.229,0.14 12.963,0.14 C14.696,0.14 16.170,3.200 16.871,7.872 C21.371,6.134 24.910,5.808 25.795,7.292 C26.607,8.777 24.615,11.566 20.817,14.500 ZM5.662,14.138 C6.583,13.486 7.542,12.761 8.648,12.73 C8.721,10.733 8.796,9.502 9.16,8.415 C4.813,6.786 1.531,6.351 0.793,7.619 C0.56,8.850 2.47,11.421 5.662,14.138 ZM8.980,20.584 C8.796,19.461 8.685,18.230 8.648,16.889 C7.542,16.238 6.546,15.586 5.662,14.862 C2.47,17.650 0.56,20.149 0.793,21.416 C1.494,22.684 4.776,22.249 8.980,20.584 ZM8.574,12.761 C7.652,13.341 6.841,13.956 6.104,14.500 C6.878,15.78 7.727,15.658 8.574,16.202 C8.537,15.695 8.537,15.78 8.537,14.500 C8.537,13.920 8.537,13.341 8.574,12.761 ZM9.238,12.399 C9.164,13.87 9.164,13.811 9.164,14.500 C9.164,15.260 9.164,15.984 9.275,16.636 C9.865,17.34 10.492,17.397 11.118,17.759 C11.745,18.85 12.372,18.447 12.963,18.737 C13.590,18.411 14.217,18.85 14.879,17.723 C15.470,17.360 16.97,17.34 16.687,16.600 C16.687,15.984 16.761,15.260 16.761,14.500 C16.761,13.739 16.761,13.51 16.687,12.399 C16.97,12.37 15.543,11.711 14.879,11.313 C14.217,10.950 13.590,10.625 12.999,10.299 C12.372,10.588 11.745,10.914 11.118,11.276 C10.454,11.639 9.754,12.73 9.238,12.399 ZM12.336,9.936 C11.377,9.429 10.454,8.995 9.570,8.669 C9.423,9.538 9.348,10.588 9.275,11.639 C9.717,11.348 10.271,11.59 10.786,10.733 C11.266,10.443 11.783,10.154 12.336,9.936 ZM9.312,17.360 C9.312,18.447 9.423,19.425 9.570,20.330 C10.454,20.4 11.377,19.570 12.336,19.62 C11.783,18.845 11.266,18.556 10.786,18.266 C10.271,17.976 9.717,17.686 9.312,17.360 ZM16.282,20.910 C15.175,20.475 14.105,20.4 12.963,19.352 C11.820,20.4 10.676,20.475 9.607,20.910 C10.234,25.400 11.488,28.370 12.963,28.370 C14.437,28.370 15.691,25.473 16.282,20.910 ZM9.643,8.54 C10.751,8.524 11.857,8.995 12.999,9.574 C14.142,8.995 15.286,8.524 16.318,8.89 C15.728,3.635 14.474,0.629 12.963,0.629 C11.525,0.629 10.271,3.562 9.643,8.54 ZM12.963,13.15 C13.810,13.15 14.474,13.667 14.474,14.500 C14.474,15.332 13.810,15.984 12.963,15.984 C12.114,15.984 11.450,15.332 11.450,14.500 C11.450,13.667 12.114,13.15 12.963,13.15 ZM13.590,19.62 C14.549,19.570 15.470,20.4 16.355,20.330 C16.502,19.461 16.614,18.411 16.650,17.360 C16.207,17.686 15.691,17.976 15.175,18.266 C14.659,18.556 14.105,18.845 13.590,19.62 ZM16.650,11.639 C16.614,10.588 16.539,9.574 16.355,8.669 C15.470,9.67 14.585,9.466 13.663,9.936 C14.142,10.154 14.696,10.443 15.175,10.733 C15.728,11.23 16.245,11.348 16.650,11.639 ZM16.945,8.415 C17.129,9.538 17.240,10.697 17.277,12.37 C18.383,12.725 19.379,13.413 20.300,14.138 C23.915,11.421 25.980,8.850 25.242,7.583 C24.468,6.279 21.186,6.749 16.945,8.415 ZM20.300,14.862 C19.379,15.586 18.383,16.274 17.277,16.962 C17.240,18.266 17.129,19.461 16.945,20.511 C21.259,22.286 24.505,22.757 25.242,21.489 C25.980,20.185 23.988,17.650 20.300,14.862 ZM17.351,16.238 C18.199,15.658 19.47,15.115 19.821,14.500 C19.47,13.920 18.272,13.341 17.351,12.761 C17.388,13.304 17.388,13.884 17.388,14.500 C17.388,15.115 17.388,15.695 17.351,16.238 Z"
                            />
                        </svg>
                    </div>
                    <div class="useful-item__des">
                        <p class="useful-item__des-title">Best time to travel to Vietnam</p>
                        <p class="useful-item__des-content">
                            Consultancy for weather in Vietnam, when you visit the country
                        </p>
                    </div>
                </div>

                <div class="useful-item">
                    <div class="useful-item__icon">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink"
                            width="30px" height="30px">
                            <path fill-rule="evenodd" fill="rgb(252, 251, 251)"
                                  d="M14.999,0.7 C23.386,0.7 30.6,6.613 30.6,14.999 C30.6,23.386 23.386,30.6 14.999,30.6 C6.613,30.6 0.6,23.386 0.6,14.999 C0.6,6.613 6.613,0.7 14.999,0.7 ZM6.745,14.338 C6.834,12.439 7.363,10.56 8.246,7.673 C6.922,7.99 5.729,6.481 4.803,5.819 C2.773,8.70 1.538,11.27 1.404,14.338 L6.745,14.338 ZM1.404,15.662 C1.538,18.972 2.773,21.929 4.803,24.180 C5.729,23.518 6.922,22.901 8.246,22.327 C7.319,19.943 6.789,17.516 6.745,15.662 L1.404,15.662 ZM11.204,1.890 C9.129,2.420 7.274,3.479 5.729,4.891 C6.524,5.465 7.540,6.39 8.687,6.525 C9.349,4.848 10.232,3.215 11.204,1.890 ZM5.729,25.107 C7.274,26.520 9.129,27.579 11.204,28.64 C10.188,26.696 9.349,25.107 8.732,23.430 C7.584,23.915 6.569,24.489 5.729,25.107 ZM14.337,9.350 C12.836,9.261 11.115,8.820 9.438,8.114 C8.643,10.189 8.158,12.263 8.69,14.338 L14.337,14.338 L14.337,9.350 ZM8.69,15.662 C8.158,17.648 8.643,19.767 9.438,21.885 C11.115,21.179 12.836,20.738 14.337,20.649 L14.337,15.662 L8.69,15.662 ZM14.337,1.361 C13.895,1.361 13.454,1.405 12.968,1.493 C11.777,3.126 10.673,4.980 9.835,7.10 C11.291,7.540 12.881,7.893 14.337,7.982 L14.337,1.361 ZM14.337,22.17 C12.925,22.106 11.380,22.415 9.880,22.944 C10.673,24.930 11.733,26.873 12.968,28.506 C13.454,28.594 13.895,28.638 14.337,28.638 L14.337,22.17 ZM15.661,7.982 C17.118,7.893 18.706,7.540 20.163,7.10 C19.325,4.980 18.222,3.126 17.29,1.493 C16.545,1.405 16.103,1.361 15.661,1.361 L15.661,7.982 ZM15.661,14.338 L21.929,14.338 C21.841,12.263 21.355,10.189 20.560,8.114 C18.883,8.820 17.161,9.261 15.661,9.350 L15.661,14.338 ZM15.661,15.662 L15.661,20.649 C17.161,20.738 18.883,21.179 20.560,21.885 C21.355,19.767 21.841,17.648 21.929,15.662 L15.661,15.662 ZM15.661,28.638 C16.103,28.638 16.545,28.594 17.29,28.506 C18.265,26.873 19.325,24.930 20.120,22.944 C18.619,22.415 17.74,22.106 15.661,22.17 L15.661,28.638 ZM24.268,4.891 C22.723,3.479 20.870,2.420 18.795,1.890 C19.767,3.215 20.649,4.848 21.310,6.525 C22.458,6.39 23.474,5.465 24.268,4.891 ZM18.795,28.64 C20.870,27.579 22.723,26.520 24.268,25.107 C23.429,24.489 22.415,23.915 21.267,23.430 C20.649,25.107 19.810,26.696 18.795,28.64 ZM28.593,14.338 C28.461,11.27 27.225,8.70 25.195,5.819 C24.268,6.481 23.76,7.99 21.752,7.673 C22.634,10.56 23.165,12.439 23.253,14.338 L28.593,14.338 ZM23.253,15.662 C23.208,17.516 22.679,19.943 21.752,22.327 C23.76,22.901 24.268,23.518 25.195,24.180 C27.225,21.929 28.461,18.972 28.593,15.662 L23.253,15.662 Z"/>
                        </svg>
                    </div>
                    <div class="useful-item__des">
                        <p class="useful-item__des-title">Vietnam Visa Guide</p>
                        <p class="useful-item__des-content">
                            Ultimate information for Vietnam visa including visa on arrival,
                            exemption cases
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 userfuls-right">
                <div class="useful-item">
                    <div class="useful-item__icon">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink"
                            width="32px" height="27px">
                            <path fill-rule="evenodd" fill="rgb(250, 250, 250)"
                                  d="M0.1,10.0 L15.998,0.835 L19.10,2.553 L19.10,0.0 L23.224,0.0 L23.224,4.964 L32.1,10.0 L0.1,10.0 ZM21.988,5.684 L21.988,1.230 L20.251,1.230 L20.246,1.237 L20.246,4.687 L15.984,2.265 L4.631,8.770 L27.375,8.770 L21.988,5.684 ZM28.29,26.999 L16.927,26.999 L16.927,20.554 L16.920,20.549 L13.764,20.549 L13.756,20.554 L13.756,26.999 L3.818,26.999 L3.818,10.962 L28.29,10.962 L28.29,26.999 ZM26.793,12.192 L26.786,12.192 L5.54,12.192 L5.54,25.760 L5.54,25.770 L12.521,25.770 L12.521,19.319 L18.163,19.319 L18.163,25.760 L18.165,25.770 L26.793,25.770 L26.793,12.192 ZM24.312,17.795 L19.604,17.795 L19.604,13.377 L24.312,13.377 L24.312,17.795 ZM6.239,13.377 L10.950,13.377 L10.950,17.795 L6.239,17.795 L6.239,13.377 Z"/>
                        </svg>
                    </div>
                    <div class="useful-item__des">
                        <p class="useful-item__des-title">Where to Sleep</p>
                        <p class="useful-item__des-content">
                            Hotel suggestion for different budgets and styles
                        </p>
                    </div>
                </div>

                <div class="useful-item">
                    <div class="useful-item__icon">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink"
                            width="11px" height="21px">
                            <path fill-rule="evenodd" fill="rgb(252, 251, 251)"
                                  d="M4.185,8.884 C4.418,8.309 4.118,8.22 3.852,8.22 C2.620,8.22 1.22,10.803 0.424,10.803 C0.191,10.803 0.8,10.579 0.8,10.387 C0.8,9.812 1.455,8.469 1.888,8.53 C3.220,6.838 4.950,5.911 6.881,5.911 C8.312,5.911 9.843,6.742 8.645,9.844 L6.248,16.79 C6.49,16.558 5.683,17.358 5.683,17.870 C5.683,18.93 5.815,18.317 6.82,18.317 C7.80,18.317 8.911,15.599 9.377,15.599 C9.544,15.599 9.776,15.791 9.776,16.79 C9.776,17.6 5.883,20.971 2.521,20.971 C1.322,20.971 0.490,20.428 0.490,19.213 C0.490,17.678 1.622,15.56 1.855,14.512 L4.185,8.884 ZM5.982,2.585 C5.982,1.179 7.246,0.28 8.711,0.28 C10.43,0.28 11.8,0.891 11.8,2.202 C11.8,3.673 9.743,4.761 8.246,4.761 C6.881,4.761 5.982,3.897 5.982,2.585 Z"/>
                        </svg>
                    </div>
                    <div class="useful-item__des">
                        <p class="useful-item__des-title">Airport Access</p>
                        <p class="useful-item__des-content">
                            International airports in Vietnam that you can land in
                        </p>
                    </div>
                </div>

                <div class="useful-item">
                    <div class="useful-item__icon">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink"
                            width="34px" height="27px">
                            <path fill-rule="evenodd" fill="rgb(250, 250, 250)"
                                  d="M33.374,11.867 C33.76,11.321 32.261,10.699 32.261,10.699 L28.897,2.403 L28.896,2.403 C28.198,0.832 26.143,0.3 25.246,0.3 L8.752,0.3 C7.856,0.3 5.800,0.832 5.102,2.403 L1.738,10.699 C1.738,10.699 0.923,11.321 0.625,11.867 C0.326,12.412 0.1,12.898 0.1,16.576 C0.1,20.163 0.781,21.439 1.716,21.880 L2.454,22.264 L2.454,25.594 C2.454,26.675 2.732,26.996 3.845,26.996 L6.344,26.996 C7.457,26.996 7.735,26.675 7.735,25.594 L7.735,22.264 L26.264,22.264 L26.264,25.594 C26.264,26.675 26.542,26.996 27.655,26.996 L30.154,26.996 C31.267,26.996 31.545,26.675 31.545,25.594 L31.545,22.264 L32.283,21.880 C33.218,21.439 33.998,20.163 33.998,16.576 C33.998,12.898 33.673,12.412 33.374,11.867 ZM6.579,18.304 C5.383,18.304 4.409,17.384 4.409,16.254 C4.409,15.124 5.383,14.204 6.579,14.204 C7.775,14.204 8.749,15.124 8.749,16.254 C8.749,17.384 7.775,18.304 6.579,18.304 ZM27.501,18.304 C26.305,18.304 25.332,17.384 25.332,16.254 C25.332,15.124 26.305,14.204 27.501,14.204 C28.698,14.204 29.672,15.124 29.672,16.254 C29.672,17.384 28.698,18.304 27.501,18.304 ZM29.916,10.699 L4.83,10.699 C4.83,10.699 3.466,10.754 3.654,10.128 C3.812,9.600 6.32,4.391 6.498,2.961 C6.687,2.469 7.759,1.339 9.208,1.423 L24.790,1.423 C26.239,1.339 27.312,2.469 27.500,2.961 C27.968,4.391 30.187,9.600 30.345,10.128 C30.532,10.754 29.916,10.699 29.916,10.699 Z"/>
                        </svg>
                    </div>
                    <div class="useful-item__des">
                        <p class="useful-item__des-title">How to travel around</p>
                        <p class="useful-item__des-content">
                            Traffic and practical info to travel during your travel in Vietnam
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="inspiration-service">
        <div class="inspiration-service-title">
            <h2 class="inspiration-service-title__name">
                <span>Travel Inspiration</span>

                <img data-src="{{asset('clients/assets/images/inspiration/inspiration-line.png')}}" alt="" />
            </h2>

            <p class="inspiration-service-title__des">
                Some of the best articles about traveling to Indochina countries
                eploring its magnificence and hospitality from one country to
                another.
            </p>
        </div>
        <div class="inspirations ">
            @if($postFirstInHome)
                <div class="inspiration">
                    <div class="inspiration__img">
                        <img data-src="{{$postFirstInHome->thumbnail_path}}" alt="" class="lazyload"/>
                    </div>
                    <div class="inspiration__des">
                        <p class="inspiration__des-title"><span>{{$postFirstInHome->name}}</span></p>
                        <p class="inspiration__des-des">
                            {{$postFirstInHome->desc}}
                        </p>
                        <a href="{{route('client.slug',$postFirstInHome->slug_custom_name)}}" class="inspiration__des-link">Read more
                            ></a>
                    </div>
                </div>
            @endif
            @foreach($posts as $item)
                <div class="inspiration">
                    <div class="inspiration__img">
                        <img data-src="{{$item->thumbnail_path}}" alt="{{$item->name}}" class="lazyload"/>
                    </div>
                    <div class="inspiration__des">
                        <p class="inspiration__des-title"><span>{{$item->name}}</span></p>
                        <p class="inspiration__des-des">
                            {{$item->desc}}
                        </p>
                        <a href="{{route('client.slug',$item->slug_custom_name)}}" class="inspiration__des-link">Read more
                            ></a>
                        @if($loop->last)
                        <div class="d-flex justify-content-end">
                            <button class="btn inspiration-more-btn" href="{{$item->slug_category_link}}">MORE TRAVEL ARTICLES >></button>
                        </div>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>

        </div>
    </section>

    <section class="customer-rate-service">
        <div class="customer-rate-service-title">
            <p class="customer-rate-service-title__name">
                Valuation of our Vietnam Tour by customers
            </p>
            <p class="cutomer-rate-service-title__des">
                See what our customers said aobut our services from a short trip
                Halong Bay till the long journey across indochina
            </p>
        </div>
        <div class="customer-rates">

            <div class="customer-rates-left">
                <div class="customer-rate ">
                    <div class="customer-rate__img">
                        <img data-src="{{asset('clients/assets/images/customerrate/customer1.png')}}" class="lazyload" alt=""/>
                    </div>
                    <div class="customer-rate__des">
                       <div style="width: 19px;height: 19px">
                           <svg
                               xmlns="http://www.w3.org/2000/svg"
                               xmlns:xlink="http://www.w3.org/1999/xlink"
                               width="19px"
                               height="19px"
                           >
                               <path
                                   fill-rule="evenodd"
                                   fill="rgb(249, 245, 246)"
                                   d="M9.497,18.999 C4.261,18.999 0.0,14.737 0.0,9.500 C0.0,4.262 4.261,0.0 9.497,0.0 C14.737,0.0 18.999,4.262 18.999,9.500 C18.999,14.737 14.737,18.999 9.497,18.999 ZM9.497,0.872 C4.741,0.872 0.871,4.742 0.871,9.500 C0.871,14.257 4.741,18.127 9.497,18.127 C14.256,18.127 18.128,14.257 18.128,9.500 C18.128,4.742 14.256,0.872 9.497,0.872 ZM9.403,9.625 L3.345,13.343 L9.499,2.682 L15.653,13.343 L9.403,9.625 Z"
                               />
                           </svg>
                       </div>
                        <p class="customer-rate__name">
                            Family Cleverson - 12 days Vietnam Tour Tailor Made.
                        </p>
                    </div>
                </div>

                <div class="customer-rate">
                    <div class="customer-rate__img">
                        <img data-src="{{asset('clients/assets/images/customerrate/cutomer2.png')}}" class="lazyload" alt=""/>
                    </div>
                    <div class="customer-rate__des">
                        <div style="width: 19px;height: 19px">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink"
                            width="19px"
                            height="19px"
                        >
                            <path
                                fill-rule="evenodd"
                                fill="rgb(249, 245, 246)"
                                d="M9.497,18.999 C4.261,18.999 0.0,14.737 0.0,9.500 C0.0,4.262 4.261,0.0 9.497,0.0 C14.737,0.0 18.999,4.262 18.999,9.500 C18.999,14.737 14.737,18.999 9.497,18.999 ZM9.497,0.872 C4.741,0.872 0.871,4.742 0.871,9.500 C0.871,14.257 4.741,18.127 9.497,18.127 C14.256,18.127 18.128,14.257 18.128,9.500 C18.128,4.742 14.256,0.872 9.497,0.872 ZM9.403,9.625 L3.345,13.343 L9.499,2.682 L15.653,13.343 L9.403,9.625 Z"
                            />
                        </svg>
                        </div>
                        <p class="customer-rate__name">
                            Ms. Larsson & Friend - 10 days north Vietnam
                        </p>
                    </div>
                </div>
                <div class="customer-rate ">
                    <div class="customer-rate__img">
                        <img data-src="{{asset('clients/assets/images/customerrate/customer4.png')}}" class="lazyload" alt=""/>
                    </div>
                    <div class="customer-rate__des">
                        <div style="width: 19px;height: 19px">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink"
                            width="19px"
                            height="19px"
                        >
                            <path
                                fill-rule="evenodd"
                                fill="rgb(249, 245, 246)"
                                d="M9.497,18.999 C4.261,18.999 0.0,14.737 0.0,9.500 C0.0,4.262 4.261,0.0 9.497,0.0 C14.737,0.0 18.999,4.262 18.999,9.500 C18.999,14.737 14.737,18.999 9.497,18.999 ZM9.497,0.872 C4.741,0.872 0.871,4.742 0.871,9.500 C0.871,14.257 4.741,18.127 9.497,18.127 C14.256,18.127 18.128,14.257 18.128,9.500 C18.128,4.742 14.256,0.872 9.497,0.872 ZM9.403,9.625 L3.345,13.343 L9.499,2.682 L15.653,13.343 L9.403,9.625 Z"
                            />
                        </svg>
                        </div>
                        <p class="customer-rate__name">
                            Mr. Harrison - 9 days Vietnam Hill Tribe Trek
                        </p>
                    </div>
                </div>

                <div class="customer-rate ">
                    <div class="customer-rate__img">
                        <img data-src="{{asset('clients/assets/images/customerrate/customer2.png')}}" class="lazyload" alt=""/>
                    </div>
                    <div class="customer-rate__des">
                        <div style="width: 19px;height: 19px">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink"
                            width="19px"
                            height="19px"
                        >
                            <path
                                fill-rule="evenodd"
                                fill="rgb(249, 245, 246)"
                                d="M9.497,18.999 C4.261,18.999 0.0,14.737 0.0,9.500 C0.0,4.262 4.261,0.0 9.497,0.0 C14.737,0.0 18.999,4.262 18.999,9.500 C18.999,14.737 14.737,18.999 9.497,18.999 ZM9.497,0.872 C4.741,0.872 0.871,4.742 0.871,9.500 C0.871,14.257 4.741,18.127 9.497,18.127 C14.256,18.127 18.128,14.257 18.128,9.500 C18.128,4.742 14.256,0.872 9.497,0.872 ZM9.403,9.625 L3.345,13.343 L9.499,2.682 L15.653,13.343 L9.403,9.625 Z"
                            />
                        </svg>
                        </div>
                        <p class="customer-rate__name">
                            Mr. Ahmed - 8 days Vietnam Holiday
                        </p>
                    </div>
                </div>

                <div class="customer-rate customer-rate--mutil">
                    <div class="customer-rate__img">
                        <img data-src="{{asset('clients/assets/images/customerrate/customer5.png')}}" class="lazyload" alt=""/>
                    </div>
                    <div class="customer-rate__des">
                        <div style="width: 19px;height: 19px">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink"
                            width="19px"
                            height="19px"
                        >
                            <path
                                fill-rule="evenodd"
                                fill="rgb(249, 245, 246)"
                                d="M9.497,18.999 C4.261,18.999 0.0,14.737 0.0,9.500 C0.0,4.262 4.261,0.0 9.497,0.0 C14.737,0.0 18.999,4.262 18.999,9.500 C18.999,14.737 14.737,18.999 9.497,18.999 ZM9.497,0.872 C4.741,0.872 0.871,4.742 0.871,9.500 C0.871,14.257 4.741,18.127 9.497,18.127 C14.256,18.127 18.128,14.257 18.128,9.500 C18.128,4.742 14.256,0.872 9.497,0.872 ZM9.403,9.625 L3.345,13.343 L9.499,2.682 L15.653,13.343 L9.403,9.625 Z"
                            />
                        </svg>
                        </div>
                        <p class="customer-rate__name">
                            Mrs. Orma - 12 days Vietnam Adventure
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('script')
    <script defer src="
https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js
"></script>

    <script >

         $(()=>{

             $('.single-item').slick({
                 dots: false,
                 infinite: true,
                 speed: 3000,
                 slidesToShow: 1,
                 slidesToScroll: 1,
                 arrows: false,
                 autoplay: true,
                 autoplaySpeed: 2000,
             });
             let imageWidth = document.querySelector('.tour-item__img').clientWidth;
             let heightImage = +imageWidth * 1.4;
             if(window.innerWidth > 567)
             {
                 $('.tour-item__img').css('height', `${heightImage}px`)
             }

             const resizeObserver = new ResizeObserver(entries => {
                 for (let entry of entries) {
                     let imageWidth = document.querySelector('.tour-item__img').clientWidth;
                     let heightImage = +imageWidth * 1.4;
                     if(window.innerWidth > 567)
                     {
                         $('.tour-item__img').css('height', `${heightImage}px`)
                     }
                 }
             });
             resizeObserver.observe(document.body); //
         })
    </script>
@endsection
