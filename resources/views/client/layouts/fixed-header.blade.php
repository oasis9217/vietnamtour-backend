<div class="fixed-header">
    <div class="header-top container">
        <a class="header__logo" href="{{route('client.home')}}">
            <img src="{{asset('clients/assets/images/header_logo.png')}}" alt=""/>
        </a>
        <div class="header__direct flex-grow-1 flex-sm-grow-0">
            <div class="header__direct-top header-contact">
                <p class="header-contact-phone">
                    Speak to US: <a href="tel:+84919666568">+ 84 - 919666568</a>
                </p>
                <a class="header-contact-email" href="{{route('client.slug','for-travel-agent')}}">
                    For Travel Agent
                </a>
            </div>
            <nav
                class="navbar navbar-expand-lg"
                aria-label="Light offcanvas navbar"
            >
                <div class="container-fluid">
                    <a class="navbar-brand" href="#"></a>
                    <button
                        class="navbar-toggler"
                        type="button"
                        data-bs-toggle="offcanvas"
                        data-bs-target="#offcanvasNavbarLight"
                        aria-controls="offcanvasNavbarLight"
                    >
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div
                        class="offcanvas offcanvas-end"
                        tabindex="-1"
                        id="offcanvasNavbarLight"
                        aria-labelledby="offcanvasNavbarLightLabel"
                    >
                        <div class="offcanvas-header">
                            <a class="offcanvas-title" id="offcanvasNavbarLightLabel" href="{{route('client.home')}}">
                                <img src="{{asset('clients/assets/images/header_logo.png')}}" alt=""/>
                            </a>
                            <button
                                type="button"
                                class="btn-close"
                                data-bs-dismiss="offcanvas"
                                aria-label="Close"
                            ></button>
                        </div>

                        <div class="offcanvas-body">
                            <ul
                                class="navbar-nav justify-content-start flex-grow-1"
                            >
                                @foreach($categories as $category)
                                    <li class="nav-item">
                                        <a class="nav-link " aria-current="page"
                                           href="{{route('client.slug',$category->slug_custom_name)}}"
                                        >{{$category->name}}
                                        </a>

                                        @if($category->childrens->count() >0)
                                            <div class="category-submenu">
                                                @foreach($category->childrens as $item)
                                                    <div class="category-submenu-item">
                                                        <a href="{{route('client.slug', $item->slug_custom_name)}}"
                                                           class="category-submenu-item__name">{{$item->name}}</a>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endif
                                    </li>
                                @endforeach

                                <li class="nav-item">
                                    <a class="nav-link"
                                       href="{{route('client.slug',$postCategory->slug_custom_name)}}">{{$postCategory->name}}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('client.customize_tour')}}"> Customize Tour</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('client.slug','contact-us')}}">Contact</a>
                                </li>

                            </ul>

                            <div class="accordion accordion-flush" id="accordionFlushMenuNav">
                                @foreach($categories as $category)
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="flush-headingOne{{$category->id}}">
                                            <a
                                                class="accordion-button collapsed"
                                                @if($category->childrens->count() >0)
                                                type="button"
                                                data-bs-toggle="collapse"
                                                data-bs-target="#flush-collapseOne{{$category->id}}"
                                                aria-expanded="false"
                                                aria-controls="flush-collapseOne{{$category->id}}"
                                                @endif
                                                href="{{route('client.slug',$category->slug_custom_name)}}"
                                            >
                                                {{$category->name}}
                                            </a>
                                        </h2>
                                        @if($category->childrens->count() >0)

                                            <div
                                                id="flush-collapseOne{{$category->id}}"
                                                class="accordion-collapse collapse"
                                                aria-labelledby="flush-headingOne{{$category->id}}"
                                                data-bs-parent="#accordionFlushMenuNav"
                                            >
                                                <div class="accordion-body">
                                                    <div class="accordion accordion-flush" id="accordionFlushMenuNav1">
                                                        @foreach($category->childrens as $item)
                                                            <div class="accordion-item">
                                                                <h2 class="accordion-header"
                                                                    id="flush-headingOne{{$item->id}}">
                                                                    <a
                                                                        class="accordion-button collapsed"
                                                                        href="{{$item->slug_link}}"
                                                                    >
                                                                        {{$item->name}}
                                                                    </a>
                                                                </h2>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="flush-headingOneCustomize1">
                                        <a
                                            class="accordion-button collapsed"
                                            href="{{route('client.slug',$postCategory->slug_custom_name)}}"
                                        >{{$postCategory->name}}
                                        </a>
                                    </h2>
                                </div>

                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="flush-headingOneCustomize2">
                                        <a
                                            class="accordion-button collapsed"
                                            href="{{route('client.customize_tour')}}"
                                        >Customize Tour
                                        </a>
                                    </h2>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="flush-headingOneCustomize3">
                                        <a
                                            class="accordion-button collapsed"
                                            href="{{route('client.slug','contact-us')}}"
                                        >Contact
                                        </a>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>
