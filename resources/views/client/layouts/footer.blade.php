<footer class="footer container">
    <div class="footer-info row">
        <div class="footer-info-item col-12 col-sm-6">
            <div class="footer-info-item-logo">
                <div class="footer-logo">
                    <img src="{{asset('clients/assets/images/header_logo.png')}}" alt="" />
                </div>
                <div class="footer-info-item-logo__des">
                    <p>AUTHENTIC ASIA TOURS</p>
                    <p>Unique & Authentic Travel Experiences</p>
                </div>
            </div>
            <div class="footer-info-item__des">
                Experts in the art of tailor-made holidays, we delight in putting
                Asia for those looking to get under the skin of this magical
                continent. Our team of experienced travel professionals are
                passionate about creating intelligent
            </div>
            <div class="footer-info-item__contact">
                <div class="footer-info-item__contact-item">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        width="16px" height="16px">
                        <image  opacity="0.8" x="0px" y="0px" width="16px" height="16px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAkFBMVEVMVhRMVhVMVRRLVRRYRB52Qy94Kz97Lz1YRR5xUCt2KEB3KUF/OjFXRB13KUBWTR13KECBNzaBOjRXSB5/NzV7QzVkQSNYVh99MDx7NTqAOTRXSR6ANzVXRB57Mzt9MTtgUyJ6LT9jQCJ8OTl9NzRoUyaCODZgUCKCNzdYSB56MTt0OStaTh97OTd0Oi3///90XbuSAAAAL3RSTlMAAAAAQoLGvEJvzMyHQsw9zJ+UP5qXVT26spQ/mkK0tErEVayaV5pNoj+6dUKkfff5TMwAAAABYktHRC8j1CARAAAAB3RJTUUH5wICBwQMf8mF2wAAAKhJREFUGNMtjtcSQjEIROEmagRB7L33+v+fJ2TkIZPdYc8CkFvtTsEYqFO6xNwrAG5gPEKsShmhcS8lhL5rowGErLE+k5IOfflvjIiJxznygUxQJmw0DWZyGeaMyeYSDMTGjSQLDy3Fv02NIayUldeCm+0ul6DkfXAPx5P3nStdLvUWZVO71iq5mUZOie9B953H05Td4ldU+wVY3h8/WL+51oeXshQRwR+/dAi9yYcdMwAAAABJRU5ErkJggg==" />
                    </svg>
                    <p> <a href="tel:+84919666568" >+ 84 - 919 666 568</a></p>
                </div>
                <div class="footer-info-item__contact-item">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        width="18px" height="13px">
                        <path fill-rule="evenodd"  fill="rgb(128, 28, 62)"
                              d="M17.645,13.0 L0.354,13.0 C0.158,13.0 0.0,12.831 0.0,12.622 L0.0,0.377 C0.0,0.169 0.158,0.0 0.354,0.0 L17.645,0.0 C17.841,0.0 18.0,0.169 18.0,0.377 L18.0,12.622 C18.0,12.831 17.841,13.0 17.645,13.0 ZM16.977,12.245 L11.746,5.28 L9.11,6.973 L6.233,5.2 L0.992,12.245 L16.977,12.245 ZM0.709,11.396 L5.641,4.582 L0.709,1.83 L0.709,11.396 ZM1.526,0.755 L9.0,6.59 L16.474,0.755 L1.526,0.755 ZM12.338,4.607 L17.290,11.438 L17.290,1.84 L12.338,4.607 Z"/>
                    </svg>
                    <a href="mailto:info@vietnamtour.asia">Info@vietnamtour.asia</a>
                </div>
            </div>
        </div>
        <div class="footer-info-item col-12 col-sm-3">
            <p class="footer-info-item-title">OUR DESTINATIONS</p>
            <div class="footer-info-item-menu">
                <a >Vietnam &gt;</a>
                <a >Cambodia &gt;</a>
                <a >Laos &gt;</a>
                <a >Myanmar &gt;</a>
                <a >China &gt;</a>
                <a href="{{route('client.slug','multi-country-tours')}}">Multi Countries Tours &gt;</a>
            </div>
        </div>
        <div class="footer-info-item col-12 col-sm-3">
            <p class="footer-info-item-title">OUR VIETNAM TOURS</p>
            <div class="footer-info-item-menu">
                @foreach($categoriesTour as $item)
                <a href="{{$item->slug_link}}">{{$item->name}} &gt;</a>
                @endforeach
            </div>
        </div>
    <div class="coppyright">
        <p class="coppyright-item">
            <span class="mr-1">&copy;</span>
            Authentic Asia Tours - All copyright Reserved

        </p>
        <a class="coppyright-item" href="{{route('client.slug','term')}}">Terms and Conditions </a>
        <a class="coppyright-item" href="{{route('client.slug','privacy-policy')}}" href="">Privacy Policy</a>
    </div>
</footer>
