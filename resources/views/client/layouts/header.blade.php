<header id="header" class="">
    @yield('header_image')
    <div class="header-content">
        @include('client.layouts.fixed-header')
        @yield('header_banner')

    </div>
</header>
