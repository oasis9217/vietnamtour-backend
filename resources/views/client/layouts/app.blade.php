<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>@yield('title', "VietNamTour")</title>

    @yield('meta')

{{--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@6.2.1/css/fontawesome.min.css" integrity="sha384-QYIZto+st3yW+o8+5OHfT6S482Zsvz2WfOzpFSXMF9zqeLcFV0/wlZpMtyFcZALm" crossorigin="anonymous">--}}
    <link rel="prefetch" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@6.2.1/css/fontawesome.min.css" as="style">


    <link
        rel="preload"
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
        type="text/css"
        as="style"
        onload="this.onload=null;this.rel='stylesheet';"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65"
        crossorigin="anonymous"
    />


    <link  rel="preload"   as="style"  href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@400;500;600;700&family=Itim&family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

    <link  rel="preload"   as="style"   href="{{asset('admin/font.css')}}">

    @yield('style')
</head>
<body>
@include('client.layouts.header')
<main id="main">
   @yield('main')
</main>
@include('client.layouts.footer')
<script  src="https://code.jquery.com/jquery-3.6.3.min.js" integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>

<script
    defer
    src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4"
    crossorigin="anonymous"
></script>
<script src="{{asset('plugins/lazy-image/lazysizes.min.js')}}"></script>
<script defer src='https://www.google.com/recaptcha/api.js'></script>


<script>
    const ck=`{{config('data.capcha_key')}}`;
</script>
@yield('script')
<script>
    window.addEventListener('scroll', function(e) {
        let scrollValue = window.scrollY || window.pageYOffset;
        if(scrollValue > 0)
        {
            let fixedHeader = $('.fixed-header')
            if(!fixedHeader.hasClass('opacacity-1'))
            {
                fixedHeader.addClass('opacacity-1')
            }
        }else {
            let fixedHeader = $('.fixed-header')
            if(fixedHeader.hasClass('opacacity-1'))
            {
                fixedHeader.removeClass('opacacity-1')
            }
        }

    });
    $(()=>{

        let widthImageBanner = document.querySelector('.header-image');
        let heightImageBaner = +widthImageBanner.clientWidth * (620/1366);

        const resizeObserverApp = new ResizeObserver(entries => {
            for (let entry of entries) {
                let widthImageBanner = document.querySelector('.header-image');
                let heightImageBaner = +widthImageBanner.clientWidth * (620/1366);
                if(window.innerWidth > 567)
                {
                    $('.header-image').css('height', `${heightImageBaner}px`)
                }
            }
        });
        resizeObserverApp.observe(document.body); // Add this line if the image is initially hidden
        if(window.innerWidth > 567)
        {
            $('.header-image').css('height', `${heightImageBaner}px`)
        }
    })

</script>
</body>
</html>
