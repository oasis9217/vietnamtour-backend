@section('style')
    <link rel="stylesheet" href="{{asset('clients/assets/css/custom_tour.css')}}"/>
@endsection
@section('meta')
    <meta name="keywords" content=" Customize Vietnam Tour, Personalize Vietnam Trip">
    <meta name="description"
          content=" Tailor Made Vietnam Holiday with one of the best travel experts who understand well your travel interests."/>
@endsection
@section('title')
    Customize Tour
@endsection
@extends('client.layouts.app')
@section('header_image')
    <div class="header-image">
{{--        <img data-src="{{asset('clients/assets/images/header_bg.png')}}" class="img-response lazyload" alt=""/>--}}
        <picture>
            <source
                media="(max-width: 767px)"
                data-srcset="{{asset('clients/assets/images/header_bg_sm.png')}}"
                data-sizes="auto"
                class="lazyload"
            />
            <source
                media="(min-width: 768px)"
                data-srcset="{{asset('clients/assets/images/header_bg.png')}}"
                data-sizes="auto"
                class="lazyload"
            />
            <img
                src="{{asset('clients/assets/images/header_bg.png')}}"
                alt="Image description"
                class="img-response lazyload"
            />
        </picture>
    </div>
@endsection
@section('header_banner')
    <div class="header-banner">
        <p class="header-banner__slogan">See Vietnam Uniquely</p>
        <p class="header-banner__des">
            Fully tailor made tour to Vietnam and its neighboring countries
        </p>
        <div class="btn-group header-banner__btn" role="group">
            <a href="{{route('client.slug',$touCategoryFirst->slug_custom_name)}}" class="btn">Vietnam Tours</a>
            <a href="{{route('client.customize_tour')}}" class="btn">Tailor Made</a>
        </div>
    </div>
@endsection
@section('main')

    <section class="contact-service">
        <div class="contact-sevice-info">
            <div class="contact-avatar">
                <div class="contact-avatar__img">
                    <img
                        data-src="{{asset('clients/assets/images/avatar/avatar1.png')}}"
                        class="img-fluid img-round lazyload"
                        alt=""
                    />
                </div>
                <div class="contact-avatar__img">
                    <img
                        data-src="{{asset('clients/assets/images/avatar/avatar2.png')}}"
                        class="img-fluid img-round lazyload"
                        alt=""
                    />
                </div>
                <div class="contact-avatar__img">
                    <img
                        data-src="{{asset('clients/assets/images/avatar/avatar3.png')}}"
                        class="img-fluid img-round lazyload"
                        alt=""
                    />
                </div>
            </div>
            <p class="contact-service-title">
                Tailormade Tour with inside Travel Experts
            </p>
            <p class="contact-service-des">
                We are happy to join hands with you to plan your beautiful trip to
                Vietnam. We strongly believe
            </p>
            <div class="contact-service-ques">
                <p>Who are we ?</p>
                <p>Why Travel with us ?</p>
                <p>What makes us different?</p>
            </div>
        </div>

        <section class="bookingService container">
            <div class="top-category col-12">
                <div class="desc-category">
                    Our Authentic Asia Tour experts are very pleased to craft an innovative and ground breaking holidays
                    for your ultimate travel experience to our country. Whether you are looking for a luxurious trip
                    encompassing the highlighted attractions or an inspiring adventure that lasts for a life time, we
                    have always striked to offer you something remarkable.
                </div>
            </div>
            <div class="row">

                <div class="col-sm-8 col-12 card ">
                    @include('flash::message')

                    <form method="post" id="book-tour-form" action="{{route('client.tours.book_tour')}}">
                        <div class="col-12">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                        @csrf
                        <h1 class="text-center title-section">Customize your Vietnam Tour Package</h1>
                        {{--                        <input type="hidden" name="tour_id" value="{{$tour->id}}">--}}
                        <div class="title-contactinfo">Your travel plan:</div>
                        <div class="row mb-3">
                            <div class="col-sm-3 col-12">Departure date: <span
                                    class="text-danger">*</span>
                            </div>
                            <div class="col-sm-7 col-12 d-flex">
                                <div>
                                    <input type="date" name="departure_date" id="departure_date"
                                           value="{{old('departure_date')}}"

                                           class="start_date_detail form-control"/>
                                </div>
                                <div class="form-check ms-3 w-fit-content">
                                    <input class="form-check-input" name="is_flexible_date" type="checkbox" value="1"
                                        {{old('is_flexible_date') == '1' ?'checked' :''}}
                                    />
                                    <label class="form-check-label" for="flexCheckDefault">
                                        Flexible date
                                    </label>
                                </div>

                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-3 col-12">Number of person:</div>
                            <div class="col-sm-8 col-12 d-flex">
                                <select class="select_adult_detail me-2 form-select w-fit-content" name="adult">
                                    @for($index = 0; $index <= 10; $index++)
                                        <option
                                            {{old('adult') == $index ? 'selected' :''}} value="{{$index}}">{{$index}}
                                            adult{{$index > 2 ?'s':''}}</option>
                                    @endfor
                                </select>
                                <select class="select_adult_detail me-2 form-select w-fit-content" name="child">
                                    @for($index = 0; $index <= 10; $index++)
                                        <option
                                            {{old('child') == $index ? 'selected' :''}} value="{{$index}}">{{$index}}
                                            child{{$index > 2 ?'s':''}}</option>
                                    @endfor
                                </select>
                                <select class="select_adult_detail form-select w-fit-content" name="child_baby">

                                    @for($index = 0; $index <= 10; $index++)
                                        <option
                                            {{old('child_baby') == $index ? 'selected' :''}} value="{{$index}}">{{$index}}
                                            child{{$index > 2 ?'babies':'baby'}}</option>
                                    @endfor

                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-3 col-12">Hotel Request:</div>
                            <div class="col-sm-3 col-12 d-flex">
                                <select class="form-select w-fit-content me-2" name="hotel_room_quantity">
                                    @for($index = 1; $index <= 10; $index++)
                                        <option
                                            {{old('hotel_room_quantity') == $index ? 'selected' :''}} value="{{$index}}">{{$index}}
                                            room{{$index > 2 ?'s':''}}</option>
                                    @endfor
                                </select>
                                <select class="form-select w-fit-content" name="hotel_class">
                                    <option value="">-- Hotel Class --</option>
                                    @foreach(config('data.client_hotel_class')  as $item)
                                        <option
                                            {{old('hotel_class') == $item ? 'selected' :''}}  value="{{$item}}">{{$item}}</option>

                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-3 col-12">International flights:</div>
                            <div class="col-sm-3 col-12">
                                <div class="form-check ms-3">
                                    <input class="form-check-input" name="international_flight" type="radio"
                                           {{old('international_flight') == '1' ?'checked' :''}}
                                           value="1"
                                    >
                                    <label class="form-check-label" for="flexCheckDefault">
                                        Included
                                    </label>
                                </div>
                                <div class="form-check ms-3 ">
                                    <input class="form-check-input" name="international_flight" type="radio" value="0"
                                        {{old('international_flight') == '0' ?'checked' :''}}
                                    >
                                    <label class="form-check-label" for="flexCheckDefault">
                                        Not included
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row row-contact">
                            <div class="col-sm-3 col-12">Tell us more:</div>
                            <div class=" col-sm-7 col-12">
                                <p>
                                    Any specific destinations, interests, budget, activities or special occasions,... =&gt;
                                    for a dream trip </p>
                                <textarea name="desc" id="request" rows="4"
                                          class="form-control">{{old('desc')}}</textarea>
                            </div>
                        </div>
                        <div class="tourbox-contactinfo" style="margin-top: 30px">
                            <div class="title-contactinfo">Contact information</div>
                            <div class="clearfix row_book">
                                <div class="row_book_2 clearfix">
                                    <div class="row row-contact mb-3">
                                        <div class="col-sm-3 col-12">Your name: <span class="text-danger">*</span></div>
                                        <div class="col-sm-7 col-12">
                                            <input type="text" value="{{old('name')}}" name="name" id="name"
                                                   class="contact-input form-control"
                                                   style="border: 1px solid red;"/>
                                        </div>
                                    </div>
                                    <div class="row row-contact mb-3">
                                        <div class="col-sm-3 col-12">Your email: <span class="text-danger">*</span>
                                        </div>
                                        <div class="col-sm-7 col-12">
                                            <input type="email" value="{{old('email')}}" name="email" id="email"
                                                   class="contact-input form-control"
                                                   style="border: 1px solid red;"/>
                                        </div>
                                    </div>
                                    <div class="row row-contact mb-3">
                                        <div class="col-sm-3 col-12">Your phone:</div>
                                        <div class="col-sm-7 col-12">
                                            <input value="{{old('phone_number')}}" type="text" name="phone_number"
                                                   id="phone" class="contact-input form-control"/>
                                        </div>
                                    </div>
                                    <div class="row row-contact mb-3">
                                        <div class="col-sm-3 col-12">Country of residence: <span
                                                class="text-danger">*</span></div>
                                        <div class="col-sm-7 col-12">
                                            <select data-live-search="true" name="country" id="country"
                                                    class=" form-control select_country selectpicker"
                                                    style="border: 1px solid red;">
                                                <option value="" selected="selected">Select a Country</option>
                                                @foreach(config('data.countries') as $item)
                                                    <option
                                                        {{old('country') == $item['name'] ? 'selected' :''}}  value="{{$item['name']}}">{{$item['name']}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-contact mb-3">
                                <div class="col-sm-3 col-12"></div>
                                <div class="col-sm-7 col-12">
                                    <div style="padding-top: 30px;padding-bottom: 20px" class="text-center">
                                        * Your privacy is respected and protected
                                    </div>
                                    @include('client.page.capcha')
                                    <div class="d-flex justify-content-center ">
                                        <button type="submit"
                                                class="btn-submit-form btn">
                                            Get my trip planned >>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-4 col-12">
                    <div style="padding: 20px;background: #f5f5f5;margin-bottom: 20px">
                        <h3 style="margin-top: 0">Some Advantages of our Tours&nbsp;</h3>
                        For any request, you will be in touch with&nbsp;an experience travel expert who knows&nbsp;Myanmar
                        like no other.<br>
                        <br>
                        You are free to make changes until you&nbsp;are happy with your travel itinerary, service. All
                        details are customizable and there is no hassle or commercial action
                    </div>

                    <div style="padding: 20px;background: #f5f5f5;margin-bottom: 20px">
                        <h3 style="margin-top: 0">You are in a good hand</h3>
                        " We committe the highest satisfaction from the first step you send us your inquiry till the
                        time
                        you travel and enjoy your holiday with us "<br>
                        <strong>- Mr. Zaw - Myanmar Travel Expert</strong></div>
                </div>
            </div>
        </section>

@endsection
@section('script')
            <script src="{{asset('clients/assets/js/capcha.js')}}"></script>
@endsection
