@section('style')
    <link rel="stylesheet" href="{{asset('clients/assets/css/booking.css')}}"/>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta3/dist/css/bootstrap-select.min.css">
@endsection
@section('title','VietNam Tour Booking tour')
@extends('client.layouts.app')

@section('header_banner')
    <div class="header-banner header-banner-tour"></div>
@endsection
@section('main')
    <section class="bookingService container">
        <div class="top-category col-12">
            <h1 class="bookingService-title">Booking tour</h1>
            <div class="desc-category">
                Please complete your booking by giving us the details of your expecting travel. Our travel specialists
                will contact you shortly to confirm or craft your tailor made tour within 24 hour.&nbsp;
            </div>
        </div>
        <div class="row">
            @include('flash::message')
            <div class="col-sm-8 col-12">
                <div class="booking-tour-info">
                    <div class="title-tour-info"><a href="">{{$tour->name}}</a></div>
                    <div style="margin-bottom: 5px">
                        <strong>Duration</strong>: {{$tour->day}}
                        day(s) {{$tour->day > 0 ? $tour->day - 1 :$tour->day}}
                        nights
                    </div>
                    <div style="margin-bottom: 5px">
                        <strong>Physical level</strong>: Hanoi
                    </div>
                    <div style="margin-bottom: 5px">
                        <strong>TOUR_CODE</strong>: TC6882
                    </div>
                </div>
                <div class="col-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <form method="post" id="book-tour-form" action="{{route('client.tours.book_tour')}}">
                    @csrf
                    <input type="hidden" name="tour_id" value="{{$tour->id}}">
                    <div class="title-contactinfo">YOUR TRAVEL PLAN:</div>
                    <div class="row mb-3">
                        <div class="col-sm-3 col-12">Departure date: <span
                                class="text-danger">*</span>
                        </div>
                        <div class="col-sm-9 col-12 d-flex">
                            <div>
                                <input type="date" name="departure_date" id="departure_date"
                                       value="{{old('departure_date')}}"
                                       class="start_date_detail form-control"/>
                            </div>
                            <div class="form-check ms-3 w-fit-content">
                                <input class="form-check-input" name="is_flexible_date" type="checkbox" value="1"
                                    {{old('is_flexible_date') == '1' ?'checked' :''}}
                                />
                                <label class="form-check-label" for="flexCheckDefault">
                                    Flexible date
                                </label>
                            </div>

                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-sm-3 col-12">Number of person:</div>
                        <div class="col-sm-9 col-12 d-flex">
                            <select class="select_adult_detail me-2 form-select w-fit-content" name="adult">
                                @for($index = 0; $index <= 10; $index++)
                                    <option {{old('adult') == $index ? 'selected' :''}} value="{{$index}}">{{$index}}
                                        adult{{$index > 2 ?'s':''}}</option>
                                @endfor
                            </select>
                            <select class="select_adult_detail me-2 form-select w-fit-content" name="child">
                                @for($index = 0; $index <= 10; $index++)
                                    <option {{old('child') == $index ? 'selected' :''}} value="{{$index}}">{{$index}}
                                        child{{$index > 2 ?'s':''}}</option>
                                @endfor
                            </select>
                            <select class="select_adult_detail form-select w-fit-content" name="child_baby">

                                @for($index = 0; $index <= 10; $index++)
                                    <option
                                        {{old('child_baby') == $index ? 'selected' :''}} value="{{$index}}">{{$index}}
                                        child{{$index > 2 ?'babies':'baby'}}</option>
                                @endfor

                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-sm-3 col-12">Hotel Request:</div>
                        <div class="col-sm-3 col-12 d-flex">
                            <select class="form-select w-fit-content me-2" name="hotel_room_quantity">
                                @for($index = 1; $index <= 10; $index++)
                                    <option
                                        {{old('hotel_room_quantity') == $index ? 'selected' :''}} value="{{$index}}">{{$index}}
                                        room{{$index > 2 ?'s':''}}</option>
                                @endfor
                            </select>
                            <select class="form-select w-fit-content" name="hotel_class">
                                <option value="">-- Hotel Class --</option>
                                @foreach(config('data.client_hotel_class')  as $item)
                                    <option
                                        {{old('hotel_class') == $item ? 'selected' :''}}  value="{{$item}}">{{$item}}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-sm-3 col-12">International flights:</div>
                        <div class="col-sm-3 col-12">
                            <div class="form-check ms-3">
                                <input class="form-check-input" name="international_flight" type="radio"
                                       {{old('international_flight') == '1' ?'checked' :''}}
                                       value="1"
                                >
                                <label class="form-check-label" for="flexCheckDefault">
                                    Included
                                </label>
                            </div>
                            <div class="form-check ms-3 ">
                                <input class="form-check-input" name="international_flight" type="radio" value="0"
                                    {{old('international_flight') == '0' ?'checked' :''}}
                                >
                                <label class="form-check-label" for="flexCheckDefault">
                                    Not included
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row row-contact">
                        <div class="col-sm-3 col-12">Tell us more:</div>
                        <div class=" col-sm-7 col-12">
                            <p>
                                Any specific destinations, interests, budget, activities or special occasions,... =&gt;
                                for a dream trip </p>
                            <textarea name="desc" id="request" rows="4" class="form-control">{{old('desc')}}</textarea>
                        </div>
                    </div>
                    <div class="tourbox-contactinfo" style="margin-top: 30px">
                        <div class="title-contactinfo">Contact information</div>
                        <div class="clearfix row_book">
                            <div class="row_book_2 clearfix">
                                <div class="row row-contact mb-3">
                                    <div class="col-sm-3 col-12">Your name: <span class="text-danger">*</span></div>
                                    <div class="col-sm-7 col-12">
                                        <input type="text" value="{{old('name')}}" name="name" id="name"
                                               class="contact-input form-control"
                                               style="border: 1px solid red;"/>
                                    </div>
                                </div>
                                <div class="row row-contact mb-3">
                                    <div class="col-sm-3 col-12">Your email: <span class="text-danger">*</span></div>
                                    <div class="col-sm-7 col-12">
                                        <input type="email" value="{{old('email')}}" name="email" id="email"
                                               class="contact-input form-control"
                                               style="border: 1px solid red;"/>
                                    </div>
                                </div>
                                <div class="row row-contact mb-3">
                                    <div class="col-sm-3 col-12">Your phone:</div>
                                    <div class="col-sm-7 col-12">
                                        <input value="{{old('phone_number')}}" type="text" name="phone_number"
                                               id="phone" class="contact-input form-control"/>
                                    </div>
                                </div>
                                <div class="row row-contact mb-3">
                                    <div class="col-sm-3 col-12">Country of residence: <span
                                            class="text-danger">*</span></div>
                                    <div class="col-sm-7 col-12">
                                        <select data-live-search="true" name="country" id="country"
                                                class=" form-control select_country selectpicker"
                                                style="border: 1px solid red;">
                                            <option value="" selected="selected">Select a Country</option>
                                            @foreach(config('data.countries') as $item)
                                                <option
                                                    {{old('country') == $item['name'] ? 'selected' :''}}  value="{{$item['name']}}">{{$item['name']}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row row-contact mb-3 mb-sm-0">
                            <div class="col-sm-3 col-12"></div>
                            <div class="col-sm-7 col-12">
                                <div style="padding-top: 30px;padding-bottom: 20px" class="text-center">
                                    * Your privacy is respected and protected
                                </div>
                                @include('client.page.capcha')
                                <div class="d-flex justify-content-center ">
                                    <button type="submit"
                                            class="btn-submit-form btn">
                                        Get my trip planned >>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-sm-4 col-12">
                <div style="padding: 20px;background: #f5f5f5;margin-bottom: 20px">
                    <h3 style="margin-top: 0">Some Advantages of our Tours&nbsp;</h3>
                    For any request, you will be in touch with&nbsp;an experience travel expert who knows&nbsp;Myanmar
                    like no other.<br>
                    <br>
                    You are free to make changes until you&nbsp;are happy with your travel itinerary, service. All
                    details are customizable and there is no hassle or commercial action
                </div>

                <div style="padding: 20px;background: #f5f5f5;margin-bottom: 20px">
                    <h3 style="margin-top: 0">You are in a good hand</h3>
                    " We committe the highest satisfaction from the first step you send us your inquiry till the time
                    you travel and enjoy your holiday with us "<br>
                    <strong>- Mr. Zaw - Myanmar Travel Expert</strong></div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script defer
            src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta3/dist/js/bootstrap-select.min.js"></script>
    <script src="{{asset('clients/assets/js/capcha.js')}}"></script>

@endsection
