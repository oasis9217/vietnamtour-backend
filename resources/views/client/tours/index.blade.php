@section('style')
    <link rel="stylesheet" href="{{asset('clients/assets/css/tour.css')}}"/>
@endsection
@section('title')
    {{$tourCategory->title ?? $tourCategory->name}}
@endsection
@section('meta')
    {!! $tourCategory->meta !!}
@endsection
@extends('client.layouts.app')
@section('header_image')
    <div class="header-image">
        <img data-src="{{asset('clients/assets/images/tours/banner.png')}}" class="img-response lazyload" alt=""/>
    </div>
@endsection
@section('header_banner')
    <div class="header-banner header-banner-tour">
        <p class="header-banner-tour__des">
            Vietnam Private Tour for discerning travelers
        </p>
        <div class="btn-group header-banner-tour__btn" role="group">
            <a href="{{route('client.customize_tour')}}" class="btn btn-outline-light px-4">
                Tailor Made
            </a>
        </div>
    </div>
@endsection
@section('main')
    <section class="tour-service container">
        <div class="tour-service-head">
            <div class="tour-service-head__title">
                <h1>{{$tourCategory->name}}</h1>
                <img
                    class="tour-service-head__line lazyload"
                    data-src="{{asset('clients/assets/images/tours/line_travelstyle.png')}}"
                    alt=""
                />
            </div>

            <div class="tour-service-head__des">
                <p>
                    The opening of Vietnam means you are free to explore more the
                    hiden gems of the country. From exotic markets to vibrant cities,
                    Vietnam is very tempting. Keep your best memory of Vietnam with
                    our carefully designed tours
                </p>
                <p>
                    From sun-drenched beaches to dramatic mountains, our Vietnam To
                    bring you to all the places of different characters. You`ll hav to
                    sample the best of our country like no others
                </p>
            </div>
        </div>

        <div class="recomment-tour tour-service-items container">
            <div class="recomment-tour-title">
                <p>BEST RECOMMENDED TOURS</p>
            </div>
            <div class="recomment-tour-items">
                <div
                    id="carouselTourRecoment"
                    class="carousel slide"
                    data-bs-ride="carousel"
                >
                    <div class="carousel-inner">
                        @foreach($toursHighLight->chunk(3) as $toursHighLightItem)
                            <div class="carousel-item {{$loop->first ?'active' :''}}">
                                <div class="tour-items row">
                                    @foreach($toursHighLightItem as $item)
                                        <div class="col-12 col-sm-4 col-md-4">
                                            <div class="tour-item">
                                                <a class="tour-item__img">
                                                    <img
                                                        data-src="{{$item->image_path}}"
                                                        alt="{{$item->name}}"
                                                        class="lazyload"
                                                    />
                                                </a>
                                                <p class="tour-item__name">{{$item->name}}</p>
                                                <div class="tour-item-des">
                                                    <div class="tour-item-des__day">
                                                        {{$item->day}} days
                                                    </div>
                                                    <div class="tour-item-des__content text-over3">
                                                        {{$item->desc}}
                                                    </div>
                                                    <a href="{{route('client.slug',$item->slug_custom_name)}}"
                                                       class="tour-item-link">View this tour</a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <button
                        class="carousel-control-prev"
                        type="button"
                        data-bs-target="#carouselTourRecoment"
                        data-bs-slide="prev"
                    >
                <span
                    class="carousel-control-prev-icon "
                    aria-hidden="true"
                >
                                       <svg
                                           style="transform: rotate(180deg);"
                                           xmlns="http://www.w3.org/2000/svg"
                                           xmlns:xlink="http://www.w3.org/1999/xlink"
                                           width="39px" height="39px">
<path fill-rule="evenodd"  fill="rgb(45, 45, 43)"
      d="M19.499,39.0 C8.747,39.0 0.0,30.252 0.0,19.499 C0.0,8.747 8.747,0.0 19.499,0.0 C30.252,0.0 38.999,8.747 38.999,19.499 C38.999,30.252 30.252,39.0 19.499,39.0 ZM19.499,1.559 C9.607,1.559 1.559,9.607 1.559,19.499 C1.559,29.392 9.607,37.440 19.499,37.440 C29.392,37.440 37.439,29.392 37.439,19.499 C37.439,9.607 29.392,1.559 19.499,1.559 ZM15.4,19.500 L21.995,14.5 L20.742,19.500 L21.995,24.994 L15.4,19.500 Z"/>
</svg>
                </span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button
                        class="carousel-control-next"
                        type="button"
                        data-bs-target="#carouselTourRecoment"
                        data-bs-slide="next"
                    >
                <span
                    class="carousel-control-next-icon"
                    aria-hidden="true"
                >
                           <svg
                               xmlns="http://www.w3.org/2000/svg"
                               xmlns:xlink="http://www.w3.org/1999/xlink"
                               width="39px" height="39px">
                        <path fill-rule="evenodd" fill="rgb(45, 45, 43)"
                              d="M19.500,38.999 C8.747,38.999 0.0,30.252 0.0,19.499 C0.0,8.747 8.747,0.0 19.500,0.0 C30.252,0.0 39.0,8.747 39.0,19.499 C39.0,30.252 30.252,38.999 19.500,38.999 ZM19.500,1.559 C9.607,1.559 1.560,9.607 1.560,19.499 C1.560,29.392 9.607,37.440 19.500,37.440 C29.392,37.440 37.440,29.392 37.440,19.499 C37.440,9.607 29.392,1.559 19.500,1.559 ZM18.257,19.499 L17.4,14.5 L23.995,19.499 L17.4,24.994 L18.257,19.499 Z"/>
                        </svg>
                </span>

                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
        </div>

        <div class="recomment-tour tour-service-items container">
            <div class="recomment-tour-title">
                <p>TRAVELLER`S CHOICES</p>
            </div>
            <div class="recomment-tour-items">
                <div
                    id="carouselTravelChoise"
                    class="carousel slide"
                    data-bs-ride="carousel"
                >
                    <div class="carousel-inner">

                        @foreach($toursSimilar->chunk(3) as $toursSimilarItem)
                            <div class="carousel-item {{$loop->first ?'active' :''}}">
                                <div class="tour-items row">
                                    @foreach($toursSimilarItem as $item)
                                        <div class="col-12 col-sm-4 col-md-4">
                                            <div class="tour-item">
                                                <a class="tour-item__img">
                                                    <img
                                                        data-src="{{$item->image_path}}"
                                                        alt="{{$item->name}}"
                                                        class="lazyload"
                                                    /> </a>
                                                <p class="tour-item__name">{{$item->name}}</p>
                                                <div class="tour-item-des">
                                                    <div class="tour-item-des__day">
                                                        {{$item->day}} days
                                                    </div>
                                                    <div class="tour-item-des__content text-over3">
                                                        {{$item->desc}}
                                                    </div>
                                                    <a href="{{route('client.slug',$item->slug_custom_name ?? '/')}}"
                                                       class="tour-item-link">View this tour</a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <button
                        class="carousel-control-prev"
                        type="button"
                        data-bs-target="#carouselTravelChoise"
                        data-bs-slide="prev"
                    >
                <span
                    class="carousel-control-prev-icon"
                    aria-hidden="true"
                >
                   <svg
                       style="transform: rotate(180deg);"
                       xmlns="http://www.w3.org/2000/svg"
                       xmlns:xlink="http://www.w3.org/1999/xlink"
                       width="39px" height="39px">
<path fill-rule="evenodd"  fill="rgb(45, 45, 43)"
      d="M19.499,39.0 C8.747,39.0 0.0,30.252 0.0,19.499 C0.0,8.747 8.747,0.0 19.499,0.0 C30.252,0.0 38.999,8.747 38.999,19.499 C38.999,30.252 30.252,39.0 19.499,39.0 ZM19.499,1.559 C9.607,1.559 1.559,9.607 1.559,19.499 C1.559,29.392 9.607,37.440 19.499,37.440 C29.392,37.440 37.439,29.392 37.439,19.499 C37.439,9.607 29.392,1.559 19.499,1.559 ZM15.4,19.500 L21.995,14.5 L20.742,19.500 L21.995,24.994 L15.4,19.500 Z"/>
</svg>
                </span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button
                        class="carousel-control-next"
                        type="button"
                        data-bs-target="#carouselTravelChoise"
                        data-bs-slide="next"
                    >
                <span
                    class="carousel-control-next-icon "
                    aria-hidden="true"
                >
                               <svg
                                   xmlns="http://www.w3.org/2000/svg"
                                   xmlns:xlink="http://www.w3.org/1999/xlink"
                                   width="39px" height="39px">
                        <path fill-rule="evenodd" fill="rgb(45, 45, 43)"
                              d="M19.500,38.999 C8.747,38.999 0.0,30.252 0.0,19.499 C0.0,8.747 8.747,0.0 19.500,0.0 C30.252,0.0 39.0,8.747 39.0,19.499 C39.0,30.252 30.252,38.999 19.500,38.999 ZM19.500,1.559 C9.607,1.559 1.560,9.607 1.560,19.499 C1.560,29.392 9.607,37.440 19.500,37.440 C29.392,37.440 37.440,29.392 37.440,19.499 C37.440,9.607 29.392,1.559 19.500,1.559 ZM18.257,19.499 L17.4,14.5 L23.995,19.499 L17.4,24.994 L18.257,19.499 Z"/>
                        </svg>
                </span>

                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
        </div>
    </section>
    <section class="travel-interest ">
        <div class=" container d-flex flex-wrap">
            <div class="col-12 col-sm-6 travel-interest-left">
                <div class="travel-interest-des">
                    <p class="travel-interest-des__note">Travel in style ?</p>
                    <p class="travel-interest-des__title">TRAVEL WITH INTEREST</p>
                    <p class="travel-interest-des__content">
                        If you are Looking for something unique for your travel to
                        Vietnam and would like to see it more beautiful and interesting
                        then you`ll find more choices here.
                    </p>
                    <p class="travel-interest-des__content">
                        From culture tours to family adventures, from beach escape to
                        romatic couple honeymoon, our tours have something for you.
                    </p>
                </div>
            </div>
            <div class="col-12 col-sm-6">
                <div class="travel-interest-des-logo">
                    <div class="travel-interest-des-logo__tour">

                        @foreach($tourCategoryChilds->chunk(3) as $itemChunk)
                            @foreach($itemChunk as $item)
                                <div class="travel-interest-tour">
                                    <div class="travel-interest-tour__img">
                                        <img
                                            src="{{asset('clients/assets/images/travel-interest/interest'.($loop->iteration % 3 == 0 ? 3 : ($loop->iteration % 2 == 0 ? 2 :1) ).'.png')}}"
                                            alt=""
                                        />
                                    </div>
                                    <a href="{{route('client.slug',$item->slug_custom_name ?? '/')}}"
                                       class="travel-interest-tour__name text-decoration-none text-white">{{$item?->name}}</a>
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                    <div
                        class="row pt-4  justify-content-center  {{$tourCategoryChilds->count() > 0 ? 'justify-content-sm-start' : ''}}"
                        style="margin-left: 10px">
                        <a class="btn btn-create-tour" href="{{route('client.slug','customize-tour')}}">CREATE YOUR
                            TOUR</a>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('#carouselTourRecoment').on('slid.bs.carousel', function () {
                var $this = $(this);
                if (!$this.find('.carousel-inner .item:first').hasClass('active')) {
                    $this.find('.carousel-control.left').show();
                } else {
                    $this.find('.carousel-control.left').hide();
                }
                if (!$this.find('.carousel-inner .item:last').hasClass('active')) {
                    $this.find('.carousel-control.right').show();
                } else {
                    $this.find('.carousel-control.right').hide();
                }
            });

            $('#carouselTourRecoment').on('slid.bs.carousel', function () {
                var $this = $(this);
                if (!$this.find('.carousel-inner .item:first').hasClass('active')) {
                    $this.find('.carousel-control.left').show();
                } else {
                    $this.find('.carousel-control.left').hide();
                }
                if (!$this.find('.carousel-inner .item:last').hasClass('active')) {
                    $this.find('.carousel-control.right').show();
                } else {
                    $this.find('.carousel-control.right').hide();
                }
            });
        });

        $(()=>{
            let imageWidth = document.querySelector('.tour-item__img').clientWidth;
            let heightImage = +imageWidth * 1.4;
            if(window.innerWidth > 567)
            {
                $('.tour-item__img').css('height', `${heightImage}px`)
            }

            const resizeObserver = new ResizeObserver(entries => {
                for (let entry of entries) {
                    let imageWidth = document.querySelector('.tour-item__img').clientWidth;
                    let heightImage = +imageWidth * 1.4;
                    if(window.innerWidth > 567)
                    {
                        $('.tour-item__img').css('height', `${heightImage}px`)
                    }
                }
            });
            resizeObserver.observe(document.body); //
        })

    </script>
@endsection
