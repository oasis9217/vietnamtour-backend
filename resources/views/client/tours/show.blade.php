@section('style')
    <link rel="stylesheet" href="{{asset('clients/assets/css/tour-detail.css')}}"/>
    <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
<style>

    .tour-service-head__des > p{
        margin-left: unset !important;
    }
    .expand-all-day{
        cursor: pointer;
    }
</style>

@endsection
@extends('client.layouts.app')
@section('title')
    {{$tour->name}}
@endsection
@section('meta')
   {!! $tour->meta !!}
@endsection
@section('header_image')
    <div class="header-image">
        <img data-src="{{$tour->header_image_path}}" class="img-response lazyload" alt=""/>
    </div>
@endsection

@section('header_banner')
    <div class="header-banner header-banner-tour"></div>
@endsection
@section('main')
    <section class="tour-service">
        <div class="container">
            <div class="tour-service-head">
                <div class="tour-service-head__title">
                    <h1>{{$tour->name}}</h1>
                    <img
                        class="tour-service-head__line"
                        src="{{asset('clients/assets/images/tours/tour-title-line.png')}}"
                        alt=""
                    />
                </div>

                <div class="tour-service-head__des">
                    {!! $tour->content !!}
                </div>
            </div>
        </div>
       <div class="container container-tour-service-info">
           <div class="tour-service-info">
               <div class="tour-service-info__tour">
                   <p><span>Tour duration</span>: {{$tour->day}} days / {{$tour->day - 1 > 0 ? $tour->day - 1 : 0 }} nights</p>
                   <p><span>Travel Spots:</span> Hanoi, Halong Bay, Nha Trang</p>
                   <p><span>Transport:</span> flight, cruise, train</p>
                   <p><span>Price:</span> from $ {{$tour->price ?? '0'}}/ pp</p>
               </div>

               <div class="tour-service-info-guild">
                   <div class="tour-service-info-guild__img">
                       <img data-src="{{asset($tourGuilder['image'])}}" class="lazyload" alt=""/>
                   </div>
                   <div class="tour-service-info-guild-user">
                       <p class="tour-service-info-guild__name">{{$tourGuilder['name']}}</p>
                       <a
                           class="tour-service-info-guild__email"
                           href="mailto:duykhiem.vietnamtour@gmail.com"
                       >duykhiem.vietnamtour@gmail.com</a
                       >
                       <a
                           class="tour-service-info-guild__phone"
                           href="tel:+ 84 919 666 568"
                       >+ 84 919 666 568</a
                       >
                       <a class="tour-service-info-guild__link"
                          href="{{route('client.customize_tour')}}"
                       >Customize this tour >></a
                       >
                   </div>
               </div>
           </div>
       </div>
        <div class="tour-service-map">
            <div class="tour-service-map-img">
                <div id = "map-info" style = "width:100%; height:472px"></div>

            </div>
        </div>

        <div class="itinerary">
            <div class="itinerary-title">
                <div class="itinerary-title__head">
                    <div class="itinerary-title__head-logo">
                        <img src="{{asset('clients/assets/images/tour-detail/Layer 85.png')}}"/>
                        <img src="{{asset('clients/assets/images/tour-detail/Layer 85 copy.png')}}"/>
                    </div>
                    <p>ITINERARY</p>
                </div>
            </div>
            <div class="itinerary-title__des">
                <p>
                    {{$tour->itinerary ?? 'The trip brings you from the vibrant Hanoi to the far most area There are more things to delve into each traveling day. There are more things to delve into each traveling day.'}}
                </p>
            </div>
        </div>
    </section>

    <section class="tour-step-service">
        <div class="container container-tour-service-info">
            <div class="tour-step-service__head">
                <p ><span class="expand-all-day">Expand all days +</span></p>
            </div>
            <div class="tour-step-service-step">
                <div class="accordion" id="accordionExample">
                    @foreach($tour->tourDetails as $item)
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="heading{{$item->id}}">
                                <button
                                    class="accordion-button collapsed"
                                    type="button"
                                    data-bs-toggle="collapse"
                                    data-bs-target="#collapse{{$item->id}}"
                                    aria-expanded="false"
                                    aria-controls="collapse{{$item->id}}"
                                >
                                    {{$item->name}}
                                </button>
                            </h2>
                            <div
                                id="collapse{{$item->id}}"
                                class="accordion-collapse collapse"
                                aria-labelledby="heading{{$item->id}}"
                                data-bs-parent="#accordionExample"
                            >
                                <div class="accordion-body">
                                    {{$item->desc}}
                                </div>
                            </div>
                        </div>

                    @endforeach
                </div>
            </div>

            <div class="tour-step-line-button"></div>
        </div>
    </section>

    <section class="inclusion-service container container-tour-service-info">
        <div class="inclusion-service-head">
            <p>inclusion of this tour</p>
        </div>
        <div class="row">
            <div class="col-12 col-sm-6">
                <div class="inclusion-item">
                    <div class="inclusion-item-title">
                        <p>WHAT`S INCLUDED</p>
                    </div>
                    <div class="inclusion-item-rules">
                        <div class="inclusion-item-rule">
                            <div class="inclusion-item-rule__img">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="34px"
                                    height="21px"
                                >
                                    <image
                                        opacity="0.588"
                                        x="0px"
                                        y="0px"
                                        width="34px"
                                        height="21px"
                                        xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAVCAQAAACJMfRcAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfnAgUTEgYFfOoHAAACoklEQVQ4y6WUv08UQRTHP7Mzu3vHcscdpyDGKCSChQg2mlhpTFRibKz8D+w0WliKPSYUxsaOWFBQkGBlNMbE3/FHIQYTNVGJCGLE4+4478fuzljsKiB4Rn3NvF95877v+2bEtf5Tx7eUxkY2L+QJkHymG0mJAI+rTGPzq2g8HCT72cUCn9iBnDz5ZtOH1mq9+1WaJiQhGaCOxuE5BeSaIgYHiUUn7VRYYgOqJAEeN82y2Hl5/0y2b/r8zdwSfyVKANBWszoGzuQduN3ztPvmpVQ1QOKQWBdOEgeNAZrxUKgokNMjB/JOpN/vHO8V8m7XuRtPeifb14Njkyr2TSbnAjV0eGrD4C1lGQBhzWWW0yb6J3bXZTH5MXtn++8gXB/YO/Rg6+AxeN1hRa5aeOjFj3C6vme2LmE+bfu/n8M3d2zvVCvATMrSAmDWPXrv7N1MxdZb81dGdr8EsExgNRpnIWUbAGXiwdqm5vS8PmSqqc3zz+z3ewCEaVgDYcKf7BiAtnDoxOi+1UmBlGGjIgY/6oAYjiZcc23/rK8atqISOlaiY1GdHvfnsk3KPNRtYqf11ljlo48KO8NXFtIYtBDGQmOENBAKYRA9M5VtMbCuC+/awat4ebvejKCIjUeVKjXX9YUGC9AIBAazwtKy2FryoGMh7qScLCf5Z4ln8n8SU+wGnq8RgAEEJsIaWyu9yxZU7KpaQfHhqeHRJeGhWESRpkaJFA4FIE1AEY+kKYqANFDAxTMJho9cPAigIjjC16UWckgUigwVXFLxG87g4+Dh4RCQAWxc0iTJVlZRHIoydRI0EQA+AQEB/horwAd8HFz4uVtWtNvSasblCxUar3q0mC65iGoLIFSqZTGrNN6SwsbwlcQfyhhsWuKvqrmc+wob898Bo1v32G/HB7oAAAAASUVORK5CYII="
                                    />
                                </svg>
                            </div>
                            <div class="inclusion-item-rule-content">
                                <p class="inclusion-item-rule-content__name">
                                    Accommodation
                                </p>
                                <p class="inclusion-item-rule-content__des">
                                    {{$tour->accommodation ?? 'All hotel nights (overnight on train/cruise per itinerary)'}}
                                </p>
                            </div>
                        </div>

                        <div class="inclusion-item-rule">
                            <div class="inclusion-item-rule__img">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="36px"
                                    height="29px"
                                >
                                    <image
                                        x="0px"
                                        y="0px"
                                        width="36px"
                                        height="29px"
                                        xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAdCAQAAABofAZ2AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfnAgUTEx94DHOGAAAG3UlEQVRIx12Ua1DU1xnGn3P+Z+/shV0uAi7gBVhQEYlK1HghlBBHMtrGS0ej0KmpmTaTVEkTUyc1sTPtYD4E7dTbaNuMVmhScxHqhctEvA4BzSIocikrV4Vlgd11d/+7/1s/oIzp+/E5Z37neZ/3nUOuRTsCVOkwzh532bJ8ItetM0/4oQGPSHJg2Z01fUt8s3idSjGOW9szmqwN0c1uPhYL4IYCMxRQ8Oxm4oZ+LvGS4nmSXPtHz83mzy2u1hdaDvgrjbDNbPi4vvz6Tlk1+1Zqbe7FlCsxnaLxzmbnW0OvJISi2xPhB6AFoGYXDtcdUOtY+3LNv1NCbbke08jSzPhmezi3WPv9xn9USHLWmd2n7vfE8SpY4YUCN/TmrPybv/nm5Nwds97RdAahhxXOVfXb4rr/VUa1AbXARE1QL+kCTFKLUW5DXv3nyTVvvpi2O6qd8V4kgEANHgNY5I36Zmdh6WZv0u+uDW4wIIhJ3H059tGLJyRGAQUEDCIRCEAR1HP3y/OKSvj/LsQkbNxoyuDK/o1Drw3kRGKSwYMhqfrgqvSrFeeGXjehU3u7ILOanyQcAwhkTlJFqKiSKYGfu+YpGR2BGVbq3Hpv2701F7UqP1WHNZa+8KXFx7zO+RDGc94gpyvOfPpw5tDwovX7uywAJRCJwWtzp/JxIwa/RLTood+BQHb89fK5kz7dxt/uWZKcE7/gF4WLqjqLTzXe+UhF9VjF7ylJbav8sHcZ5Oi2gJoqjHBM5/g2pXn2wECxarApV6BamBBefPK8/GTbz/gLyYiHDAn2blJf9KfafV8c8Ka9vSsmhOAvt/Qkt29Jbh0YCaoImGlYl03EoX43+D4HjAvpyBIh3f5utWVg1QZhWEYEAgBAgICQL+eDHGf5WZN7c5kB0a4sV93x9Ca9LDECurqirvRGGWwTkOOufnhry8ZD+eTUQUn15tak4RkQ8ePyIbOyZHfVns5i4DEmEzy2jNsMCiFgRX+T7efKE3ZIE8Q2nrlm/7zKwcWNm8p2ZvYAYXQ8BxERhRyoUXT09qYv9qXW9YRDDk4S2/xgCgdqjeTuW3xqwJEYHnSsOJpxgJcbSuIfzqjywINRaKA8xUjQQQMbCLTh4oqO3IG8PIykMJj7ADVRCD2Dr9CW+erpj4pWfHVtvg9p3A8FOf8R+F70YgTzYAYgQ4IaL8GEEEQEkdVginQtiYU1iYn2YQ0EiAqrhhphnSH0GOpwSONGr3UiJa7dCt10SwIMEBEB/zR2AmXcdv9B2nL47Fq3l/cDCsCiQBCSJS4CmVKZA6Jk9bi3B+FpkAbZ6EYLmqYVFVH7ieE6OqNjAk50IkqSGXt2SAAooNAGOdFiSgU/rfvRBRnz0AbuqcYpIYM1tBKi89t1jy95jUhN6ngKehapGrEeU994VgABECjgYEETPIhCAmZDA3mqNdO1rJe+tmD9oZH02tLCC91P9F5KQMCBQgFAEMKo6LjifFWlNsOIGPTjCQgoAIJBAHGwIBaD+T7dnBYvrgR19+MfbFm3rPxBHmVg4JQpRxQ+aLH27ECGawNBBNK0U4AgiAgmQCCoat7NuJtyawwjCBpJ4AqGBJVMZciQnmakwQ/oxtxbK6tPfTZm74cLqucWkiGADhA0ljbn/7Q8NhQHAiiglBIOChUhQiLPli4eZtDI5vep7vxpnY3h/8uIB8Unjr12IutLCRpkgJv2TAnI9DWeZCMTCriuTRv6ss9coSsZ1KBTI4caetXdvX84t7T6V++ZIEMxpEWbpKn4QZ57VIFRSElwxc9y2mC+uv0n9ccPNaRfyK8MO9VBRgfjXAVnSybtRYff+r1HcEKrqTmffNc8RBQFCjj8CBTlf7T26PGPi7MvWzB85+3Vl0p7th6p0ghknKpardqJuRdf3hl3g6IBetZ6rCO/5JPvlylkahRMwdSvTUHgN87/8vH6vTX7txurjAgEc47Y/x5rz3CEZ/B86sNgDxv2IhG9sBqu/+VOye5S4epoAVUUyBAVJoNAZhAFMHFSf9tf9obqyP7KouWFfx57xDAcSu5K6pLAIwb94JEBhp7VteW9mb/ebvunCzLjqE42h0UVtwfp0Oe1rONwvXTpd7NrDJHC8xZ3zd7WHUaD+6EYcMgcCAgIQhgwWFacO/j1J8bHH/w86jIPCwwzb+2KFVwbGUgADONzDp/oyp7f8s4uY/8kotELT8aF9/vX+WLnNNqbjCNsUmRKzNisroKJOUmta48LJ5cIg1gIDQL6zz6993r0aMl7xAdAh3v6hpRXXFl8GEEY0YUxBCGnh/Od+X0vBGNkRsD46N7MG/o6x81c73ksxyiyocEYGuFOnzuxwP0/o4vzGdhXzisAAAAASUVORK5CYII="
                                    />
                                </svg>
                            </div>
                            <div class="inclusion-item-rule-content">
                                <p class="inclusion-item-rule-content__name">Meals:</p>
                                <p class="inclusion-item-rule-content__des">
                                    {{$tour->meals ??'All breakfasts and lunches in tour days'}}
                                </p>
                            </div>
                        </div>

                        <div class="inclusion-item-rule">
                            <div class="inclusion-item-rule__img">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="42px"
                                    height="16px"
                                >
                                    <image
                                        x="0px"
                                        y="0px"
                                        width="42px"
                                        height="16px"
                                        xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACoAAAAQCAYAAABgIu2QAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH5wIFExQLLZcxPAAAB9xJREFUSMedlntQVNcdx3/n3nPuvXv3vcAKCizBJQjyMpUZDTWCEDRtJa2S0czUSgcntjMdtaltMzWaMe1Mpp3pK8lEHNtE27GRmrFRkwgimPhAebsKiAoIqzwCu8vCPu7ufZ3+YXHMxE6bfGfOnH/Omfme7/n9PvNDMzMz8N57h2GxOwucTid4vaOwePFiEAQBeF6AtrY2KC9bDRcuXTYbBCHdIIpFY/fH8mfn5jKDwZlEhmENs7OzJBwO21iW1S0Wc8xgEOI8bxjLcLmujIyONpSvKevLzc1VPzx5EvLz84BgAnfuDEJ6ejokJSXC0aNHweFwQHJyMmRkZMLFixegoCAPTCYz+P0+sFqtgOExopQCxxEwm00EIbT8wsVLpb19fc/LspwqGo2fCxzfHwzOeFJTU4cWLlw4cXtwSNM07e1FixbdzcnJfru7uzuFZZm8Ue/oulgs9qMrV6/28Dz/pslk+pRSCl9HXzKKEAKLxZI4MHD7u4ODg9/r7e1dEY1GHJXPVtQJBnFPdvaSDo7j5hobzkBCQgKYzWZYVVICbe3tfaqqEkEQm1euWAnB2VmorKxEJ06ceFqW5S3H6uuPiKJ4trAg/7VYLD7+dYwiAMCUUp5SWhgIBDYcOvSXdffu3csOhUKs1WqVd+3auTEzM+sEQgANn5yBUDgECxakgM1uA5vNAklJTgCAicnJyXVFhQVGjiOR/a//GkZGRigh5HJFefllnueKwuHI7+rqDn5WVVW1gxByBmMMGOP/1yhNFgThD+eamsr8gUBSPB5nAABYlgVRFMFsNkcikahleHh4vdVqAb/fHwnOBmWEGJUQRgNKZV2HCCGYC4VCGYGAv5gCeCVJUn0+n+Z0OrX29g7FajUPFRUt2+y55vn5kSN/+3tJSck7giCcQQimEEIxjuOiPM/PCoKgI4S+ZBRN+3yWs41N1TcHBkrHxu5viESiRoZhvlCvqqoCQggQQoAxBgYhQAyjMwg0xDAqy2JV0zUSj8UEURRDhHAy1XUJEywZDGJM4HnZaDIqkUhkjmXZ4MTEZMXk5KTDarVSgyDMCAIfcCQkjNtt9puLUtP6fdNTN0pKVvZxHD81Pj4GNpsNkD8QhK6uThi7P/ak57rnotd738lx5AtGNU17aPRR6ToFAAq6ToFlGWBZBmRZAUopsCz78K6u66BpOrAsAwzDAMdxwDAMqKoKqqoCAICm6YDQg5+0WixSSkrKcFFR4b9c6Wlv8YIwha9evVLb2nplbTAYTPMHZpIIeWBSURTQNBXMZnOU50xyOBIxxiSJcDwP84k/EvxDcRwHCCGY726O4750Rtd1iMsyMAhRu90eBqBoLhQSqU4ZAADBYIgWFOa/09NzzYcxozmdCwC3t3f8sLu7u8RkMgHDPHhxNBoFh8MxU1294U92m/28xWpR4zHJ0dnZubXxbPMLLMsCy7L/tfAfRdDjcCRJEizJzu5dt27tnw0GoU+jOkWAnmw40/iT7p6eYl3XWFd6xujExNTlwaERnhDOzubk5P5KVVUHxhgQQhCPx8HhcEy/8sova80mi//U6Y92tLZe2SJJklhdXf1WQoLD39XZtQoTAl9VCCGIxWJQWJDXsXv37i2jIyM5J0+d3nXz5sBzC1NSJjdu3Pjb6anprL6+vqUDtwY2MAxT5vf7a3mBX4oNBkNM0zQghICu6wBA9Rdf3PxHRVEMr+7bdzQcCjFGoxE8nutLBweHK/fv3/ed3hu9z7S1d5QYjUb4KgDXNA1EozFWW1v7i+bmpm0HDh7aJQoiUATQ03NtRU3N1tSamh/8Zvju3WXT077E2dm5EkmSQBAEjXnqqWVNkhQDWVFAURQwmy0zC1NSrh4//sG2aDTKbH/ppd/v3fvqmrVrK1uvea5ltJz/dP3y5cv/oaoq6LoOlNL/uXT9wS5JMchyu7sYhokfq/9glyvdFXz5Zz/dtnfPnk2ZizN9H5489eNINIqXLFnSCUCB4zgghAClVMDPV1W9EQ6Fxb7+/tXRaNSQlpbmVVRN8/l8qVaLGSorK+osFsvgzEzg43PNzU/fHxvLdWc+0YAxhlgsBhhjQICAAgUEABQoPC7keQIkOOyfT01PO4PBWSguLvYU5Ocd5jlOy8tduv2jjz9Zc/v2nQSDIAQIITrHcXFRFEMul6sDy3J8qqCgYHtWVpZpfHzcUVGxRjeIYiwpKXHq1q1b2cfq//lGbk5OS0vz+e9TXYcnXK4+hmGuf/tbz71ud9h1r9drisfimBCssyzWMcEaJoSyDEsxy+gYY51hWWqz2rSRkRGVEOJxJiWNOZ1JqsfjKW5padlrNpnDnV3dxYmJCUqW2+2LhMPv5uct/SwtLW0mNS19ODc3x4Pna0dRlLCqqmFJkiAhMQGqqtYfGBoa/kZ9/fFqUTRURyNRKC1bfaOwsOD9rq5ur9vt7n1m1TfhcuslmJsL/2fa4oEXBDAYReAwAQ6zwPECYIwhOTkZGhobob+vH5KTk8nmTZvePFB38OW/vnv4tXlk1dRsPejKcLW3tbdFNE1rVlUVZFkGRVEePz3F43Gw223v79y5Y7al5fyWwcHBlLLS1R1ut7tO0zTvPGcj0SjE4zLIsvyQrYAQIJYBHWugYRY0nQLGGFRVBUVR5jmqPFtRvsdkMt9uaj73AlCKy9eUnl5dWnqQYBx5XIP+G84ZyPWOzg4NAAAAAElFTkSuQmCC"
                                    />
                                </svg>
                            </div>
                            <div class="inclusion-item-rule-content">
                                <p class="inclusion-item-rule-content__name">Transport:</p>
                                <p class="inclusion-item-rule-content__des">
                                    {{$tour->transport ?? 'Private car with A/C, internal flights'}}
                                </p>
                            </div>
                        </div>

                        <div class="inclusion-item-rule">
                            <div class="inclusion-item-rule__img">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="31px"
                                    height="31px"
                                >
                                    <image
                                        opacity="0.671"
                                        x="0px"
                                        y="0px"
                                        width="31px"
                                        height="31px"
                                        xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAAfCAYAAAAfrhY5AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH5wIFExQ2df99LQAACxxJREFUSMeVl2lslVd+xp93f+997+K7+nq3r/drMDYETAgwxokdSEBU06bNZEgyrZSotFJTtdNK7ZfRRO2HqovSUcKE0KijfigkM5MGpkkMARsMRISA7RAv2L7et2vuvr370g8eojCEafv/fKTf/zzP/zznHGJ6ahokSUKWZdweHcXi4hLsNhsqyivw1fgYMpkMAgE/lYgnmgmCbIvdizdYhubKF0SSpGiYug67nTcZlsuXlYVmTNMYLy0NTa2urhoejwednR2IzkxDFEW0RiLY+dgO0DQNy7JA4xFFURQIgiAsy+q+dWtkP0mSJf6ALxoMBm5lM+mNQDCQK68o1xbmFhhRklxud0lpURSb4vH4nvn5pUxVZfkQQRCXaZq2HsV4CG5ZFliGQS6fD09O3n1ZFEW+o6PjkqfE/TnDMlkCBK5dvw6OY6FpGjRNgyyr6OiohdPh+DSeTLkVSeq689Wdg/fiie6Ghvr/YBlmtmhZ/zuc4zjEE8nuGx/3P79n964rqVT6A6fTqXAch0KxCIIgEAqFYLfbYJkmgqVB2OxFSJIEiiTBsmyWY+gLzc3NV9wu13fPnHnvrzo7tr0nCPbBR8ItywLLsojFNo7Mzc4ebKyvf3vP7sdH/+vsOei6DgAQBAEOhx08b4OqKsjnMigtLYXfbyDg90NRNWRzeei6Dk3XlT17dp9OpdKTd6emX6msKHe2RiLnrG8oQH5zx7FYrDsajR7at/eJN1iOGRUlCbIsQ1EUPNnTg4N9veA5HoAFwzAA4OvGbDYb9u/dg76neqCpGmRZhihKYFl2tLe3743FxeWnNzY2DnA89yCcoijEE4najz7u/97url3v2Gy2GcuyIIoiDj7diz98+UXU1tZA13UwDA0AnGmaTaZpPWaaZhMAjqZpaLqO5qZGvPTSMfQ+1QNRkmBZFniem3li7xOnzp376Hux9Y0wTVObslsUjVgiQf3kzRN/dPTIs5ePHXthtFAoQlM1qKqC5uZmAEAqlQbLskRs496B/v4LzyZTqTLTsGiCIvSAz7du4/mPIpHWQVVRrPr6MMJ1tZiansbOHdvBsiwEQRgVbPzAP//LGz/4yx/+xY99Xo9BsiyLicm7+7LZnO1Ad/eHIAjwPI9AMIDm5mbc90gQ7Dh/4eJzJ3566vjKylq1YZiMBYswDZNZWlmtPvHTU8cHBgZ/zy7YYRoGTNNEc1MTAgE/WJYFAHQf+M7ZfLFoGx+f2McwLKgXnn+ePn3m/Zd7eroHnU7n1L2NONbX1+F0OMHxPDRNAwHgxuc3d/z49b9/laAogud5AABBECAIAgzDwLQs3BoeiWxrb5+rrKxaVxQFpmUhk8lhamoGyVQasqzoJe4Sa+jq1e5Ia+s1OpVKNRULeffex3ffcLqc0DQNPM9heGQEa2vroBkGNp6nrly91iOrKuF2uWD9xpm1LAs8xyGbzRGnz7zfs7q6NirJsqHrOsrLQujcthWapoFmGJS4HZ9fHLh0OJ5MNNK3R77cWl/fMKMaen5tPQaaprG6tobzn16CIisgSRI0TduXl5drOJZ9CPxAAzyHhYWFGl3XBF3Xc6qqorWlBcFgKURRBEEQ4Dg2X1VZNTNye2QrOTE52dDY0DDt83jhdrng9/kgSwpW12KQFRVFSUJBFClNNxiSJPHbiiAIaJrBiqJEipIMSZKRSKZAkRTKQmUI+gMoLytDpKV5em5uvoFWFdmRyaQ2Lg0MwDAMkCSJYrGIzm1bQBLEZgbwrPKlpqZWV1a87CN2vwnW4PN5km0tLYqsqiAIQFVVDF65jJISD0zTAEVRSGezG/lCwUEXCkVyeWU5F43OAgSgKgq2bNmCF/7gOciKfD/ZJLtNGL51a7jBbrc/UnZVU9HV1TX8zME+qVgUYVkWBIcD/f3ncfXaVfAcd9+eXLFYJGkQJIKBoK5pm0mlKApCoRB8fj9EUQJBEKAoCgeffvrijRs3t98eHmlwOp0gfq0KAJimhXw+h927ds7+ztEjFwW7HS6XCxRFIZlMwrIM1FZXg6I2w4VlGW12fhFUfWNTL0USw+l0OpPL51AsFrGwsIBwOIyamhqYpgmKohAMBuT29q0TX9z8oqZYFAOyokDXNOi6Do5jUVVVOfF3r//orZaW5rhhGuB4Fl6vF1PT0zh79lfQNA35fB65XA6iKPlS6cwuWrDZzOrqWpfX54VhGCAAiJIEl9sNkiSxvr4OXddBURQkSV7v7Xvy9XvxRFcqmWziWM6laWo+FApNeTyezxOJhPHV2BiSyRSCpaWoq2HhdrnRsX07OG4z0ymSRDqTccXuJUyaYbmCL+AP9vY+iWJR/LWMJkKhEDRd3xzAQhHRaBQgSdQ31NtT6XQyGAzOGqYlOAR7MZVMJcPhsH18YjxPkhSqq6pAhsqgGQb8fi/27dm9aRMBCHY7Bi8PBR2CUKDbtrRFozMzzTt3bL8oKwoMXYfP74fT6UQ+lwMAOBwCNE2rXFhafuo/T5/Zk83m3Jqmk6ZpgiBIsCxtjnz5ZXZ7Z8dnDeHwJUEQlgHANAy43W44nS6kUilQFAVV1bC8vNIUDtdG6R2dHWPvnHr3FYKknKFQWV5RVTgdDsCy7ocCPrv+2Xfe+/kvX1hbj3l5ngdN02AY5puTTiaTGc9/f9T/bHlZ2eNHVeX0d+vqLhMEAcsCvF4v7HY7WJZFNpt1zi8sNf3gpWOnyMry8mm3y5Ubvn27KxGPQxFFeD0ekCQJm82G/v4LR948cfJP44mk1+VygWGYByb9/hlnGBpOpxP34nHvyVP//ief9J8/wrEcKIqCwyEgl8shFovh5hdfdNkFe76ionyarKur0Q4d6rv8wYdn+0pKXHxjYz0y6RQymQwuXry0+9S7P/s+RdMEz/OPjNYHI5YHwzDEv/7kre9/0t//eKFQgK7raGpqhNPl5D/48FzfMwf7BmtqqjVSlmVEIq1DguBQBgYGj5qmiZWVFSzMz7lOvH3yRcsCdf9K/L/W5nqCOvnOu8dWVlZc+XwelmVhYPDKUbvdrkQirUOKooB69fhx2AXB3LIlsvj2yX97KZ3JLgHYuHbt+pErQ9e7BIcAWP8vNgCAYRmsrK4KVZUVqs1mG3//579ov3Dh0+f/5q9/+GYg4E8qirr5jNINHaFQ6dyhQ31nhoaGXk1nMnWCICz4vB5JEjefQvgNn3/b5WJZFiRRQsDnkTRNm5ufX6i7cePmq4efPXQmFCqd0/XN9x/1Z6+9BoIgoCgqioXCfCaVom8ND79Y4nZfP3z48CexjVhtPJ7w65oGiqJAkuS3DNwm1DRNSJIE0zTR1Fh/9/gfv/KP0ZlZXL4y9OfVVdUX2tra+n0+79cxS3+zY1VVEQgGznk8nsLI6J3X7Hbh4+79+/7hsR2d22/fHulbWFxqVhQNAECSFDb92IRasMAyLLa1b51ub99ynqGp4ejcfM+dsbFntnd2vk/T9ICqqg80/tCnQVEUVFVUDrRGIku/+OUHL0uy3LVr585feTzet/bv3y9MTN5tTKaS9fls1mNZFksQpCo4nOlAwDfb1toSnZicLBi6VnP9+md/y7Ks8fvP/e4/iaIcXV5efsiih+D3FXC7XdGGhvofUTR94KuxsYMAhLHx8YnFpcVoTXXtQFlpINtQ36BPTU3Tiqa7lxYW/IRl7Y1tbEQSyWRx27b2AVVRB1xOp5nJ5B6y6lvh90vXDei6bobD4UvlZWVX7m3EW0zTaNNUbX8mk3EUxSK5tLxmaZoKh8NpVVRUFEo8JVGHy/mzupqaKZuN0++MjX/9ufi2+h+1vGQmtwh+1QAAAABJRU5ErkJggg=="
                                    />
                                </svg>
                            </div>
                            <div class="inclusion-item-rule-content">
                                <p class="inclusion-item-rule-content__name">
                                    Travel team:
                                </p>
                                <p class="inclusion-item-rule-content__des">
                                    {{$tour->travel_team ?? 'Private tour guide, private drivers'}}
                                </p>
                            </div>
                        </div>

                        <div class="inclusion-item-rule">
                            <div class="inclusion-item-rule__img">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="30px"
                                    height="25px"
                                >
                                    <image
                                        opacity="0.769"
                                        x="0px"
                                        y="0px"
                                        width="30px"
                                        height="25px"
                                        xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAZCAQAAACMPFaRAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfnAgUTFR3AWLUsAAAEHElEQVQ4y5WUXWyURRiFn/lmdrftfs0u7Za2UHENNMQAVluShmpBQLGKFGpIbAIJiaZeIAkJXDQaDUKUEC9QNCExoZQQNXghQfkpFpBGRcFaaBuhDT+FVGBLZbfbZbe0+/2MF2xL4c73XM57Mu85c94R27lDDzOIYPAE7ZSQL7vWdb+ulQAEGkA/3fJsU9K6iYsHiFFAKUUoAFAo09AiBcr8c313reHImCsABGg72LnWnjp3F8M8UgokgvbiwY+EW7TZk93R2FdVeDO8k9s6c7NAFPZv/HvZaH7RDiciAQOJASiF5Y8vvrIkMQdObUsUDMwEz6iudrwPx5ZpTxquVsR3Tu/BgJQV77FOyoTYHtz/YW8l/7vCXWu2qJOreysLL5ccTMS1EAC4WGgU0lYu2NKRNgIvYoIY8EdW3ShrWyNyD9wvbHh38NJ1NAZwDx9PkuJtjhtX/JqZIzXOXkz6SWMCoJnGzZnXdqdTaiQ3K9YfHQZcbGIsJ8wrtIRPL7lUPlCiRfq293z1zyuuHeEWJwgiMYgyGvdEk3lKIMUFwyCAyyx2UEWK5mXN7/QVgZkSOjKvc154uWiqP5xDDfu4jmQIJbUhXAXgxSHCXhYRRPPD0q0f2MbCMwsPllxV3Ar/turU4o8bQ+7aoy9RwVk248/oz4TE4D4+vAwyWPzeRsdo3LN03zmCKGLRTR059T+t37qh4mLJjVwCDzKXYQGQopYSIozy1YpIXm3b+/tdnAxsNh+ob43l7q3zkMae5HqG7JLPP7TT6TmxQLkLj8Xc4UyTYIynePGozz5dec78nYvIyfEEjaKPXxnDF7obKhiyrzWjyUUi8XGZy5h9oX/jwbZCJxkdV/pQM+gHUBjCkpaBIM15fAQw0AjHcF1jTDqkH10MAJti5pIgZ8ifHCwomLYylmQPvXiZTQMmfxUOB3KSc+469NP+uGZBkjDzeS5Z3j3qvVBtEXuwjjhEsThfnTDn9Twfe4G52I+T/bTQTwjFW4d9ztcrv6lqw4dAkMUvfFvRtFqy9scx5CS7JsgaHxY2ISq6Gw7cNb/clqhPB73SK9OBe3WffTIQXHew7JzJCMlJTzVhWA6bKGUD81nUFMk+9MauDaE3S3uU/mP2nSLBa0eqdqfoopnrBCboQh7zjwbXuwM2YBNnFSYLZHLpoZrOsns+8Fvl3a+2qtYO6z7HCWAgycIIRb8YnqKU5WbPVj1oBIoCWsjirLOltfxkXvGsIiV6B2YMzLC3EmOMfDRg41AqB3OEI8q2dL1ccezOd87Y+DAaCy/aEY4CLDAMlUbx8Nznza/rqCs9Ixpmff9pbGp20hiZ/MnoTGzGPRU8Utmp3MBQbaOsjJldtpnIc7yO4chxuNKVrtRSSy1d6UpnMpRlT+945vMpF/8DnVazt3DA8aMAAAAASUVORK5CYII="
                                    />
                                </svg>
                            </div>
                            <div class="inclusion-item-rule-content">
                                <p class="inclusion-item-rule-content__name">Experiences</p>
                                <p class="inclusion-item-rule-content__des">
                                    {{$tour->experiences ?? '10 unique travel experiences >>'}}
                                </p>
                            </div>
                        </div>

                        <div class="inclusion-item-rule">
                            <div class="inclusion-item-rule__img">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="33px"
                                    height="34px"
                                >
                                    <image
                                        opacity="0.769"
                                        x="0px"
                                        y="0px"
                                        width="33px"
                                        height="34px"
                                        xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAiCAYAAADRcLDBAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH5wIFExYPGMyXpwAAChtJREFUWMOll3twVNUdx7/nnnvv7t59ZDdskiXvmACKQAghERKHoA0vp6JIsNgqaEd5tFU7Y9upWvlD278crIy1Wq1KovVBkYrUB3QkFlCkQABDAvIQyHMTk02yj3t3773nnv6R7E4gCTLjb+bMzj7umc/+ft/f93cOiScSaG9vxycff4JIJAJBEKDrOopLilG3ciV6enoQjUZRUFDg2PL8lidOnPh6Zt2qug9uu23Z1n379iM/Pw/xeByNn/8Xhq6DEALLsjBt2jTccMP18Hg8SE9PB2MMAECIgC8Pf4WSudPRfLAJpVNnQMRVghACn89Hzp49l37q1On535w5k1NVVfXWhzs//HleXu7XHo/ngizLA5qm4YeECACCIIAQctmilELT4r76rfW/OHL0SHFR0XUJxaHEGTNFl9sV3bXrowfbLl2Slt+5/PMbp09/mxDCRz+f3POaICil0FStKBaL3axpmotSaiUSCWqaZvOePXs8DfUNyyxuwTTMcElJSfDcufN12dnZA0cOH7n+3LlzsmEagfR16a2mYZTG43E3IYQzxoRoJNrNGNtLKe3/XoivT5yY8+QTf3gmFAql2ew2AEAsGkMikfjMm+a9mNATUBQFp0+f9gwNDjk8aR61pbUlEAlHIEkSent6va0trbmNextXRqNRHyEE4MCO93egtvZHtY8/8fgmWZZ7TNOAaTJwzsdCvPDCX9abpilVVVfVB4PBDsuyiKZpos/nu+if5M+oXVTbRCnlAGAaJjGZSSZnT7YopTB0A2netMTk7MnnA4HAczE1phAQLssyqZ1SW719+/YF8+bPX1pTU1MfjUbh9XrhdrvHQrS1teUXFRZdnD59+tZgMGiYpgnLssBMhqxAljSrdNYhZrERpQIEJPVvLGbB6XLyUCgUMwyjlTGGuBbHw488jKnTpnbu3Llzwdmz5wLpk9LJ4OAgLy0thd/vBzgHpTSlGVGWZCORSFiUUqWsrGyoqakJmjqsdsaYEY1GDYtb49bSYhZAMAzNGFRVxQMPPICly5aiublZFgQBgkAswzSQkZGB64qKYDEGSZLkcDjs5hY3bDZbONmiAmMMLpcLc+bMwRcHvoBpmtfcYpxzaJqGNWvW4I4770AkEgFjjCTbXBIlZGdnQ5IkDAwO5h4+9L9fHv7r4VmZ/syB/Kycv1/mE4wxeDwelM8thyzLsCzrmiAMw8Da+9diydIlCIfDsNlslwHa7XYUFRVhYHDQ9/LLL/12x/s7yt0uN9ovtKXFVW3dGLPSdR1+vx+cc1iWlXoVBGFsOSwLpmFiytQpyM/Lh6qqo72BcM7BORcsy+KmybBnz566j/79UbnX64UoiqAiRU9vr2fMzoQQMMbAGINpmiCEQFEUlJWVgVscx48dg67rmD9vPhSnAlmW4fP5EI/HR20CEBAuiqJOiJCoWbAALS3Ny955++2fiKIIURTBOUcsFsNN825qndC2OeeIRqOovrka+fn5MAwDzGIwDAOcc7icLswtL0dvXx/ssg2ju5+AQLbJ3+Tm5j6dkeE/c7GtrXzz5s2PDAwM2BVFAQCEw2GUzp4dXLd+/YvC1SAkSUJhYSF0XU+1ZTLdyffRaBQnW1qg63rq8xFtDdatqvs8ryCPP7d582OdHZ1uRVHAOYeqqsjJyYms37hhs9vtvjQhBAAsvGUhDMO4qihFUURnVydkSYbd4YDd4YAky5jkn4Tbl9+e1tLS8rumI0fzkwCGYcBmt/F7fvbT+nSf76Ce0MefopxzeL1eBAKBcW12PBBJkiAIAviIEdkkCW9t/8eDH+z4V4VDcUAQBDDGoGka1q1f//GKO1e8axjG8LAbb1Nd11GzsAY2uy3VIaIoQlGUXFmWZymKkitKdAw4AFBKIUsSPmvcu+rNhvoVNpsNkiTBsixomoYfL7/98OrVq1/knKcm7biZSHYHFQRYAoUgCGhrb5uxY8f7v+/t6fV/e+FCSBCED6urqv4JgCW1QgD09/ejp6encsufn99o6CZsdhs454hEIlh4660nN27Y+DSA8GgPGgMRj8dRWVmJwoICmAYbJhUpdu3atfrQV4cKnU4nurq6XGfOfPOru++++/raRYtfpJR+RwgBFQRoqlr42quv/joUCskulyvlplmTA7G6VXWviVQcYIxBFMWJIQghiOsJqIl4Ks12Yhe6gl2ZdpsdDocjJbCG+obanmBPYG5lxbNOp3Lesiz7tm3bHm1qasp3u90ghMAwDFBKsWHDxjfnlM4+rI/oYLTWaEnJlHsURYkWFBb8xzTNhCiK6OzoRJrXC6fTiXg8Dl3XOaXUs3//gQpBEEDpcIlkWcbp06czQ6H+qkAgEN63f/+Sd999Z5HD4Ug5rBqL4b61a3evXLnyJcuyxlX5GIjkF5OzA6AihaZp0DQNuTm536Z5PRknT7aUJLQ4RFEEIQQ2mw3dXd2uAwcOLDhx4vh0QaAk6YhDQ2EsWrK4ec3atX8SRVGjlKayfdlRcjwIQRAQ6g8hryAvNfdNZhozbpxxsHR2aVdnR0dxd3e3O3kWlWV52E84J+JIJ6iqiqrqqot1dXXP2Oz2HkmSQEZEb1nWZWvc7iCEQNM0hPpCyMjKAHjSCS3D6XR+vOb+tcdOtbRueO+9925VNY0oIwaFkTrHNQ3XFRf3VVdXPRMJhy+CA8Gu7gl95qqO2dLcAofiAKUUfATENE0YutH90EMP/fGpTZu2ZGZmRgcGB2CZJixreN6k+bzGvffd+0Jz88lvQwMDctLIJloTakIQBJimiePHjsPhcCAz0w9JlBGLxRBPxJGXm2/l5ua0pqf7LiUSenF/f79PoAJycnJCDz/66MtFhUXNr/ztlWezMrMcXq/3ZDQaTenryjVhORhj6O7uRiwWQ19fH748oOCuu+5CJBzB8Dim6OsbgMvl2r/pyadami+dyrEkQr5r7ejNDgSCkXCkrKO944ahoaGzcT1++ai/IsZAJDMQDAZhGAbsdjuYyTA0FMbrr78BQzdQcVMFsjKykJ+fB0IIHA5HyONNC5kChy03F0QQwMGT8ySR7IJrgkhmIBgMQtf1VK+P3kRURBw9chTHjx1HVVUVOjs7MLV4Kmx2GxJxFXPKy6CbJoaGhnCtkYJIHuO6u7svA7gyOOeQZRmcczQ2NoIQgje2bgWRBETVGNgttaiorLhmAAAQLcsSKKXMbrcbFy5cuCrAlVlTFAWEEHR2dWL4aMWxbds2eNI8cLvdCRCAWUxgJkvdyseFIERgqqpOysvLm9ze3n4+HA5TgX4/xOhwiI5hMBCYpsl3f7obM2bOyNHjOlxOl+n3+6Gq6sQQixfX7tu6tX5FQ33DJsMwehJ6QrjW2/RE5ers7MTuT3dPVZyKMW/evGMzZ85EIpGY8Bmx5paa12KqKjXubayWZTkjee/8IREeDAuyJA899pvHGiorKw4KggBJkib8/f8BUFghENnl+HUAAAAASUVORK5CYII="
                                    />
                                </svg>
                            </div>
                            <div class="inclusion-item-rule-content">
                                <p class="inclusion-item-rule-content__name">
                                    Visa Arrangement :
                                </p>
                                <p class="inclusion-item-rule-content__des">
                                    {{$tour->visa_arrangement ?? 'We`’ll help you to arrange the visa'}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 inclusion-items-right">
                <div class="inclusion-item">
                    <div class="inclusion-item-title">
                        <p>WHAT`S NOT INCLUDED</p>
                    </div>
                    <div class="inclusion-item-rules">
                        <div class="inclusion-item-rule">
                            <div class="inclusion-item-rule__img">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="27px"
                                    height="35px"
                                >
                                    <image
                                        x="0px"
                                        y="0px"
                                        width="27px"
                                        height="35px"
                                        xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAjCAYAAABl/XGVAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH5wIFExct1LfnAgAACMtJREFUSMeVl1tsHFcZx//nzMzu7Mzuzl69dpy1u2vHsd2ojZ20dRI30LQkLYHSC1UrXqiiSiB4Q0K8ICQkqvKChEBAEQKpaR9QeCqFNJagrUvdhNqNSeM4re2sL7v1bddr73Xu5/BgO7Fjp2q+l5VW35zfdz/fIR98OATOOACAEII7CeccflVNEEKdty8MrC4tLTFBEHA3Ijq2A1VVwTmHbdt3BIqiGP3k6tWfVyrVGCH0t8lkcpAx9oUG7jiDuQySKCEQCEDySKCE7qqoqEr1nX+/235hYODRZ599Jn/4gQfedx2Hc34Xnm2GaHJyEtFoFOVKBZTuBIqiaO5p3vNuKBR6dHz8+nMnHnnkT/GGhsu2bd8dDAAICBSfD0FNA2cMtxtMKQUhuO7z+ewbN25EBwcHf/j106dfYox9Kd8IIbdgHByCIEALBpBIJHb9wCfL17xeb1kUxehHw8PfVFT13ra2tjHTNME535k/zgFCQAiBLMu3YJv0mZkZRCIRSJK0A5ZIJGaTyeTU8vJytFqtxyRJfK5Wq461tLRCURTsSCAhAOdYWFiAbds7YYy5GBsbQyqV2gGTJMm4t7t76OORkYds2yYfDQ+/ePTIkTebmpouy7K8DcY5g+MyUEJQyOdhmeZ22HpuBBQKBSiKglgsiq0poZTgwIGuD1S//0ecA5MTUy2Heg+9cPHipcu6XgchFACH6zKEQiHc09qCleIKBCpsL5DbPECxWEQ8Hgdj9k2LLcuG1yt/0tTYtPT5/HxClmWMj19/es+e0m8cx87ZtgNCCBzHQWNjIyIhDblcDq0trbdghJCNaiM3w2lZFkzLRLlchusygAACpXBdVgyFQxNz2bmELHsxMTHR3tf30HfT6dTL2bk5CIIAzjlUVd1smVuRsW2bmKZ52DCMH3DOI1uBBASrq2tobGzEnqZGxGIxaJq2JoriDcYYAALbtjEwMPBSNpttU3wKNkfYbv1Hw+GwMHrlyotnX3/jd8MjIy+JoiBIkghwDtd1USqVYZkWVosl2JYD2SvzQ729/+OcgTEGRVGQz+dbDcM8rYXDSCaTCIcjmF9Y2DEcaEdHh9OeTn9ULBb5P86f/+nMzNyzV8fGEY5G4fV6b44vXdehaRqi0Qi6uzqHfT6lWiqV4DgOSqUyuXjx0pnZ2bn01rDt8Ozz+Xmk0qkLBw/eP5vN5gLvvPveLxcXlo7Lsgwq0I0qpOs5NE1cufIJRMkzevBgz2AymZwMBoMLvb09Hy4szLfEYtEWwzAgCMJ6Gm5rclFVfCisFJcPHT78V4GKJ8KREHn0xImkKIph2StblNK4y1xDFMVG27bLhmnsdV0nd/hQz3/6+4++B3DXcRwxEAicW5if7y6trbmKqiYMw6jquk4FQTAFQRjinBuEua40MDCQqNb07wS1oJbN5VLRSGR5cmqqWxLFSqlUTuuGXhUo3VOtVg1JEoPVao1HwpGVpeWllsbGxs/m5xcOaFowq+t6U6VaJbFYzFlcWFQj0eiaT5Zpd1fnuZ6eg68Qxpj42muv/Xp4+OMX1splybZtMjM9HWiIx7FaKiHg94MxBkopmOvCK8twHAeiKIJSCtO04PerqNfr8CkKmOuCcw6Px4O6roMxBo9Hwisv/+JbYrlcdjnnLx8/fvyaaVn9IS2YzRfygenM9DEtFKrOzsxqnPMWItC14koxKAhC0HVZxTSNgKYFK+VyRXAc1x+JRMqFQj4YDGqo1eoAAI8kQdd1RKOR0cXFxWmSzWaxd+9enDv3t19d//Szo6ZpqGfOnPkJd53Rum5o1VrNvHr1avJrjz3mLxQK5WqteqAhHi8sLS13z88vtHd1dQ5eG79+PNEQX15dW3Uq5WpaUdVcJpO5p609XfKral4g5Jzk8YyKPkVBcXU1df7tC9/LZnPqvn3tuVwuV/P71UVwvlitVECpONPWlkatXkPH/o4hQRC00dErJzKZzP4bmczejo6Oscujow8/8/RTPzZN67Is+1goFGKd+zt8+/a1l/576RJWikWIAb8f7w2+/43FxUW1pTVZe/75577vD6jv67U6FEXZKF8O13GgBYNgrouJiYn7/v7WW2ds25Zt28bQ0NCJtra2Rdtx1jweT71Wq8GyLLiua238rrdQqVSm09PTD1BC8NSTT/4slUr9U/UpiEajoFQAYwyu68KybTQ3N6+vDqXyg47jyLIsQ9M0+Hw+PPHEqT/sa28fSyQaoCg+rI+z7SJOTk54e3sOnu3q3P9mMpl8mxICVVEAQlCv5yFKEo4dO4rSWgmMc/h8PjI5NdXjOA4URYGu60inU3PHH374rMfjgcfrQalUxm6bkMgY0zVN+5fX6wWAWxZxjlgsinQ6hVAoBNu2IQgCqtVqw+z0TO/mWHJdF6dOnvxzKBSaMU0DgiDCZS4438Wz9dt5PVS37xENDQkIlELXDZCNi3VlZTU9l83t93q9MAwDyWRyvrOz6y9LS3ms75GA7PGhtaUVtrN98u86NTfvt2JxZdv/Ho8H05lMf7VWpaqqwnEcPH7q1O/j8VjOMIyN4zhkr4jm5mYAbFvudoUJgoDdpjelFNfGx79CqQDDMNDWls4cPXbkDb/fD1mWt2hyBLUAOGewTGt32GZSb5X8dpEkKTA5NdUjisK6V48/8cdoJDLruC4I3apPAELAbAa2pVB2mK9p2q5rHADkcrm+Qj4fdxwHqVRqtq/vwbO2bW87cEsuwDmH1+u9WRM3YYwxBPx+hMNh3Ek+m5j8ak2vSwQEJ08+9mog4F80DPOO+iAEokCh+v1gAETO+c0qtGwb+Xx+V0sDAT/NZDL31Ws6enrun+rr63vdsmx84cuCcximic6urvX1W5ZleDwexONxUErBOcftzwpRFFHIr3SMj48/JIoCTp8+/Wo4FPrcdpxdHyF3EjEWiwEAarXaHa2UJAlLy0vNkiTl+/v7P040NLxhWRYsy/rSIAAQR0ZGvpSi1+v99MiRvm9XKrUVwzSX74qyIf8Hl8tDKsiHCMQAAAAASUVORK5CYII="
                                    />
                                </svg>
                            </div>
                            <div class="inclusion-item-rule-content">
                                <p class="inclusion-item-rule-content__name">
                                    International Flights
                                </p>
                                <p class="inclusion-item-rule-content__des">
                                    {{$tour->international_flights ?? 'Flights to/ from the destination'}}
                                </p>
                            </div>
                        </div>

                        <div class="inclusion-item-rule">
                            <div class="inclusion-item-rule__img">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="28px"
                                    height="29px"
                                >
                                    <image
                                        opacity="0.651"
                                        x="0px"
                                        y="0px"
                                        width="28px"
                                        height="29px"
                                        xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAdCAQAAAATWMS6AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfnAgUTGBeVIyJ/AAAEyElEQVQ4y2WUWUxUVxzGf+cuMwwMM4gCWhAEwQ3EuBEFo62tNlQttn2oS9KaUNsmYtJ0i7a29aHVvtSYWlNNTdS2cW2aWkxwq6QgKmrVorI4IjIDKAgCM8MwM/feuX1wxKXf//X8vu87OfkfEQIkejDwsJ11XFaGjTg//nLO7dT7CYgEb3rHNNfMpq6unLCdXJqxM5IIoDAkk3gaJ5yZe7CgLxYUnJrAndycXTnXFn7jkla1pE7BHDotHiVGCKifLr64yBMLc1xjW9xurV/GtGeMbh1zdqIunFrZ8bIjXQOWaGIU7CXg+GLVgVkaJVdeOeVuWdi3i2soZFFKjSNr9PEX9hdCcd3Xu+2dyY+rKgj7+2XH8uJDWw8lVOYEG+hHQyDQ8eL35t8Y4Squ+3BlRX7v2r1bLd0hTCSBgU/esPJYXkLg5x2rKgaCA4CEjh+DMAJBABGOr5q3LaP3fNbnb/tUHQlJRbBt3q+FFuPH3Utr+zAQgJ8UFjKCLDQ0woQZyQvXN+9MCP45/atiXcQhQVfS3pIB9ZOKZTVgwYoC9DCB9YyliBAh/PiYxRqW1206FKZ8cVe6ilJPxaybSVM9JeVHMRH0IAMSIfrRCSEQqLg4jo0QSyt/m3l64sGibo/ki99fKJllR2t8bbTQyQAxqFiQMTGRsaJiw08H7XjYF1pzRDX2FwYTlPup/2Sk+G+rnhljvONbCcXKt9LanlNCDo/zvoXBxPb09tjmTpvbpg1XL6ZfTeyzZT64mdSfpZweBw9s37wDCqurc07VFWx6qc8KuXezD4jQruXnM+C4sbAyufranC3zgzKoOlRMEvPXnp4N6d0j79Vn+2PGeFsdJkX1nY5baY6Q0/DEpnem3b8wUZczvS2OGG2q6+7wOykwuV6WlvQmljTO2j7y7w2uqly3M07b+pPvj9LaUc4zmV51xZUVP3jPfNxSPaXDnhj4ZadxdHLt2JS6UYYhtSWpTD5peHT/vKvPX4SChqzqPp+1e1mFI2ALv37S0eXz51yYfRmKrhbWDvpEZ+EJu9aToIzrkQZFh4U3cZPvTgzmum6Qx15s/Zpup70/QDGNTHXVTM+7c42XuY18N7+jLUYp29Fs/8jtJQ2BdP5c/1u3ZpqXOMpwoQNIIV4llcZzdb3LmyYD3Ti72vbE6cqgJ0yAABoWQgPapTD32IMDBQGAlSpKCPm0i0EgTBAr4SYJ6eEKP1IEgY8arNHVjiDRRDBq8qSkCEEEqQi+xUUJBhuxE8EEBBKCWMqR/w9aySMJOxEGeMBqCmhDIKIZJiYmPjxPtIqCC1iDhAlICEDwGpHHPwsCiUFqSMF4GsyJIrKwTrJkgoV3CaOhYYKw8HCSySYOsAyBSkPU2Zv8/Qd5zcFtgUCAscio5hWC5uWIPlR5FyoOSh+BrofBeOVOu562MSE7sIICFlMbX64EzWHB8FCGQRA/5VFUmRYFAz1TWv/NqC6acVjBilOcmNcTm99a2j341M1MVB5ayZ+hoCATbzgjR6afzX4Q7zMY1bDo9/matHl/agtIT4xAws4ZFIT/UWcMy7qV2xbAiIAu9cXAl0feOzwQ4RlFGM4WrMjrh0pEjMLruW3tsZ54Qy9o/G7fi3/pkWdfDyCOcyj8Byf3C25LP7RwAAAAAElFTkSuQmCC"
                                    />
                                </svg>
                            </div>
                            <div class="inclusion-item-rule-content">
                                <p class="inclusion-item-rule-content__name">
                                    Travel Insurance
                                </p>
                                <p class="inclusion-item-rule-content__des">
                                    {{$tour->travel_insurance ?? 'For the time you travel'}}
                                </p>
                            </div>
                        </div>

                        <div class="inclusion-item-rule">
                            <div class="inclusion-item-rule__img">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="25px"
                                    height="38px"
                                >
                                    <image
                                        x="0px"
                                        y="0px"
                                        width="25px"
                                        height="38px"
                                        xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAmCAQAAACbzPiQAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfnAgUTGQl2Ny5dAAAEMElEQVQ4y4WUe2wUVRSHv5nZbbfdPqQrTYGCJXQR2wC2PAJKgg+aSEzEKPXV2AhqgqhpFCI2BBqlAZWXEQhRgYAKSFJDeAQfaSqKRZRSCgQKhW5bWB7bUnbX7m53Z3bm+sd224EtcM8/5577++bcc+6dKwkWrVdtG97BNAIcIIrEnUNjEhOQYWLzpgWXx5iXLhAeBIgPGcq2O9uqlw+EglxEgXshilq9fEv55fx46Dwh5Hsj8MIep6t62UAOC9wPUbTqqi3lHfkAzffJATIEiVC6J+/K6iXQS+t9coAM3fyCT1tVtfWNW7lXiGBFuavJgAWiae2OG8JZ7/BVVs1aEbjHtjQp7MeHxqs/WTVJl1QEQtEVXb6rKXrxaV+6dMFR7H5/w9N1kizpkpBCeophHTyPMNrGvLuxdgJ1RSl6Y4EgZmsX+jPifqJ1WrO935daPKMz/entf3KBMUy2rfqiJ6/qo930JlQUpZicaLq3carlbGFyoNl6MU3hhDQiUrl2TcWMdZ5A4tZUSVPtkezL7lyLe2jHyLJLigSGVHPum9dXv/fySUMRCYyQJjd+XfLouePjLL7cWUeWrDYUAdj9he0v1XxXtrtcMhLLz+pORb1mTKfgjw/WR00lthRaRP2Tg5cfYl3FqGY5M/O870uOxpIDzrOzf1taPViTG9jKOZ9IlUO2caESdDzQ91t9/Nnhx85MuhPowsdERoc0i/yfLVd9hPFYiPYtTjtS2PJVxe1AmCBOChipqbIcttoNKw/g6L/BcrTy8x1lnSMGAB0NBw6yyDCiskxiP5m7J8u77U1zJBU7qUjYDEOSh4TbLZc4xSka6eoTJAcrNm2bH02OAzdpxYULN9esSYac0dmZp2DDRorpq29vvj7s4Jz4LAMHWWSRSWBYepD53045LdDR0dFMZzBv54zj8XiUMBEi6Mz7YeZhDj5ni3pyE4+tpWBfadw3MDAQ6NbhXZ9W0ZPxoHflJwPSYAJs9Hv7S2XR5kSwZqm992aOILaNDlz4EQiiCAxuESB2oXSr01X+owBBb5qzfc4hgUBFx80JjtGBQMOHm6v0oCIQVK63h6/mCRAImqZajMUbYnncNFFPA4JTNODiOj3oCDZXIPa+IuhDBD8/r4i3dgTTBVdp4ignEfxLA21cR0dXqlcgNi6KaftLq3smLfRw61+zvLTSwBkEJzlNFwGaimb+LYntC+JKU2dczhf3JxvT/1m2eNeU33O7h9aOqClat7CkNtl4ov7YtAFdvxPrSv3j5VtHubNCNm2I16amRYZ75u499KyBQB8M0ejBxw0uJv06dnO5TV314YHxzSkR/PjpNSGSMN2sW+zDjp0kHMmzPTsnJbX2oNPFTPJNqtueec/I1ocEIIukbN1W+1RKjioJDMZ25reYHxuTLV2p+GKGl2688dlru8yq/wGucVafiGenxwAAAABJRU5ErkJggg=="
                                    />
                                </svg>
                            </div>
                            <div class="inclusion-item-rule-content">
                                <p class="inclusion-item-rule-content__name">
                                    Meals not included in the itinerary
                                </p>
                                <p class="inclusion-item-rule-content__des">
                                    {{$tour->meals_not_included ?? 'Drinks and meals not mentioned'}}
                                </p>
                            </div>
                        </div>

                        <div class="inclusion-item-rule">
                            <div class="inclusion-item-rule__img">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="33px"
                                    height="33px"
                                >
                                    <image
                                        opacity="0.471"
                                        x="0px"
                                        y="0px"
                                        width="33px"
                                        height="33px"
                                        xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAhCAQAAAD97QrkAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfnAgUTGTgn6S5nAAAGj0lEQVRIx22VS28caRWG3/q+qq6uqq7qe7vddqd9SduJYyd2hiQzkTMoGQkYsZkZIViAxI5fwL9hg9ixYYUACTQihGSSMAmJE9vtS3ztm/veXdV1r/pYOICZ4V2dc6TzbM7l5f4AAU1UcIw+0kiggS6mJCN3eqmTNbSxbAuRQDalUaI7Wcs2T/QCm0IVRyCYwy2U4ILHBTFEoEVri0/mRwlb9iIBHxAGD7piJoaTzcuKnj7StsQRu9j0vwiNP5qtLDSLncTFaoiQ8yNOZKz00nrWKyzsxQ8w/j8IAkE9uPbmWjV7nkueZER0znIZR/hooBgxSwQGymChW1jM0A10v4EgHLT+2pPbHgE4yJZkKPVSLd4auMNQREYYp2vT7mQvbikh14w171zTYo+5HsILCKroaw8/PI8To5WXud2tvsKE/O46C6V+dNOqpCprscpc5TvtHAOwefU6IY/QeY9giPP1pT+vngMW92aft9p5p8iSaGmdy0C0I+zOoAhi6JX7jdrNZyseD+zMFXXlGdMB+nMk8Wzut3fbKgA8eHPtidy44W9Apc703nJ3klFQNZyz1XED3w0UM9OV/ZM8owEdJCbHqw0H9FMM5d/delkCgPX9+48nOhNYwpeXGqu15Xox4AFQJO10Ny46n5tRUHuqHYvsFgBTCIRCwzbpPTxa+vvyOEpYUv/JX7JnKWSEl+U39/avjuIBDwCMs8V2qjmTjmb6OWvMVG+luzvVUxinSwbSJ/T70T99dFAAFPuTF7n9RKBis/CrT6sZgPokBAFoyPscQlLLDiW+GbEFSH7G2C6aoksZvVEnR/muBgCp8fp2wT3EY7qR66lANFx4O/eKMSDavfF07h3A8HLByIggSAZrx/NtAsCU92ZIpWjKgOiW6vGhiiqeye9SIQfMNFNfO/vgACsI35U2ZkYIfVKTK6QLDZo3exzTAUvcneSPs7YAxMf50w1GoUOGzQGAHpUynHi+t21TaBb8nBLw6ZrCTHyNALnj5OJIdYRqku9qngAoptw6QhELkO1K7zWAWsr+iAQAIEhWuXdSOpkNHRSQQB9boEj1FBPwyUjiDTkggO+NDYYJTCPuRRpPe03N4bsT58vmacEdrzyoNw7i3cB2mQcfIUZ+6AEhHIEGd5kAkH6w0/WeI4M8OGu6c6YO1JC8v1TOFgeJerE+P8upXeKdYRsD9FAvGzmA+VS8FUaAxKC8K3kqQrSxHT4fsIPSaWbsRS35v0ftRk5zM+3JjgUPMWhoLw5yAPFpYsWTQy5rXD9ilgiKQBgoJ1GR3W4vdsZcbQbQjCtv471BihGXV02z5bh5xJCmh1e7GQ4Rm0/qZsITHMnPlnsU79CIG0t60oS+o1XEKgDIVum17JBIpQx0km4spvMAhHgoATyL2fxUpz1hCf3YaXF9J4om2sTJ9+aAr7JXBmPx/B0GkGxtDACSo3kqVIxQLY40QPCyIzJfk23Aip4WDHmMBfygvbpDQ+AwU7/JlcAAl/cm9z54tQRw7FK9PPoQX+A6aZQGKiC6pTN+pq6NG2mgE9+eX61Me1HmN2br+9PA5hLAcQxD7a/3GfEEIKWv1ssuD5sjudOCSwHZXj6kDzxXaWXtiCt0kuuHkuVDtUnHlDvpkJyPlZGADylQbP7i4eVqJiyiJ/36wV4+IHz4wcEPX9MfYV6v5aopxg0Vnk40Aq/NVP1dP2FEYUVDHiBMcnKt8s6dF987sMPX2BTerP5+zaXAbOvHT0t9+lMUHJceFsYR4ChDGd/vOX0YRqGa7XtiPwsIjrRZfHVz83K3jwBvon9c2rg9koCYe3dj/S0H+hlcaIYjHE/4NCAH+YCX+qf+XGiw+EjA0QLH+L72KFETAxf7BMrw2t6dszhAwvW99VeSKYN+DoquN9b52GkWCGm10J2cMiS9GQqA2khFhkJnpT5pHmCLFPP/+Phva0MZAKZbP3v8cSMCHtxvIKCOLXKYqt7bXQQAGipGupU+KZwIwza1wggrhFQ9nq4W9Skz5vIAMH32yy9XqggCsH/7CAtZR350R99b7kUDMtIMrTdRXRQM3nRDQmqyL1uqqVoUACR2Y/vqPxerETYGd9EQGYLO9LN0/6BcLRiREEN1qOJbEtlEI3c4U5lrB3DBfduWdX3lRe70q+X+JUd2RV8ISAgA4EAZ7/Fu1BIbV7aSRzpssPeAbyA4OBi1Yg+XM61SrTBIWJIjhByB4MtWfJRslY536oan/Kf5XP8C+V46o6bwqVoAAAAASUVORK5CYII="
                                    />
                                </svg>
                            </div>
                            <div class="inclusion-item-rule-content">
                                <p class="inclusion-item-rule-content__name">
                                    Personal Expenses :
                                </p>
                                <p class="inclusion-item-rule-content__des">
                                    {{$tour->personal_expenses ?? 'Shopping or personal purchases'}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="book-now-service">
        <div class="container container-tour-service-info">
            <div class="book-now-tour">
                <div class="book-now-tour-guild">
                    <div class="book-now-tour-guild__img">
                        <img src="{{asset('clients/assets/images/avatar/avatar1.png')}}" />
                    </div>
                    <div class="book-now-tour-guild-des">
                        <p class="book-now-tour-guild-des__name">
                            Book this tour/ Personalize it your way
                        </p>

                        <p class="book-now-tour-guild-des__des">
                            To get the best price of this tour, please procced your
                            booking. Our tour expect will get back to your with our best
                            offer and details.
                        </p>
                    </div>
                </div>

                <div class="book-now-tour-btn">
                    <a class="btn btn-book-this-tour" href="{{route('client.tours.book_tour_form',$tour->slug_custom_name)}}">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink"
                            width="30px"
                            height="22px"
                        >
                            <image
                                x="0px"
                                y="0px"
                                width="30px"
                                height="22px"
                                xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAWCAQAAAB9auREAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfnAgUUCC/yrJ41AAAB3UlEQVQ4y42US0jVQRSHv7lcspACa1FRUEi0jGgVBAaCywpbFYU9JRB6QG1rL7SzIISUQCgiiHARLaOgQMJF2c6FgZCkGYUuvtH/aaHdR1flnmEY5sB3fnNmzpwkbKaHHg5SojkLJhlJQ7GYbGWASwCpiNQUnADS8+hN9jIIDKSXUA8norLWzSK6uQHcKXMVIO1kPH41eezd6UAAXCTPWBiGE3ZJ7cg1a656TziZw3DJGZx12cd+yZHn7avFqnAFL3nLP4Zj9rvoHP4w7HS/bw3DR26v1anT3uGwYfjGvR61cHYF7hbbHDQM33loTfiw7w0jP3Cb2FWFT4uY8k3nDafyyQb4lN8Mf9q3GrgBJmNnnjBc8J4tFXiTd10w/OzxSkprwuR2XxmGL9wj4i6fGoajttdc4zowbrHfJcNxz3vWMcNl7+fW6t1n8rowki84bayO715peIEuC2fL9RVfsSd85TLHSHxIQ/Fx7VIrr1uEY3yiDZinWO9/NMA16gVzGxd5iVRP0Oio86SKRqSVBrC8Ufz4b59WnAlKTEHqaPIzVq0DmCkzzJG4Tub1xvp1qXZyGxhJbmWQM4As0VwbKtECaTR6ktDKNc6xj1K19fzLLhrIRBRM8yw9jN9/AeyPuVHrpNw6AAAAAElFTkSuQmCC"
                            />
                        </svg>
                        <span>Book this tour</span>
                    </a>
                    <a class="btn btn-tailor-made" href="{{route('client.customize_tour')}}">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink"
                            width="32px"
                            height="32px"
                        >
                            <image
                                x="0px"
                                y="0px"
                                width="32px"
                                height="32px"
                                xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH5wIFFAkkfGV2/AAACS9JREFUWMOdl32sZndRx78z83s55zzPvbeBUNhml1KKJHaBWHHpdtHKKhLT0qAhooRIVUqVRk0qNdFE0yIQE16a1ApFgrxUIaBg1Fak0sZCadluYktMaXdrQl9oqbR2X+59nvOc83uZ8Y/n3vXusm3ASU7OycnMmc/M7/ebmUNHjxwBABARxDmUUlBVkVMCM8OLwACvZjtgto+JLgjen+e83yUiUwCoqn1O6bsppQeI6GBR/Wat9fGubVOpFaoKLwIWATNDVWFmS7/PBUBEIXr/MjV7W9O2v+yd22WqZMDTpZTHSylHYQbv/RqL7GSiM5kIRfXJYRxvIeDTuZTDZjb+aAC1opTyIiH6jdW1tSsY2FlVD48pfbnv+7uZ+eHJZDJLKSWYIcbo1zc2pmb2kslkckEM4RIW+XGYPb0xm31azT4uzN+VHxKAaq3nhxCu7WK8tNT6wGIYPslEt5dSHko59yKCruuQUsImAGbzOWqt8M613vtz1ex1bdO8wzn3Eynnr47jeI2I3MPMuh3A4WQhx/zaJoTrnPd7Uik3Lfr+egD3ijtV9fRCRAsiul9rvX/e93c0TXNlCOGdwrwjl/JuALcB0C193nqwpfGepm2vc86d3/f9teMwvIuZ7/2hPP8gCUTk/nEcr+oXiz8WkZfGED7MzD+9FT0AMBFtEtgOJ3INM+8ZxvF9Oef3ikivuoRVVeh2Q+ZtvghmdiKtqgotZUtvzDl/aBzHPxGRV3jnriWic7bsOOUMEMF5/1uO+eKc802qep2ZqW4eITNDUUWtFVvAfd8vnZlhTAnee3jvASKoKnLOSwfMS/tabyylfISI9jPzu8yMUkrgNI6A6nkxhHeC6H4t5QOOecPMUEs54ZBOiXY+nwNmUFWM4wjvPUIIABFo8wIAYYbWipzzUGr9MIB7YgiXmdlPpnEEExGxyGVMdFbO+a9LrQ8aEZgIBrjtqd0uz7UEp+4GmO1U1VU1e7jW+glmPsM599tE5J2InO29f1Mp5fCY8+2qqkIkzrlf8N7/mpppKeULtdavbt+9zyW2zEw0s59j5jd5758ZxvETMFs3sztKrfcF739RRH7Mee/3eefOXgzDX9RaDznniICfdd5fnUtZIyLfte0r+r6XCtxKRLWWApw+WqjqWdH7C1em00ud9xea2XEi+oyqHlssFhDnHnEi/9S27TXOuZ9xInIBAB2G4U7vXBazaMA+VX2Jmf1RyflgcO6zzPx2EbnLgOOnSXU0sxcT0Z4mxjd3TfN6ANQPw9+Z6o0GHCZgrqog1bLI+e6maQbv/T7nnNtdSvn+bDZ7NIaAtbW1rLV+2zt3jJn3zlM6oGZPeu/3llpfZ6q3Ahg2a8jzAJzhnLvYO/dLIYRXM/Ok1Hr3YhiuT+N4Wwzh+NY+GcYRGEeo6hMr0+mjzrndzjm3K+f8GBFt2HIB1YBbq+pLvXNXTqbTS53IxDn3QhF533w+f3kGvsnMK0T0Su/989u2/dXg/c5a61Oz+fyjavYRAE/YaVK1+arPOT8SYzzfMfO01np0dXU1A0CuFSLSD+N4/fr6+tfatt2fiXxV3dHEeNHqysq1q6uryDl/r9Z6p3Nud875P8Zx/FdV/fuqeqeI5FO8QryH837rTa2lHHNd5xwtqU7Asghi28J7bzPV46WUp733rZlNc86HSin/xcwvCiGcaWbRe/8qEXnVMI7/UHP+H5hpUYWIwDkHYkaIEToM24+pgciIiJyaLZzIGevr6y42DVa7DrXWoKpvn3Td1SQycSIiIs9LKT28MZvdMKZ0CxHtbpvmLUT0VIzxzOl0+tbk3J7ZfP5RAJ8CcHx7EkTkRDFb1ieemlnmkvPj3rldAKa2LLsEs59nosur2V3rGxsXp5zvWQzDkfl8/n4AnyWix7TWf6u1XjObzd567NixP1/0/X3i/Vlra2sf7Nr2JgD7zWyyHaKUgpwzUkqd8/6cXMoRN6b0wMpk8urYNLsAfDsNQzTVnyLm56vqzQQ8ZcCklnLQzP5lW2QVwGNE9FhK6VtjSv8oIvu6rntLiPENLfOexWJxExF9alNvyLVaLQU+hLOcc+cs+v4rbGYHidk1MV4IwJtZEuZ7mfm/mfndbdt+UZYwf6NmG1tH6qRaS9QD+M/FYvGxYRh+ZxyGK2utB5um+RUR+UtVfbOZrfCyRUsM4TVM1KWUDjhV/UZV/V4b4yUppc/3w/Dg2srK7VprU2u9jJlrKeVvVfVmMysA4EWgtf5g0V92v4dKKQ+llP5ZRF5vZpeo6m4A3xDmde/9zibGN6rqUQO+7kopD4/j+OWuba8I3l+Ua33QzIZa65dms9ltQpS76bTfHrU+e+PZDvJMSukLeRhuIZFd4v3xtmngmF/rnNubUvoSgENsZrXk/Ek1O9K27Tuapjm3LIdSsMhxYj7hfOseQkAI4US/xyaQmZ20PJtdcu69P9SEcLRpmh2haS4HUUo538jMA8emATv3rVzKZ0RkTwjhKjVrUs4QZjAzcs6AGXLOUFXEbQDCDHEObdPAe49hWWqhqiAiNF2HGCPatnXe+98Vkf3jOH5OVQ8wMzjGCHHOVPVj1ewOx3ylAFeYKngzwrI5XpVNkFN7PzuHpmngncO4CbCVjabrQMsp6deF6A+s1oOllBuYuTIz2My2WusjOef3lFoPNTG+34Xw+6rqtqf+1N2/vdSebgkAQGslEfnNGOOHDPi+qr6HiB7cmpr4JGWzr48pXa1mT6yurHywbZoP1Fpfhv+nqOqLnXN/1jbNDaWUjcU4/qECt2I5hAM45b+AAK2qXxnGcRa9f2/XdVeJ93uHYfi4qt5lZt/ZLEDPKmZGqnqOiOxtm+byJsb9qnrPvO//1Hn/79H7k+xpY2PjpA/UWkEATPVcIvq9ruveBqIup3TvbDa72cwOTKfTJ8eU5maWHTOqmYshTPq+f2EFLph23Ru9969hojSk9EUyu64fhsMhBIveI+X8f79mzwpghmEYWifyytg0VwTv30DMLzDVRSnlO7mUR81s3YlYVV3xzp0dvD+XmCe11mfSON4+5vxXILqvCaFfDAN8CPhRASDL6TfmnF/OIhc1IVwYYzzPef8CA8LmWU+1lKPjOD6QUjpQa/2aiBxi5gWY0YSAZwP4X45800wGKuCSAAAAAElFTkSuQmCC"
                            />
                        </svg>
                        <span>TAILOR MADE </span>
                    </a>
                </div>
            </div>
        </div>
    </section>


    <section class="tour-highlight container container-tour-service-info">
        <div class="tour-highlight-head">
            <p>tour highlights</p>
        </div>

        <div class="tour-highlight-slide">
            <div
                id="carouselTourHighlight"
                class="carousel slide"
                data-bs-ride="true"
            >
                <div class="carousel-indicators">
                    @foreach($toursHighLight->chunk(6) as $toursHighLightItem)
                        <button
                            type="button"
                            data-bs-target="#carouselTourHighlight"
                            data-bs-slide-to="{{$loop->index}}"
                            class="btn-change-slide-dot {{$loop->first ?'active' :''}}"
                         {{$loop->first ?'aria-current="true"' :''}}
                            aria-label="Slide {{$loop->index+1}}"
                        ></button>
                    @endforeach

                </div>
                <div class="carousel-inner">
                    @foreach($toursHighLight->chunk(6) as $toursHighLightItem)
                        <div class="carousel-item {{$loop->first ?'active' :''}}">
                            <div class="row">
                                @foreach($toursHighLightItem as $item)
                                    <div class="col-12 col-sm-4">
                                        <div class="tour-highlight-item">
                                            <div class="tour-highlight-item__img">
                                                <img data-src="{{$item->thumbnail_path}}"
                                                    alt="{{$item->name}}"
                                                    class="lazyload"
                                                />
                                            </div>
                                            <div class="tour-highlight-item-content">
                                                <a class="tour-highlight-item-content__name text-decoration-none text-dark
                                                    " href="{{route('client.slug',$item->slug_custom_name)}}">

                                                    {{$item->name}}
                                                </a>
                                                <p class="tour-highlight-item-content__des text-over2">
                                                    {{$item->desc}}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </section>


    <section class="similar-tour-service container mt-5 mb-5 container-tour-service-info">
        <div class="similar-tour-service-head">
            <p>similar tours</p>
        </div>
        <div class="row">

            @foreach($toursSimilar as $item)
                <div class="col-12 col-sm-4">
                    <div class="similar-tour-item">
                        <div class="similar-tour-item__img">
                            <img
                                data-src="{{$item->thumbnail_path}}"
                                class="lazyload"
                                alt=""
                            />
                        </div>
                        <div class="similar-tour-item-content">
                            <a class="similar-tour-item-content__name text-decoration-none text-dark
                        " href="{{route('client.slug',$item->slug_custom_name)}}">
                                {{$item->name}}
                            </a>
                            <p class="similar-tour-item-content__des text-over2">
                                {{$item->desc}}
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>

        <div class="d-flex justify-content-center mt-5">
            <a class="btn btn-silimar-more" href="{{route('client.slug',$tour?->categories?->first()?->slug_custom_name)}}">MORE TOURS</a>
        </div>
    </section>

    <section class="line-service"></section>
@endsection
@section('script')
    <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOK6r2qDrP56Jph6-eHaUHYr17stCRFD8"></script>
    <script>
        let tourDetails = @json($tour->tourDetails);

        $(function () {
            $(document).on('click', '.expand-all-day',function () {
               if($(this).hasClass('active-all')){
                   $(this).removeClass('active-all')
                   $('.accordion-button').addClass('collapsed');
                   $('.collapse').removeClass('show');
               }else{
                   $(this).addClass('active-all')
                   $('.accordion-button').removeClass('collapsed');
                   $('.collapse').addClass('show');
               }
            })
        })

    </script>
    <script src="{{asset('admin/googlemap/googleTourMap.js')}}"></script>
@endsection
