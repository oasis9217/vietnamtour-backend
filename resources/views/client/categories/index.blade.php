
@section('style')
    <link rel="stylesheet" href="{{asset('clients/assets/css/sub-category.css')}}" />
@endsection

@section('meta')
    {!! $tourCategory?->meta !!}
@endsection
@section('title')
    {!! $tourCategory->title ?? $tourCategory->name !!}
@endsection
@extends('client.layouts.app')
@section('header_image')
    <div class="header-image">
        <img data-src="{{asset('clients/assets/images/tours/banner.png')}}" class="img-response lazyload" alt="" />
    </div>
@endsection
@section('header_banner')
    <div class="header-banner header-banner-tour">
        <p class="header-banner-tour__des">
            Vietnam Private Tour for discerning travelers
        </p>
        <div class="btn-group header-banner-tour__btn" role="group">
            <a href="{{route('client.customize_tour')}}" class="btn btn-outline-light px-4">
                Tailor Made
            </a>
        </div>
    </div>
@endsection
@section('main')
    <section class="tour-service container">
        <div class="tour-service-head">
            <div class="tour-service-head__title">
                <h1>{{$tourCategory->name}}</h1>
                <img
                    class="tour-service-head__line"
                    src="{{asset('clients/assets/images/tours/tour-title-line.png')}}"
                    alt=""
                />
            </div>

            <div class="tour-service-head__des">
                <p>
                    The opening of Vietnam means you are free to explore more the
                    hiden gems of the country. From exotic markets to vibrant cities,
                    Vietnam is very tempting. Keep your best memory of Vietnam with
                    our carefully designed tours
                </p>
                <p>
                    From sun-drenched beaches to dramatic mountains, our Vietnam To
                    bring you to all the places of different characters. You`ll hav to
                    sample the best of our country like no others
                </p>
            </div>
        </div>

        <div class="tour-items row mt-5">
            @foreach($tours as $item)
             <div class="col-12 col-sm-4 col-md-4">
                <div class="tour-item">
                    <a class="tour-item__img" href="{{route('client.slug',$item->slug_custom_name)}}">
                        <img data-src="{{$item->image_path}}" class="lazyload" alt="{{$item->name}}" />
                    </a>
                    <p  class="tour-item__name text-uppercase">{{$item->name}}</p>
                    <div class="tour-item-des">
                        <div class="tour-item-des__day">
                            {{$item->day}} days
                        </div>
                        <div class="tour-item-des__content text-over2">
                            {{$item->desc}}
                        </div>
                        <a href="{{route('client.slug',$item->slug_custom_name)}}" class="tour-item-link">View this tour</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        <div class="row justify-content-center pb-4 pt-5">
            <a class="btn btn-more-blog" href="{{route('client.slug',$tourCategory->slug_custom_name)}}">View more tour</a>
        </div>
    </section>
    <section class="line-service"></section>
@endsection
@section('script')
    <script>
        $(()=>{
            let imageWidth = document.querySelector('.tour-item__img').clientWidth;
            let heightImage = +imageWidth * 1.4;
            if(window.innerWidth > 567)
            {
                $('.tour-item__img').css('height', `${heightImage}px`)
            }

            const resizeObserver = new ResizeObserver(entries => {
                for (let entry of entries) {
                    let imageWidth = document.querySelector('.tour-item__img').clientWidth;
                    let heightImage = +imageWidth * 1.4;
                    if(window.innerWidth > 567)
                    {
                        $('.tour-item__img').css('height', `${heightImage}px`)
                    }
                }
            });
            resizeObserver.observe(document.body); // Ad
        })


    </script>
@endsection
