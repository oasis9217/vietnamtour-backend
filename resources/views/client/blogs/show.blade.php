
@section('style')
    <link rel="stylesheet" href="{{asset('clients/assets/css/blog-detail.css')}}" />
@endsection
@extends('client.layouts.app')
@section('meta')
    {!! $post?->meta !!}
@endsection
@section('title')
    {!! $post->name !!}
@endsection

@section('header_image')
    <div class="header-image">
        <img data-src="{{$post->header_image_path}}" class="img-response lazyload" alt="" />
    </div>
@endsection
@section('header_banner')
    <div class="header-banner header-banner-tour"></div>
@endsection
@section('main')
    <section class="content">
        <div class="content-head">
            <div class="content-head__title">
                <h1>{{$post->name}}</h1>
                <img
                    class="blog-service-head__line"
                    src="{{asset('clients/assets/images/tours/tour-title-line.png')}}"
                    alt=""
                />
            </div>
        </div>
        <div class="content-body">
            {!! $post->content !!}
        </div>
    </section>
    <section class="moreblog-service">
        <div class="container">
            <div class="moreblog-service-head">
                <p class="moreblog-service-head__title">
                    <span>More travel inspiration</span>
                    <img
                        class="moreblog-service-head__line"
                        src="{{asset('clients/assets/images/tours/tour-title-line.png')}}"
                        alt=""
                    />
                </p>

                <div class="moreblog-service-head__des"></div>
            </div>
            <div class="posts">
                <div class="blog-items row">
                    @foreach($postsInspiration as $item)
                    <div class="col-12 col-sm-4 col-md-4">
                        <div class="blog-item">
                            <a class="blog-item__img">
                                <img data-src="{{$item->image_path}}" class="lazyload" alt="{{$item->name}}" />
                            </a>
                            <a class="blog-item__name text-center d-block text-dark" href="{{route('client.slug',$item->slug_custom_name)}}">{{$item->name}}</a>
                            <div class="blog-item-des">
                                <div class="blog-item-des__content text-over2">
                                    {{$item->desc}}
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>

            <div class="d-flex justify-content-center pb-5" style="margin-top: 100px">
                <a class="btn btn-more-blog" href="{{route('client.slug', $categorySlug ?? $item->slug_custom_name)}}">View all articles</a>
            </div>
        </div>

    </section>
    <section class="line-service mt-5"></section>
@endsection
@section('script')
    <script>
        document.querySelectorAll('figure.table').forEach(a => {
            a.style.margin = "0 auto"
        })
    </script>
@endsection
