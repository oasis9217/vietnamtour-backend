@section('style')
    <link rel="stylesheet" href="{{asset('clients/assets/css/blog.css')}}"/>
@endsection
@extends('client.layouts.app')
@section('header_image')
    <div class="header-image">
        <picture>
            <source
                media="(max-width: 767px)"
                data-srcset="{{asset('clients/assets/images/header_bg_sm.png')}}"
                data-sizes="auto"
                class="lazyload"
            />
            <source
                media="(min-width: 768px)"
                data-srcset="{{asset('clients/assets/images/header_bg.png')}}"
                data-sizes="auto"
                class="lazyload"
            />
            <img
                src="{{asset('clients/assets/images/header_bg.png')}}"
                alt="Image description"
                class="img-response lazyload"
            />
        </picture>
{{--        <img data-src="{{asset('clients/assets/images/header_bg.png')}}" class="img-response lazyload" alt=""/>--}}
    </div>
@endsection
@section('title')
    {!! $postCategory->title ??  $postCategory->name !!}
@endsection
@section('meta')
    {!! $postCategory->meta !!}
@endsection
@section('header_banner')
    <div class="header-banner header-banner-tour">

    </div>
@endsection
@section('main')
    <section class="blog-service container">
        <div class="blog-service-head">
            <div class="blog-service-head__title">
                <h1>Travel Inspiration</h1>
                <img
                    class="blog-service-head__line"
                    src="{{asset('clients/assets/images/tours/tour-title-line.png')}}"
                    alt=""
                />
            </div>

            <div class="blog-service-head__des">
                <p>
                    The opening of Vietnam means you are free to explore more the
                    hiden gems of the country. From exotic markets to vibrant cities,
                    Vietnam is very tempting. Keep your best memory of Vietnam with
                    our carefully designed tours
                </p>

            </div>
        </div>
        @if(!(request()->page && request()->page > 1))
        <div class="posts ">
            <div class="blog-items row">

                @foreach($postsInspiration as $item)
                    <div class="col-12 col-sm-4 col-md-4">
                        <div class="blog-item">
                            <a class="blog-item__img" href="{{route('client.slug',$item->slug_custom_name)}}">
                                <img
                                    src="{{$item->image_path}}"
                                    alt="{{$item->name}}"
                                />
                            </a>
                            <a class="blog-item__name d-block text-center text-dark" href="{{route('client.slug',$item->slug_custom_name)}}">{{$item->name}}</a>
                            <div class="blog-item-des">
                                <div class="blog-item-des__content text-over2">
                                    {{$item->desc}}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
        @endif
    </section>

    @if(!(request()->page && request()->page > 1))
    <section class="article-service">
        <div class="article-service-image">
            <img data-src="{{asset('clients/assets/images/feature-article/background.png')}}" class="lazyload" />
        </div>
        <div class="article-service-content">
            <div class="article-service-head">
                <p class="article-service-head__title">
                    <span>Featured Article </span>
                    <img
                        class="article-service-head__line"
                        src="{{asset('clients/assets/images/tours/tour-title-line.png')}}"
                        alt=""
                    />
                </p>
            </div>
            <div class="article-service-des pt-2 pt-sm-2 pt-md-2">
                <p class="article-service-des__title">How to spend 3 days with halong bay</p>
                <p class="article-service-des__des">For those who are seeking for an idea to explore Halong </p>
            </div>
        </div>
    </section>

    <section class="suggetion-service">
        <div class="suggetion-service-head">
            <p class="suggetion-service-head__title">
                <span>Insider Travel Suggestion </span>
                <img
                    class="suggetion-service-head__line"
                    src="{{asset('clients/assets/images/tours/tour-title-line.png')}}"
                    alt=""
                />
            </p>
        </div>
        <div class="suggetion-service-des">
            <p>Autumn is a wonderful time to admire the rice
                terraces in northern Vietnam. From Mu Cang
                Chai to Sapa, Ha Giang, picture-poscard sites
                are abundant !
            </p>
        </div>
    </section>

    @endif

    <section class="moreblog-service container">
        @if(!(request()->page && request()->page > 1))
        <div class="moreblog-service-head">
            <p class="moreblog-service-head__title">
                <span>More Travel Articles</span>
                <img
                    class="moreblog-service-head__line"
                    src="{{asset('clients/assets/images/tours/tour-title-line.png')}}"
                    alt=""
                />
            </p>

            <div class="moreblog-service-head__des">
            </div>
        </div>
        @endif
        <div class="posts ">
            <div class="blog-items row">

                @foreach($morePosts->get('posts') as $item)
                    <div class="col-12 col-sm-4 col-md-4">
                        <div class="blog-item">
                            <a class="blog-item__img" href="{{route('client.slug',$item->slug_custom_name)}}">
                                <img
                                    src="{{$item->image_path}}"
                                    alt=""
                                />
                            </a>
                            <a class="blog-item__name d-block text-center text-dark" href="{{route('client.slug',$item->slug_custom_name)}}">{{$item->name}}</a>
                            <div class="blog-item-des">
                                <div class="blog-item-des__content text-over2">
                                    {{$item->desc}}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="d-flex justify-content-end more-post-paginate" style="margin-top: 102px">
            <ul class="pagination m-0">
                @if($morePosts->get('meta')['first'])
                <li class="page-item">
                    <a class="page-link" href="{{$morePosts->get('meta')['first']}}" rel="prev" aria-label="« Trang sau">«</a>
                </li>
                @endif
                @if(count($morePosts->get('links')) > 1)
                    @foreach($morePosts->get('links') as $key => $val)
                            @if($morePosts->get('currentPage') ==  ($key+1) )
                                <li class="page-item active" aria-current="page"><span class="page-link">{{$key+1}}</span></li>

                            @else
                                <li class="page-item"><a class="page-link" href="{{$val}}">{{$key+1}}</a></li>

                            @endif
                        @endforeach
                 @endif
                    @if($morePosts->get('meta')['first'])
                        <li class="page-item">
                            <a class="page-link" href="{{$morePosts->get('meta')['first']}}" rel="next" aria-label="Trang trước »">»</a>
                        </li>
                    @endif
            </ul>
        </div>
    </section>
    <section class="line-service"></section>
@endsection
@section('script')
    <script>
        $(()=>{
            let imageWidth = document.querySelector('.blog-item__img').clientWidth;
            let heightImage = +imageWidth * 1.4;
            if(window.innerWidth > 567)
            {
                $('.tour-item__img').css('height', `${heightImage}px`)
            }

            const resizeObserver = new ResizeObserver(entries => {
                for (let entry of entries) {
                    let imageWidth = document.querySelector('.blog-item__img').clientWidth;
                    let heightImage = +imageWidth * 1.4;
                    if(window.innerWidth > 567)
                    {
                        $('.tour-item__img').css('height', `${heightImage}px`)
                    }
                }
            });
            resizeObserver.observe(document.body); // Add this line if the image is initially hidden
        })
    </script>
@endsection
