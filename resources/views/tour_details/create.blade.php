@extends('layouts.app')
@push('page_css')
    <link rel="stylesheet" href="{{asset('admin/ckeditor/css/ckeditor.css')}}">
@endpush
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                     @lang('models/tourDetails.singular')
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::open(['route' => 'tourDetails.store']) !!}

            <div class="card-body">
                <div class="row">
                    @include('tour_details.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Lưu', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('tourDetails.index') }}" class="btn btn-default">
                 @lang('crud.cancel')
                </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
@push('page_scripts')
    <script src="{{ asset('ckeditor5-classic/build/ckeditor.js') }}"></script>
    <script src="{{ asset('ckfinder/ckfinder.js') }}"></script>
    <script src="{{ asset('admin/ckeditor/js/init.js') }}"></script>

@endpush
