@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                   @lang('models/tourDetails.plural')
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-primary float-right"
                       href="{{ route('tourDetails.create') }}">
                         @lang('crud.add_new')
                    </a>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-body p-0 table-responsive">
                <form id="search_form" action="{{ route('tourDetails.index') }}" method="get">
                    {{-- Setting --}}
                    <div class="row p-3">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <select class="form-control selectpicker" name="tour_id" data-live-search="true">
                                    <option value="">Tên Tour</option>
                                    @foreach($tours as $tour)
                                        <option {{ request('tour_id') == $tour->id ? 'selected' : '' }} value="{{ $tour->id }}">{{ $tour->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3 form-group">
                            <button class="btn btn-primary" type="submit">Tìm kiếm</button>
                        </div>
                    </div>
                </form>
                <table class="table table-hover">
                    <tr>
                        <th>Id</th>
                        <th>Tên</th>
                        <th>Tour</th>
                        <th>{{__('models/tourDetails.fields.lat')}}</th>
                        <th>{{__('models/tourDetails.fields.lpg')}}</th>
                        <th>Action</th>
                    </tr>
                    @foreach($tourDetails as $tourDetail)
                        <tr>
                            <td>{{ $tourDetail->id }}</td>
                            <td>{{ $tourDetail->name }}</td>
                            <td>{{ $tourDetail?->tour?->name }}</td>
                            <td>{{ $tourDetail->lat }}</td>
                            <td>{{ $tourDetail->lpg }}</td>
                            <td>
                                <form method="POST" action="{{ route('tourDetails.destroy', $tourDetail->id) }}" accept-charset="UTF-8">
                                    @csrf
                                    @method('DELETE')
                                    <div class="btn-group">
                                        <a href="{{ route('tourDetails.show', $tourDetail->id) }}" class="btn btn-default btn-xs">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="{{ route('tourDetails.edit', $tourDetail->id) }}" class="btn btn-default btn-xs">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <button type="submit" class="btn btn-danger btn-xs" onclick="return confirm(&quot;Are you sure?&quot;)"><i class="fa fa-trash"></i></button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
                {!! $tourDetails->appends(Request::all())->links() !!}
                <div class="card-footer clearfix float-right">
                    <div class="float-right">

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


