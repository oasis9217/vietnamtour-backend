<div class="form-group col-sm-12">
    <label for="post_category_id">Tour</label>
    <select class="form-control selectpicker" name="tour_id" id="tour_id" data-live-search="true">
        @foreach($tours as $tour)
            <option {{ isset($tourDetail) ? $tourDetail->tour_id == $tour->id ? 'selected' : '' : '' }} value="{{ $tour->id }}">{{ $tour->name }}</option>
        @endforeach
    </select>
</div>



<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', __('models/tourDetails.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Desc Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Nội dung ngày tour') !!}
    {!! Form::textarea('desc', null, ['class' => 'form-control']) !!}
</div>

<!-- Lat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lat', __('models/tourDetails.fields.lat').':') !!}
    {!! Form::text('lat', null, ['class' => 'form-control lat','step' =>'0.1']) !!}
</div>

<!-- Lpg Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lpg', __('models/tourDetails.fields.lpg').':') !!}
    {!! Form::text('lpg', null, ['class' => 'form-control lpg' ,'step' =>'0.1']) !!}
</div>

<div class="form-group col-sm-12">
    <label for="post_category_id">Vị trí Toạ độ </label>
    <select class="form-control selectpicker" id="tourDetailLat" data-live-search="true">
        <option  value="">Chọn Vị trí Toạ độ </option>
        @foreach(config('data.tour_detail_lat') as $item)
            <option  value="{{ $item['lat'].'-'.$item['lpg'] }}" class="text-capitalize">{{ $item['name'] }}</option>
        @endforeach
    </select>
</div>

<!-- Content Field -->
<div class="form-group col-sm-12">
    {!! Form::label('content', __('models/tourDetails.fields.content').'(hiển thị trên bản đồ google api):') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control editor']) !!}
</div>
