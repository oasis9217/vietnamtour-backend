<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/tourDetails.fields.id').':') !!}
    <p>{{ $tourDetail->id }}</p>
</div>

<!-- Tour Id Field -->
<div class="col-sm-12">
    {!! Form::label('tour_id', __('models/tourDetails.fields.tour_id').':') !!}
    <p>{{ $tourDetail->tour_id }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/tourDetails.fields.name').':') !!}
    <p>{{ $tourDetail->name }}</p>
</div>

<!-- Desc Field -->
<div class="col-sm-12">
    {!! Form::label('desc', __('models/tourDetails.fields.desc').':') !!}
    <p>{{ $tourDetail->desc }}</p>
</div>

<!-- Lat Field -->
<div class="col-sm-12">
    {!! Form::label('lat', __('models/tourDetails.fields.lat').':') !!}
    <p>{{ $tourDetail->lat }}</p>
</div>

<!-- Lpg Field -->
<div class="col-sm-12">
    {!! Form::label('lpg', __('models/tourDetails.fields.lpg').':') !!}
    <p>{{ $tourDetail->lpg }}</p>
</div>

<!-- Content Field -->
<div class="col-sm-12">
    {!! Form::label('content', __('models/tourDetails.fields.content').':') !!}
    <p>{{ $tourDetail->content }}</p>
</div>

<!-- Sort Field -->
<div class="col-sm-12">
    {!! Form::label('sort', __('models/tourDetails.fields.sort').':') !!}
    <p>{{ $tourDetail->sort }}</p>
</div>

