<div class="table-responsive">
    <table class="table" id="tourDetails-table">
        <thead>
        <tr>
            <th>@lang('models/tourDetails.fields.id')</th>
        <th>@lang('models/tourDetails.fields.tour_id')</th>
        <th>@lang('models/tourDetails.fields.name')</th>
        <th>@lang('models/tourDetails.fields.desc')</th>
        <th>@lang('models/tourDetails.fields.lat')</th>
        <th>@lang('models/tourDetails.fields.lpg')</th>
        <th>@lang('models/tourDetails.fields.content')</th>
        <th>@lang('models/tourDetails.fields.sort')</th>
            <th colspan="3">@lang('crud.action')</th>
        </tr>
        </thead>
        <tbody>
         @foreach($tourDetails as $tourDetail)
            <tr>
                <td>{{ $tourDetail->id }}</td>
            <td>{{ $tourDetail->tour_id }}</td>
            <td>{{ $tourDetail->name }}</td>
            <td>{{ $tourDetail->desc }}</td>
            <td>{{ $tourDetail->lat }}</td>
            <td>{{ $tourDetail->lpg }}</td>
            <td>{{ $tourDetail->content }}</td>
            <td>{{ $tourDetail->sort }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['tourDetails.destroy', $tourDetail->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('tourDetails.show', [$tourDetail->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('tourDetails.edit', [$tourDetail->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
         @endforeach
        </tbody>
    </table>
</div>
