@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                   @lang('models/posts.plural')
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-primary float-right"
                       href="{{ route('posts.create') }}">
                         @lang('crud.add_new')
                    </a>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-body p-0 table-responsive">
                <form id="search_form" action="{{ route('posts.index') }}" method="get">
                    {{-- Setting --}}
                    <div class="row p-3">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <select class="form-control selectpicker" name="post_category_id" data-live-search="true">
                                    <option value="">Thể loại</option>
                                    @foreach($categories as $category)
                                        <option {{ request('post_category_id') == $category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" name="name" value="{{ request('name') }}" class="form-control" placeholder="Tìm kiếm theo tên ...">
                        </div>
                        <div class="col-sm-3 form-group">
                            <button class="btn btn-primary" type="submit">Tìm kiếm</button>
                        </div>
                    </div>
                </form>
                    <table class="table table-hover">
                    <tr>
                        <th>Id</th>
                        <th>Tên</th>
                        <th>Mô tả</th>
                        <th>Ảnh</th>
                        <th>Thumbnail</th>
                        <th>Ảnh Header</th>
                        <th>Action</th>
                    </tr>
                    @foreach($posts as $post)
                        <tr>
                            <td>{{ $post->id }}</td>
                            <td>{{ $post->name }}</td>
                            <td>{{ $post->desc }}</td>
                            <td><a href="{{  $post->image_path }}" target="_blank">{{ $post->image }}</a></td>
                            <td><a href="{{ $post->thumbnail_path }}" target="_blank">{{ $post->thumbnail }}</a></td>
                            <td><a href="{{  $post->header_image_path }}" target="_blank">{{ $post->header_image }}</a></td>

                            <td>
                                <form method="POST" action="{{ route('posts.destroy', $post->id) }}" accept-charset="UTF-8">
                                    @csrf
                                    @method('DELETE')
                                    <div class="btn-group">
                                        <a href="{{ route('posts.show', $post->id) }}" class="btn btn-default btn-xs">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-default btn-xs">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <button type="submit" class="btn btn-danger btn-xs" onclick="return confirm(&quot;Are you sure?&quot;)"><i class="fa fa-trash"></i></button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
                {!! $posts->appends(Request::all())->links() !!}
                <div class="card-footer clearfix float-right">
                    <div class="float-right">

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


