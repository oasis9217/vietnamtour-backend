<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', __('models/posts.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    <label for="post_category_id">Thể loại</label>
    <select multiple class="form-control selectpicker" data-live-search="true" name="post_category_id[]" id="post_category_id">
        @foreach($categories as $category)
            <option {{ isset($post) ? (in_array($category->id, $post->categories->pluck('id')->toArray()) ? 'selected' : '') : ($categories->count()==1? 'selected' : '') }} value="{{ $category->id }}">{{ $category->name }}</option>
        @endforeach
    </select>
</div>

<!-- Desc Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('Excerpt') !!}
    {!! Form::textarea('desc', null, ['class' => 'form-control']) !!}
</div>

<!-- Content Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content', __('models/posts.fields.content').':') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control editor', 'rows'=>'10']) !!}
</div>

<p class="col-12 font-weight-bold">Setting</p>
<div class="form-group col-sm-6 col-md-6 col-lg-3">
    <div class="form-check">
        {!! Form::hidden('is_inspiration', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('is_inspiration', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('is_inspiration', __('Is Inspiration'), ['class' => 'form-check-label']) !!}
    </div>
</div>

<div class="form-group col-sm-6 col-md-6 col-lg-3">
    <div class="form-check">
        {!! Form::hidden('is_travel_article', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('is_travel_article', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('is_travel_article', __('is Travel Article'), ['class' => 'form-check-label']) !!}
    </div>
</div>

<div class="form-group col-sm-6 col-md-6 col-lg-3">
    <div class="form-check">
        {!! Form::hidden('is_show_in_home', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('is_show_in_home', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('is_show_in_home', __('Is Show in Home (3 Post)'), ['class' => 'form-check-label']) !!}
    </div>
</div>


<div class="form-group col-sm-6 col-md-6 col-lg-3">
    <div class="form-check">
        {!! Form::hidden('is_in_first_home', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('is_in_first_home', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('is_in_first_home', __('Is show First In  Home'), ['class' => 'form-check-label']) !!}
    </div>
</div>



<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', __('models/posts.fields.image').':') !!}
    <p>(348x413)</p>
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('image', ['class' => 'custom-file-input']) !!}
            {!! Form::label('image', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>

<div class="form-group col-sm-6">
    {!! Form::label('thumbnail', __('Thumbnail in Home').':') !!}
    <p>First Home(557 X 343) Second in Home (311 X 343)</p>
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('thumbnail', ['class' => 'custom-file-input']) !!}
            {!! Form::label('thumbnail ', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('header_image', __('Header Image').':') !!}
    <p>(1366 X 620)</p>
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('header_image', ['class' => 'custom-file-input']) !!}
            {!! Form::label('header_image', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<!-- Meta Field -->
@include('meta_example')

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('meta') !!}
    {!! Form::textarea('meta', isset($mode) && $mode != 'edit' ? config('data.meta_default') : null, ['class' => 'form-control']) !!}
</div>
<div class="clearfix"></div>
