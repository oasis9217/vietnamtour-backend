<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/posts.fields.id').':') !!}
    <p>{{ $post->id }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/posts.fields.name').':') !!}
    <p>{{ $post->name }}</p>
</div>

<!-- Slug Field -->
<div class="col-sm-12">
    {!! Form::label('slug', __('models/posts.fields.slug').':') !!}
    <p>{{ $post->slug }}</p>
</div>

<!-- Desc Field -->
<div class="col-sm-12">
    {!! Form::label('desc', __('models/posts.fields.desc').':') !!}
    <p>{{ $post->desc }}</p>
</div>

<!-- Content Field -->
<div class="col-sm-12">
    {!! Form::label('content', __('models/posts.fields.content').':') !!}
    <p>{!! $post->content !!}</p>
</div>

<!-- Image Field -->
<div class="col-sm-12">
    {!! Form::label('image', __('models/posts.fields.image').':') !!}
    <p><img src="{{$post->image_path }}"></p>
</div>

<div class="col-sm-12">
    {!! Form::label('Thumbnail', __('Thumbnail').':') !!}
    <p><img src="{{  $post->thumbnail_path}}"></p>
</div>
<div class="col-sm-12">
    {!! Form::label('Header Image', __('Header Image').':') !!}
    <p><img src="{{ $post->header_image_path}}"></p>
</div>
