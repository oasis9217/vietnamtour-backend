@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    @lang('models/tourCategories.plural')
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-primary float-right"
                       href="{{ route('tourCategories.create') }}">
                        @lang('crud.add_new')
                    </a>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-body p-0 table-responsive">
                <form id="search_form" action="{{ route('tourCategories.index') }}" method="get">
                    {{-- Setting --}}
                    <div class="row p-3">
                        <div class="col-sm-3">
                            <input type="text" name="name" value="{{ request('name') }}" class="form-control"
                                   placeholder="Tìm kiếm theo tên ...">
                        </div>
                        <div class="col-sm-3 form-group">
                            <button class="btn btn-primary" type="submit">Tìm kiếm</button>
                        </div>
                    </div>
                </form>
                <table class="table table-hover">
                    <tr>
                        <th>Id</th>
                        <th>Tên</th>
                        <th>Slug</th>
                        <th>Parent Name</th>
                        <th>Sắp xếp</th>
                        <th>Action</th>
                    </tr>
                    @foreach($tourCategories as $tourCategory)
                        <tr>
                            <td>{{ $tourCategory->id }}</td>
                            <td>{{ $tourCategory->name }}</td>
                            <td>{{ $tourCategory->slug_custom_name }}</td>
                            <td>{{ optional($tourCategory->parent)->name }}</td>
                            <td>{{ $tourCategory->sort }}</td>

                            <td>
                                <form method="POST" action="{{ route('tourCategories.destroy', $tourCategory->id) }}"
                                      accept-charset="UTF-8">
                                    @csrf
                                    @method('DELETE')
                                    <div class="btn-group">
                                        <a href="{{ route('tourCategories.show', $tourCategory->id) }}"
                                           class="btn btn-default btn-xs">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="{{ route('tourCategories.edit', $tourCategory->id) }}"
                                           class="btn btn-default btn-xs">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        @unless(in_array($tourCategory->id,[1,2]))
                                            <button type="submit" class="btn btn-danger btn-xs"
                                                    onclick="return confirm(&quot;Are you sure?&quot;)"><i
                                                    class="fa fa-trash"></i></button>
                                        @endunless
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
                {!! $tourCategories->appends(Request::all())->links() !!}
                <div class="card-footer clearfix float-right">
                    <div class="float-right">

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


