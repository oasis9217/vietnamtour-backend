<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/tourCategories.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6"></div>
<div class="form-group col-sm-6">
    <label for="post_category_id">Parent</label>
    <select class="form-control selectpicker" name="parent_id" id="parent_id" data-live-search="true">
        <option value=""

        >
          Không chọn</option>
        @foreach($tours as $item)
            <option value="{{ $item->id }}"
                @if(isset($tourCategory))
                    {{ $tourCategory->parent_id == $item->id  ? 'selected' : ''}}
                @endif
            >
                {{ $item->name }}</option>
        @endforeach
    </select>
</div>

<!-- Content Field -->
<div class="form-group col-sm-12">
    {!! Form::label('content', __('models/tours.fields.content').':') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control editor']) !!}
</div>

<!-- Meta Field -->
@include('meta_example')
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('meta') !!}
    {!! Form::textarea('meta', isset($mode) && $mode != 'edit' ? config('data.meta_default') : null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('Title') !!}
    {!! Form::textarea('title', null, ['class' => 'form-control']) !!}
</div>
