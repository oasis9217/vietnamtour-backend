@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                      @lang('models/tourCategories.singular')
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::model($tourCategory, ['route' => ['tourCategories.update', $tourCategory->id], 'method' => 'patch']) !!}

            <div class="card-body">
                <div class="row">
                    @include('tour_categories.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Lưu', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('tourCategories.index') }}" class="btn btn-default">
                    @lang('crud.cancel')
                 </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection

@push('page_scripts')
    <script src="{{ asset('ckeditor5-classic/build/ckeditor.js') }}"></script>
    <script src="{{ asset('ckfinder/ckfinder.js') }}"></script>
    <script src="{{ asset('admin/ckeditor/js/init.js') }}"></script>
@endpush
