<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/tourCategories.fields.id').':') !!}
    <p>{{ $tourCategory->id }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/tourCategories.fields.name').':') !!}
    <p>{{ $tourCategory->name }}</p>
</div>

<!-- Slug Field -->
<div class="col-sm-12">
    {!! Form::label('slug', __('models/tourCategories.fields.slug').':') !!}
    <p>{{ $tourCategory->slug }}</p>
</div>

<!-- Parent Id Field -->
<div class="col-sm-12">
    {!! Form::label('parent_id', __('models/tourCategories.fields.parent_id').':') !!}
    <p>{{ $tourCategory->parent_id }}</p>
</div>

<!-- Sort Field -->
<div class="col-sm-12">
    {!! Form::label('sort', __('models/tourCategories.fields.sort').':') !!}
    <p>{{ $tourCategory->sort }}</p>
</div>

