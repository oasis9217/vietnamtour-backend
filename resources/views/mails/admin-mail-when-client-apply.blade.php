<style>

</style>

<div style="padding: 20px">
    <p>Dear Mr Khiêm,</p>
    @if($tour)
    <b>Customers register for the tour
        : {{$tour->name}}</b>
    @endif

    <h2 style="">Customer information: </h2>
    <h3 style="text-transform: capitalize;background: #aaa; padding: 20px 10px;">personal information
    </h3>
    <table style="margin-bottom: 1rem;
        color: #212529;
        background-color: transparent;">
        <tr style=" ">
            <th style="padding: 0.75rem;
        vertical-align: top;

        text-align: left;">Name:
            </th>
            <td style="padding: 0.75rem;
        vertical-align: top;

        text-align: left;">{{$customerContact->name}}</td>
        </tr>
        <tr>
            <th style="padding: 0.75rem;
        vertical-align: top;

        text-align: left;">Email:
            </th>
            <td style="padding: 0.75rem;
        vertical-align: top;

        text-align: left;">{{$customerContact->email}}</td>
        </tr>
        <tr>
            <th style="padding: 0.75rem;
        vertical-align: top;

        text-align: left;">Phone Number:
            </th>
            <td style="padding: 0.75rem;
        vertical-align: top;

        text-align: left;">{{$customerContact->phone_number}}</td>
        </tr>
        <tr>
            <th style="padding: 0.75rem;
        vertical-align: top;

        text-align: left;">Description:
            </th>
            <td style="padding: 0.75rem;
        vertical-align: top;

        text-align: left;">{{$customerContact->desc}}</td>
        </tr>
        <tr>
            <th style="padding: 0.75rem;
        vertical-align: top;

        text-align: left;">Country of residence:
            </th>
            <td style="padding: 0.75rem;
        vertical-align: top;

        text-align: left;">{{$customerContact->country}}</td>
        </tr>

        <tr>
            <th style="padding: 0.75rem;
        vertical-align: top;

        text-align: left;">Family member:</th>
            <td style="padding: 0.75rem;
        vertical-align: top;

        text-align: left;">
                <span>{{$customerContact->adult }} adult | </span>
                <span>{{$customerContact->child }} child |  </span>
                <span>{{$customerContact->child_baby }} child baby </span>
            </td>
        </tr>
    </table>
    </br>
    <h3 style="text-transform: capitalize;background: #aaa; padding: 20px 10px;">Tour information
    </h3>

    <table>
        @if($tour)
            <tr>
                <th style="padding: 0.75rem;
        vertical-align: top;

        text-align: left;">Tour Name:
                </th>
                <td style="padding: 0.75rem;
        vertical-align: top;

        text-align: left;">{{$tour->name}}</td>
            </tr>
        @endif

        <tr>
            <th style="padding: 0.75rem;
        vertical-align: top;

        text-align: left;">International flights:
            </th>
            <td style="padding: 0.75rem;
        vertical-align: top;

        text-align: left;">{{$customerContact->international_flight == 1? 'Yes':'No' }}</td>
        </tr>
        <tr>
            <th style="padding: 0.75rem;
        vertical-align: top;

        text-align: left;">Departure date:
            </th>
            <td style="padding: 0.75rem;
        vertical-align: top;

        text-align: left;">{{$customerContact->departure_date }}</td>
        </tr>
        <tr>
            <th style="padding: 0.75rem;
        vertical-align: top;

        text-align: left;"> Flexible date:
            </th>
            <td style="padding: 0.75rem;
        vertical-align: top;

        text-align: left;">{{$customerContact->is_flexible_date == 1? 'Yes':'No' }}</td>
        </tr>


        <tr>
            <th style="padding: 0.75rem;
        vertical-align: top;

        text-align: left;">Hotel Request:
            </th>
            <td style="padding: 0.75rem;
        vertical-align: top;

        text-align: left;">
                <p>{{$customerContact->hotel_room_quantity }} Room</p>
                <p>Hotel Class:{{$customerContact->hotel_class }}</p>
            </td>
        </tr>
    </table>



    <p>Thank you!</p>
    <p>--</p>
    <p><b>VietNam Tour Team</b></p>
    <img style="height: 40px; width: 40px;"
         src="{{asset('clients/assets/images/header_logo.png')}}">
    <p>Address:Ha Noi</p>
    <p>Phone: +84 919666568</p>
    <p>Email: duykhiem.vietnamtour@gmail.com
    </p>
</div>
