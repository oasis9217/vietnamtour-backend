const mix = require('laravel-mix');


/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.scss', 'public/css');

mix.sass('public/clients/assets/scss/style.scss', 'public/clients/assets/css/').options({
    outputStyle: 'compressed'
})
    .sass('public/clients/assets/scss/blog.scss', 'public/clients/assets/css/').options({
    outputStyle: 'compressed'
})
    .sass('public/clients/assets/scss/blog-detail.scss', 'public/clients/assets/css/').options({
    outputStyle: 'compressed'
})
    .sass('public/clients/assets/scss/sub-category.scss', 'public/clients/assets/css/').options({
    outputStyle: 'compressed'
})
    .sass('public/clients/assets/scss/tour.scss', 'public/clients/assets/css/').options({
    outputStyle: 'compressed'
})
    .sass('public/clients/assets/scss/tour-detail.scss', 'public/clients/assets/css/').options({
    outputStyle: 'compressed'
})
    .sass('public/clients/assets/scss/booking.scss', 'public/clients/assets/css/').options({
    outputStyle: 'compressed'
})
    .sass('public/clients/assets/scss/contact_us.scss', 'public/clients/assets/css/').options({
    outputStyle: 'compressed'
})
    .sass('public/clients/assets/scss/custom_tour.scss', 'public/clients/assets/css/').options({
    outputStyle: 'compressed'
})

;
