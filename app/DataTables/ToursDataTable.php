<?php

namespace App\DataTables;

use App\Models\Tours;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class ToursDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'tours.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Tours $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Tours $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('models/tours.fields.id'), 'data' => 'id','searchable' => false]),
            'name' => new Column(['title' => __('models/tours.fields.name'), 'data' => 'name','searchable' => false]),
            'slug' => new Column(['title' => __('models/tours.fields.slug'), 'data' => 'slug','searchable' => false]),
            'desc' => new Column(['title' => __('models/tours.fields.desc'), 'data' => 'desc','searchable' => false]),
            'image' => new Column(['title' => __('models/tours.fields.image'), 'data' => 'image','searchable' => false]),
            'price' => new Column(['title' => __('models/tours.fields.price'), 'data' => 'price','searchable' => false]),
            'content' => new Column(['title' => __('models/tours.fields.content'), 'data' => 'content','searchable' => false]),
            'is_public' => new Column(['title' => __('models/tours.fields.is_public'), 'data' => 'is_public','searchable' => false]),
            'meta' => new Column(['title' => __('models/tours.fields.meta'), 'data' => 'meta','searchable' => false]),
            'day' => new Column(['title' => __('models/tours.fields.day'), 'data' => 'day','searchable' => false]),
            'keyword' => new Column(['title' => __('models/tours.fields.keyword'), 'data' => 'keyword','searchable' => false])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'tours_datatable_' . time();
    }
}
