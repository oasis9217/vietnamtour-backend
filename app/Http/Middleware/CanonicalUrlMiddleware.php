<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CanonicalUrlMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse) $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->isMethod('get') && $request->route()) {
            $url = $request->url();
            $url2 = url()->current();
            $slug = $request?->slug;
            if ((strpos($url, '/public/') || strpos($url, '/public')) !== false) {
                $url2 = rtrim(str_replace('/public', '', $url), '/');
            }

            if (strpos($request->header('host'), 'www.') === 0) {
                $url2 = rtrim(str_replace('www.', '', $url), '/');
            }
            if (!$request->secure() && env('APP_ENV') !== 'local') {
                $url2 = rtrim(str_replace('http://', 'https://', $url), '/');
            }

            if($slug && strpos($url, '.html'))
            {
                $url2 = rtrim(str_replace('.html', '', $url), '');
            }
            $response = $next($request);
            $response->header('Link', '<' . $url2 . '>; rel="canonical"');
            return $response;
        }
        return $next($request);
    }
}
