<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ConvertUrlMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse) $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (env('APP_ENV') !== 'local') {
            $url = $request->url();
            $slug = $request?->slug;
            if ((strpos($url, '/public/') || strpos($url, '/public')) !== false) {
                $url2 = rtrim(str_replace('/public', '', $url), '/');
                $request->server->set('REQUEST_URI', $url2);
                return redirect($url2);

            }

            if (strpos($request->header('host'), 'www.') === 0) {
                $url2 = rtrim(str_replace('www.', '', $url), '/');
                $request->server->set('REQUEST_URI', $url2);
                return redirect($url2);
            }
            if ($slug && strpos($url, '.html')) {
                $url2 = rtrim(str_replace('.html', '', $url), '');
                $request->server->set('REQUEST_URI', $url2);
                return redirect($url2);
            }

            if (!$request->secure() && env('APP_ENV') !== 'local') {
                $url2 = rtrim(str_replace('http://', 'https://', $url), '/');
                $request->server->set('REQUEST_URI', $url2);
                return redirect($url2);
            }
            if (!$request->secure() && !$request->getRequestUri() && env('APP_ENV') !== 'local') {
                return redirect()->secure('/');
            }
        }
        return $next($request);
    }
}
