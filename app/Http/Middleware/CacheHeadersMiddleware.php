<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CacheHeadersMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        $response->header('Cache-Control', 'public, max-age=2592000'); // Thiết lập tiêu đề Cache-Control
        $response->header('Expires', gmdate('D, d M Y H:i:s', time() + 2592000) . ' GMT'); // Thiết lập tiêu đề Expires

        return $response;
    }
}
