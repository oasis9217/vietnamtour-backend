<?php

namespace App\Http\Controllers;

use App\DataTables\SlugDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSlugRequest;
use App\Http\Requests\UpdateSlugRequest;
use App\Repositories\SlugRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;

class SlugController extends AppBaseController
{
    /** @var SlugRepository $slugRepository*/
    private $slugRepository;

    public function __construct(SlugRepository $slugRepo)
    {
        $this->slugRepository = $slugRepo;
    }

    /**
     * Display a listing of the Slug.
     *
     * @param SlugDataTable $slugDataTable
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $slugs = $this->slugRepository->search($request->all());
        return view('slugs.index', compact('slugs'));
    }

    /**
     * Show the form for creating a new Slug.
     *
     * @return Response
     */
    public function create()
    {
        return view('slugs.create');
    }

    /**
     * Store a newly created Slug in storage.
     *
     * @param CreateSlugRequest $request
     *
     * @return Response
     */
    public function store(CreateSlugRequest $request)
    {
        $input = $request->all();

        $slug = $this->slugRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/slugs.singular')]));

        return redirect(route('slugs.index'));
    }

    /**
     * Display the specified Slug.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $slug = $this->slugRepository->find($id);

        if (empty($slug)) {
            Flash::error(__('messages.not_found', ['model' => __('models/slugs.singular')]));

            return redirect(route('slugs.index'));
        }

        return view('slugs.show')->with('slug', $slug);
    }

    /**
     * Show the form for editing the specified Slug.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $slug = $this->slugRepository->find($id);

        if (empty($slug)) {
            Flash::error(__('messages.not_found', ['model' => __('models/slugs.singular')]));

            return redirect(route('slugs.index'));
        }

        return view('slugs.edit')->with('slug', $slug);
    }

    /**
     * Update the specified Slug in storage.
     *
     * @param int $id
     * @param UpdateSlugRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSlugRequest $request)
    {
        $slug = $this->slugRepository->find($id);

        if (empty($slug)) {
            Flash::error(__('messages.not_found', ['model' => __('models/slugs.singular')]));

            return redirect(route('slugs.index'));
        }

        $slug = $this->slugRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/slugs.singular')]));

        return redirect(route('slugs.index'));
    }

    /**
     * Remove the specified Slug from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $slug = $this->slugRepository->find($id);

        if (empty($slug)) {
            Flash::error(__('messages.not_found', ['model' => __('models/slugs.singular')]));

            return redirect(route('slugs.index'));
        }

        $this->slugRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/slugs.singular')]));

        return redirect(route('slugs.index'));
    }
}
