<?php

namespace App\Http\Controllers;

use App\DataTables\HomeSlideDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateHomeSlideRequest;
use App\Http\Requests\UpdateHomeSlideRequest;
use App\Repositories\HomeSlideRepository;
use App\Services\HomeSlideService;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;

class HomeSlideController extends AppBaseController
{
    /** @var HomeSlideRepository $homeSlideRepository*/
    private $homeSlideRepository;
    protected HomeSlideService $homeSlideService;
    public function __construct(HomeSlideRepository $homeSlideRepo, HomeSlideService $homeSlideService)
    {
        $this->homeSlideRepository = $homeSlideRepo;
        $this->homeSlideService = $homeSlideService;
    }


    public function index(Request $request)
    {
        $homeSlides = $this->homeSlideService->search($request);
        return view('home_slides.index',compact('homeSlides'));
    }

    /**
     * Show the form for creating a new HomeSlide.
     *
     * @return Response
     */
    public function create()
    {
        return view('home_slides.create');
    }

    /**
     * Store a newly created HomeSlide in storage.
     *
     * @param CreateHomeSlideRequest $request
     *
     * @return Response
     */
    public function store(CreateHomeSlideRequest $request)
    {
        $homeSlide = $this->homeSlideService->create($request);

        Flash::success(__('messages.saved', ['model' => __('models/homeSlides.singular')]));

        return redirect(route('homeSlides.index'));
    }

    /**
     * Display the specified HomeSlide.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $homeSlide = $this->homeSlideRepository->find($id);

        if (empty($homeSlide)) {
            Flash::error(__('messages.not_found', ['model' => __('models/homeSlides.singular')]));

            return redirect(route('homeSlides.index'));
        }

        return view('home_slides.show')->with('homeSlide', $homeSlide);
    }

    /**
     * Show the form for editing the specified HomeSlide.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $homeSlide = $this->homeSlideRepository->find($id);

        if (empty($homeSlide)) {
            Flash::error(__('messages.not_found', ['model' => __('models/homeSlides.singular')]));

            return redirect(route('homeSlides.index'));
        }

        return view('home_slides.edit')->with('homeSlide', $homeSlide);
    }

    /**
     * Update the specified HomeSlide in storage.
     *
     * @param int $id
     * @param UpdateHomeSlideRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHomeSlideRequest $request)
    {
        $homeSlide = $this->homeSlideRepository->find($id);

        if (empty($homeSlide)) {
            Flash::error(__('messages.not_found', ['model' => __('models/homeSlides.singular')]));

            return redirect(route('homeSlides.index'));
        }

        $homeSlide = $this->homeSlideService->update($request,$id);

        Flash::success(__('messages.updated', ['model' => __('models/homeSlides.singular')]));

        return redirect(route('homeSlides.index'));
    }

    /**
     * Remove the specified HomeSlide from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $homeSlide = $this->homeSlideRepository->find($id);

        if (empty($homeSlide)) {
            Flash::error(__('messages.not_found', ['model' => __('models/homeSlides.singular')]));

            return redirect(route('homeSlides.index'));
        }

        $this->homeSlideService->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/homeSlides.singular')]));

        return redirect(route('homeSlides.index'));
    }
}
