<?php

namespace App\Http\Controllers;

use App\DataTables\PostCategoryDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePostCategoryRequest;
use App\Http\Requests\UpdatePostCategoryRequest;
use App\Repositories\PostCategoryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;

class PostCategoryController extends AppBaseController
{
    /** @var  PostCategoryRepository */
    private $postCategoryRepository;

    public function __construct(PostCategoryRepository $postCategoryRepo)
    {
        $this->postCategoryRepository = $postCategoryRepo;
    }


    public function index(Request $request)
    {
        $postCategories = $this->postCategoryRepository->search($request->all());
        return view('post_categories.index',compact('postCategories'));
    }

    /**
     * Show the form for creating a new PostCategory.
     *
     * @return Response
     */
    public function create()
    {
        return view('post_categories.create');
    }

    /**
     * Store a newly created PostCategory in storage.
     *
     * @param CreatePostCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreatePostCategoryRequest $request)
    {
        $input = $request->all();

        $postCategory = $this->postCategoryRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/postCategories.singular')]));

        return redirect(route('postCategories.index'));
    }

    /**
     * Display the specified PostCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $postCategory = $this->postCategoryRepository->find($id);

        if (empty($postCategory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/postCategories.singular')]));

            return redirect(route('postCategories.index'));
        }

        return view('post_categories.show')->with('postCategory', $postCategory);
    }

    /**
     * Show the form for editing the specified PostCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $postCategory = $this->postCategoryRepository->find($id);

        if (empty($postCategory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/postCategories.singular')]));

            return redirect(route('postCategories.index'));
        }

        return view('post_categories.edit')->with('postCategory', $postCategory);
    }

    /**
     * Update the specified PostCategory in storage.
     *
     * @param  int              $id
     * @param UpdatePostCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePostCategoryRequest $request)
    {
        $postCategory = $this->postCategoryRepository->find($id);

        if (empty($postCategory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/postCategories.singular')]));

            return redirect(route('postCategories.index'));
        }

        $postCategory = $this->postCategoryRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/postCategories.singular')]));

        return redirect(route('postCategories.index'));
    }

    /**
     * Remove the specified PostCategory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $postCategory = $this->postCategoryRepository->find($id);

        if (empty($postCategory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/postCategories.singular')]));

            return redirect(route('postCategories.index'));
        }

        $this->postCategoryRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/postCategories.singular')]));

        return redirect(route('postCategories.index'));
    }
}
