<?php

namespace App\Http\Controllers;

use App\DataTables\CustomerContactDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCustomerContactRequest;
use App\Http\Requests\UpdateCustomerContactRequest;
use App\Repositories\CustomerContactRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class CustomerContactController extends AppBaseController
{
    /** @var CustomerContactRepository $customerContactRepository*/
    private $customerContactRepository;

    public function __construct(CustomerContactRepository $customerContactRepo)
    {
        $this->customerContactRepository = $customerContactRepo;
    }

    /**
     * Display a listing of the CustomerContact.
     *
     * @param CustomerContactDataTable $customerContactDataTable
     *
     * @return Response
     */
    public function index(CustomerContactDataTable $customerContactDataTable)
    {
        return $customerContactDataTable->render('customer_contacts.index');
    }

    /**
     * Show the form for creating a new CustomerContact.
     *
     * @return Response
     */
    public function create()
    {
        return view('customer_contacts.create');
    }

    /**
     * Store a newly created CustomerContact in storage.
     *
     * @param CreateCustomerContactRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerContactRequest $request)
    {
        $input = $request->all();

        $customerContact = $this->customerContactRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/customerContacts.singular')]));

        return redirect(route('customerContacts.index'));
    }

    /**
     * Display the specified CustomerContact.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $customerContact = $this->customerContactRepository->find($id);

        if (empty($customerContact)) {
            Flash::error(__('messages.not_found', ['model' => __('models/customerContacts.singular')]));

            return redirect(route('customerContacts.index'));
        }

        return view('customer_contacts.show')->with('customerContact', $customerContact);
    }

    /**
     * Show the form for editing the specified CustomerContact.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customerContact = $this->customerContactRepository->find($id);

        if (empty($customerContact)) {
            Flash::error(__('messages.not_found', ['model' => __('models/customerContacts.singular')]));

            return redirect(route('customerContacts.index'));
        }

        return view('customer_contacts.edit')->with('customerContact', $customerContact);
    }

    /**
     * Update the specified CustomerContact in storage.
     *
     * @param int $id
     * @param UpdateCustomerContactRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerContactRequest $request)
    {
        $customerContact = $this->customerContactRepository->find($id);

        if (empty($customerContact)) {
            Flash::error(__('messages.not_found', ['model' => __('models/customerContacts.singular')]));

            return redirect(route('customerContacts.index'));
        }

        $customerContact = $this->customerContactRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/customerContacts.singular')]));

        return redirect(route('customerContacts.index'));
    }

    /**
     * Remove the specified CustomerContact from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $customerContact = $this->customerContactRepository->find($id);

        if (empty($customerContact)) {
            Flash::error(__('messages.not_found', ['model' => __('models/customerContacts.singular')]));

            return redirect(route('customerContacts.index'));
        }

        $this->customerContactRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/customerContacts.singular')]));

        return redirect(route('customerContacts.index'));
    }
}
