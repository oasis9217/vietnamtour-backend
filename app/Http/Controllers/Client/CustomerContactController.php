<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCustomerContactRequest;
use App\Jobs\SentEmailToAdminWhenClientApplyFormJob;
use App\Jobs\SentEmailToClientWhenApplyFormJob;
use App\Repositories\CustomerContactRepository;
use Illuminate\Http\Request;
use Flash;


class CustomerContactController extends Controller
{
    protected CustomerContactRepository $customerContactRepository;

    /**
     * @param CustomerContactRepository $customerContactRepository
     */
    public function __construct(CustomerContactRepository $customerContactRepository)
    {
        $this->customerContactRepository = $customerContactRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCustomerContactRequest $request)
    {
        $input = $request->all();

        $customerContact = $this->customerContactRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/customerContacts.singular')]));
        $this->dispatch(new SentEmailToAdminWhenClientApplyFormJob($customerContact));
        $this->dispatch(new SentEmailToClientWhenApplyFormJob($customerContact));

        return redirect(route('client.home'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
