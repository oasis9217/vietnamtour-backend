<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCustomerContactWhenBookTour;
use App\Jobs\SentEmailToAdminWhenClientApplyFormJob;
use App\Jobs\SentEmailToClientWhenApplyFormJob;
use App\Models\CustomerContact;
use App\Services\CustomerContactService;
use App\Services\SlugService;
use App\Services\TourService;
use App\Traits\CapchaCustom;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class TourController extends Controller
{

    use CapchaCustom;
    protected TourService $tourService;
    protected CustomerContactService $customerContactService;
    protected SlugService $slugService;

    /**
     * @param TourService $tourService
     * @param CustomerContactService $customerContactService
     * @param SlugService $slugService
     */
    public function __construct(
        TourService $tourService,
        CustomerContactService $customerContactService,
        SlugService $slugService
    ) {
        $this->tourService = $tourService;
        $this->customerContactService = $customerContactService;
        $this->slugService = $slugService;
    }


    public function index()
    {
        return  view('client.tours.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show($slug)
    {
        $tour = $this->tourService->findBySlug($slug);
        $toursSimilar =  $this->tourService->getToursRandomShowInClient();
        $toursHighLight =  $this->tourService->getToursHighLight();
        return  view('client.tours.show', compact('tour','toursSimilar','toursHighLight'));
    }

    public function bookTourForm($slug)
    {

        return view('client.tours.booking');
    }

    public function bookTour(CreateCustomerContactWhenBookTour $request)
    {

        if( $this->verifyCapcha($request))
        {
            $cust = $this->customerContactService->store($request);
            $this->dispatch(new  SentEmailToClientWhenApplyFormJob($cust));
            $this->dispatch(new  SentEmailToAdminWhenClientApplyFormJob($cust));
            Flash::success('Successful registration for the tour, thank you.');
            return redirect()->back()->with(['message' => 'create succseess']);
        }else{
            Flash::error('Enter capcha');
            return redirect()->back();
        }

    }

}
