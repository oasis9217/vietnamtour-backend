<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Jobs\SentEmailToAdminWhenClientApplyFormJob;
use App\Repositories\SettingRepository;
use App\Repositories\TourRepository;
use App\Services\HomeSlideService;
use App\Services\PostService;
use App\Services\SettingService;
use App\Services\TourCategoryService;
use App\Services\TourService;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    protected TourService $tourService;
    protected PostService $postService;
    protected TourCategoryService $tourCategoryService;
    protected SettingService $settingService;
    protected HomeSlideService $homeSlideService;

    /**
     * @param TourService $tourService
     * @param PostService $postService
     * @param TourCategoryService $tourCategoryService
     * @param SettingService $settingService
     */
    public function __construct(
        TourService $tourService,
        PostService $postService,
        TourCategoryService $tourCategoryService,
        SettingService $settingService,
        HomeSlideService $homeSlideService
    ) {
        $this->tourService = $tourService;
        $this->postService = $postService;
        $this->tourCategoryService = $tourCategoryService;
        $this->settingService = $settingService;
        $this->homeSlideService = $homeSlideService;
    }


    public function index()
    {
        $tours = $this->tourService->getTourShowInHomeClient();
        $postFirstInHome = $this->postService->getShowFirstInHomeClient();

        $quantity = $postFirstInHome ? 2 : 3;
        $postsShow = $this->postService->getShowInHomeClient(3);
        if($postFirstInHome)
        {
            $posts = $postsShow->where('id','!=',$postFirstInHome->id);
        }
        $touCategoryFirst = $this->tourCategoryService->first();
        $tourCategoryWithTour = $this->tourCategoryService->getTourCategoryByStyleWithTour();
        $multiTourContent = $this->settingService->getMultiTourInHome();
        $homeSlides = $this->homeSlideService->allPublic();
        return view('client.home.index',compact('tours','posts','tourCategoryWithTour', 'multiTourContent','touCategoryFirst','postFirstInHome','homeSlides'));
    }


    public function getCustomizeTour()
    {
        $touCategoryFirst = $this->tourCategoryService->first();
        return view('client.customize-tour.index', compact('touCategoryFirst'));
    }
}
