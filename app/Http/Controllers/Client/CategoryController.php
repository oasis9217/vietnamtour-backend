<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Services\TourCategoryService;
use App\Services\TourService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    protected TourCategoryService $tourCategoryService;
    protected TourService $tourService;

    /**
     * @param TourCategoryService $tourCategoryService
     * @param TourService $tourService
     */
    public function __construct(TourCategoryService $tourCategoryService, TourService $tourService)
    {
        $this->tourCategoryService = $tourCategoryService;
        $this->tourService = $tourService;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show($slug)
    {

        $tourCategory = $this->tourCategoryService->findBySlug($slug);
        $tours = $this->tourService->getTourOfCategory($tourCategory->id);
        return view('client.categories.index',compact('tourCategory','tours'));
    }
}
