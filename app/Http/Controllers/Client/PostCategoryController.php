<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Services\PostCategoryService;
use App\Services\PostService;
use Illuminate\Http\Request;

class PostCategoryController extends Controller
{
    protected PostCategoryService $postCategoryService;
    protected PostService $postService;

    /**
     * @param PostCategoryService $postCategoryService
     * @param PostService $postService
     */
    public function __construct(PostCategoryService $postCategoryService, PostService $postService)
    {
        $this->postCategoryService = $postCategoryService;
        $this->postService = $postService;
    }


    public function show($slug)
    {
        $postsInspiration = $this->postService->getWithPaginate(6);
        $morePosts = $this->postService->getWithPaginate(3);
        return view('client.blogs.index', compact('postsInspiration', 'morePosts'));
    }



}
