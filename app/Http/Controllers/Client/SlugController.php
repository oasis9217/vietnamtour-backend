<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\PostCategory;
use App\Models\TourCategory;
use App\Models\Tours;
use App\Services\PageService;
use App\Services\PostService;
use App\Services\SlugService;
use App\Services\TourCategoryService;
use App\Services\TourService;
use Illuminate\Http\Request;

class SlugController extends Controller
{
    protected SlugService $slugService;
    protected PostService $postService;
    protected TourService $tourService;
    protected TourCategoryService $tourCategoryService;
    protected PageService $pageService;

    /**
     * @param SlugService $slugService
     * @param PostService $postService
     * @param TourService $tourService
     * @param TourCategoryService $tourCategoryService
     * @param PageService $pageService
     */
    public function __construct(
        SlugService $slugService,
        PostService $postService,
        TourService $tourService,
        TourCategoryService $tourCategoryService,
        PageService $pageService
    ) {
        $this->slugService = $slugService;
        $this->postService = $postService;
        $this->tourService = $tourService;
        $this->tourCategoryService = $tourCategoryService;
        $this->pageService = $pageService;
    }


    public function slug($slug)
    {

        $slugArr = explode('-', $slug);
        $isSlugSys = in_array($slugArr[0], config('slug.prefix_custom')) || in_array($slug, config('slug.sys'));
        if ($isSlugSys) {
            return $this->showSlugSystem($slug);

        } else {
            return $this->showSlugCustom($slug);
        }
    }

    public function showSlugSystem($slug)
    {
        $slugArr = explode('-', $slug);
        $isSlugBooking = in_array($slugArr[0], config('slug.prefix_custom'));
        if ($isSlugBooking)
        {
            array_shift($slugArr);
            $slugTour = implode('-',$slugArr);
            $slugObj = $this->slugService->slug($slugTour);
            $tour = $slugObj->slugable;
            if ($tour)
            {
                return view('client.tours.booking',compact('tour'));
            }
            return  redirect(404);
        }else{

            if ($slug == 'contact-us')
            {
                return view('client.page.contact-us');

            }
            $page = $this->pageService->findBySlug($slug);
            return view('client.page.index',compact('page'));
        }
    }

    public function showSlugCustom($slug)
    {

        $slug = $this->slugService->slug($slug);
        $id = $slug->slugable_id;
        switch ($slug->slugable_type) {
            case Tours::class:
                return $this->showTour($id);
            case PostCategory::class:
                return $this->showPostCategory($id);
            case Post::class:
                return $this->showPost($id);
            case TourCategory::class:
                return $this->showTourCategory($id);
            default:
                return redirect(404);
        }
    }

    public function showPost($id)
    {
        $post = $this->postService->findOrFail($id);
        $post->load('categories');
        $categorySlug = $post->categories && $post->categories->count() > 1 ? $post->categories->first()->slug_custom_name : '';

        $postsInspiration = $this->postService->getPostInspirationInDetailPost();
        return view('client.blogs.show', compact('post', 'postsInspiration', 'categorySlug'));
    }

    public function showTour($id)
    {
        $tour = $this->tourService->findOrFail($id);
        $tour->load('categories');
        $toursSimilar = $this->tourService->getToursRandomShowInClient();
        $toursHighLight = $this->tourService->getToursHighLight();
        $tourGuilder = config('data.tour_guilder')[rand(0, 2)];
        return view('client.tours.show', compact('tour', 'toursSimilar', 'toursHighLight','tourGuilder'));
    }

    public function showPostCategory($postCategoryId)
    {
        $page = request()?->page;
        $postCategory = PostCategory::first();
        $productShowInPage = 6;
        if ($page && $page > 1)
        {
            $productShowInPage = 12;
        }

        $postsInspiration = $this->postService->getRandomWithPaginateClient(3, $postCategoryId);
        $morePosts = $this->postService->getWithPaginateInClient($productShowInPage, $postCategoryId);

        return view('client.blogs.index', compact('postsInspiration', 'morePosts','postCategory','page'));
    }

    public function showTourCategory($id)
    {

        $tourCategory = $this->tourCategoryService->findOrFail($id);
        if ($tourCategory->parent_id)
        {
            $tours = $this->tourService->getTourOfCategory($tourCategory->id);
            return view('client.categories.index', compact('tourCategory', 'tours'));
        }else{
            $tourCategory->load('childrens');
            $tourCategoryChilds = collect();
            $names = ['Culture Tours',
                'Vietnam Cruises',
                'Day Tours',
                'Active Travel',
                'Family Holidays',
                'Beach Escapes'];
            if($tourCategory->childrens->count() >0)
            {
                foreach ($names as $name)
                {
                    if($tourCategory->childrens->where('name',$name)){
                        $tourCategoryChilds->push($tourCategory->childrens->where('name',$name)->first());
                    }
                }
            }
            $toursSimilar = $this->tourService->getToursRandomShowInHomeClient(9,$tourCategory->id);
            $toursHighLight = $this->tourService->getToursHighLightInHomeClientBy(9, $tourCategory->id);
            return view('client.tours.index', compact('tourCategory', 'toursSimilar','toursHighLight','tourCategoryChilds'));
        }

    }
}
