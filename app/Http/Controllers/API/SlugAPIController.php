<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSlugAPIRequest;
use App\Http\Requests\API\UpdateSlugAPIRequest;
use App\Models\Slug;
use App\Repositories\SlugRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SlugController
 * @package App\Http\Controllers\API
 */

class SlugAPIController extends AppBaseController
{
    /** @var  SlugRepository */
    private $slugRepository;

    public function __construct(SlugRepository $slugRepo)
    {
        $this->slugRepository = $slugRepo;
    }

    /**
     * Display a listing of the Slug.
     * GET|HEAD /slugs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $slugs = $this->slugRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            $slugs->toArray(),
            __('messages.retrieved', ['model' => __('models/slugs.plural')])
        );
    }

    /**
     * Store a newly created Slug in storage.
     * POST /slugs
     *
     * @param CreateSlugAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSlugAPIRequest $request)
    {
        $input = $request->all();

        $slug = $this->slugRepository->create($input);

        return $this->sendResponse(
            $slug->toArray(),
            __('messages.saved', ['model' => __('models/slugs.singular')])
        );
    }

    /**
     * Display the specified Slug.
     * GET|HEAD /slugs/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Slug $slug */
        $slug = $this->slugRepository->find($id);

        if (empty($slug)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/slugs.singular')])
            );
        }

        return $this->sendResponse(
            $slug->toArray(),
            __('messages.retrieved', ['model' => __('models/slugs.singular')])
        );
    }

    /**
     * Update the specified Slug in storage.
     * PUT/PATCH /slugs/{id}
     *
     * @param int $id
     * @param UpdateSlugAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSlugAPIRequest $request)
    {
        $input = $request->all();

        /** @var Slug $slug */
        $slug = $this->slugRepository->find($id);

        if (empty($slug)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/slugs.singular')])
            );
        }

        $slug = $this->slugRepository->update($input, $id);

        return $this->sendResponse(
            $slug->toArray(),
            __('messages.updated', ['model' => __('models/slugs.singular')])
        );
    }

    /**
     * Remove the specified Slug from storage.
     * DELETE /slugs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Slug $slug */
        $slug = $this->slugRepository->find($id);

        if (empty($slug)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/slugs.singular')])
            );
        }

        $slug->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/slugs.singular')])
        );
    }
}
