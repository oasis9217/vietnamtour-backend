<?php

namespace App\Http\Controllers;

use App\DataTables\ToursDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTourDetailAPIRequest;
use App\Http\Requests\CreateTourDetailRequest;
use App\Http\Requests\CreateToursRequest;
use App\Http\Requests\UpdateToursRequest;
use App\Models\TourCategory;
use App\Repositories\TourCategoryRepository;
use App\Repositories\TourDetailRepository;
use App\Repositories\TourRepository;
use App\Services\TourService;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;

class ToursController extends AppBaseController
{
    /** @var  TourRepository */
    private $toursRepository;
    protected $tourService;
    private $tourDetailRepository;
    protected $tourCategoryRepository;

    public function __construct(
        TourRepository $toursRepo,
        TourService $tourService,
        TourDetailRepository $tourDetailRepo,
        TourCategoryRepository $tourCategoryRepository
    ) {
        $this->toursRepository = $toursRepo;
        $this->tourService = $tourService;
        $this->tourDetailRepository = $tourDetailRepo;
        $this->tourCategoryRepository = $tourCategoryRepository;
    }

    public function index(Request $request)
    {
        $tours = $this->tourService->search($request);
        $categories = $this->tourCategoryRepository->all();
        return view('tours.index', compact('tours', 'categories'));
    }

    public function create()
    {
        $categories = $this->tourCategoryRepository->all();
        return view('tours.create', compact('categories'));
    }

    public function store(CreateToursRequest $request)
    {
        $tours = $this->tourService->create($request);

        Flash::success(__('messages.saved', ['model' => __('models/tours.singular')]));

        return redirect(route('tours.index'));
    }

    public function show($id)
    {
        $tours = $this->toursRepository->find($id);

        if (empty($tours)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tours.singular')]));

            return redirect(route('tours.index'));
        }

        return view('tours.show')->with('tours', $tours);
    }

    public function edit($id)
    {
        $tour = $this->toursRepository->find($id);
        $categories = $this->tourCategoryRepository->all();

        if (empty($tour)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tours.singular')]));

            return redirect(route('tours.index'));
        }

        return view('tours.edit', compact('categories'))->with('tour', $tour);
    }

    public function update($id, UpdateToursRequest $request)
    {
        $tours = $this->toursRepository->find($id);

        if (empty($tours)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tours.singular')]));

            return redirect(route('tours.index'));
        }

        $tours = $this->tourService->update($request, $id);

        Flash::success(__('messages.updated', ['model' => __('models/tours.singular')]));

        return redirect(route('tours.index'));
    }

    /**
     * Remove the specified Tours from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tours = $this->toursRepository->find($id);

        if (empty($tours)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tours.singular')]));
            return redirect(route('tours.index'));
        }

        $this->tourService->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/tours.singular')]));

        return redirect(route('tours.index'));
    }

    public function storeCreateTourDetail(CreateTourDetailAPIRequest $request)
    {
        $input = $request->all();
        $tourDetail = $this->tourDetailRepository->create($input);
        return response()->json([
            'message' =>'Tạo mới thành công ngày tour',
        ]);
    }
}
