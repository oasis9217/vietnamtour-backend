<?php

namespace App\Http\Controllers;

use App\DataTables\PostDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Repositories\PostCategoryRepository;
use App\Repositories\PostRepository;
use App\Services\PostService;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;

class PostController extends AppBaseController
{
    /** @var  PostRepository */
    private $postRepository;
    protected $categoryRepository;
    protected $postService;

    public function __construct(
        PostRepository $postRepo,
        PostCategoryRepository $categoryRepository,
        PostService $postService
    ) {
        $this->postRepository = $postRepo;
        $this->categoryRepository = $categoryRepository;
        $this->postService = $postService;
    }

    /**
     * Display a listing of the Post.
     *
     * @param PostDataTable $postDataTable
     * @return Response
     */
    public function index(Request $request)
    {
        $posts = $this->postService->search($request);
        $categories = $this->categoryRepository->all();
        return view('posts.index', compact('categories', 'posts'));
    }

    /**
     * Show the form for creating a new Post.
     *
     * @return Response
     */
    public function create()
    {
        $categories = $this->categoryRepository->all();
        return view('posts.create', compact('categories'));
    }

    /**
     * Store a newly created Post in storage.
     *
     * @param CreatePostRequest $request
     *
     * @return Response
     */
    public function store(CreatePostRequest $request)
    {
        $this->postService->create($request);

        Flash::success(__('messages.saved', ['model' => __('models/posts.singular')]));

        return redirect(route('posts.index'));
    }

    /**
     * Display the specified Post.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $post = $this->postRepository->find($id);

        if (empty($post)) {
            Flash::error(__('messages.not_found', ['model' => __('models/posts.singular')]));

            return redirect(route('posts.index'));
        }

        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified Post.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $post = $this->postRepository->find($id);
        $categories = $this->categoryRepository->all();

        if (empty($post)) {
            Flash::error(__('messages.not_found', ['model' => __('models/posts.singular')]));

            return redirect(route('posts.index'));
        }

        return view('posts.edit')->with(['post' => $post, 'categories' => $categories]);
    }

    /**
     * Update the specified Post in storage.
     *
     * @param  int              $id
     * @param UpdatePostRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePostRequest $request)
    {
        $post = $this->postRepository->find($id);

        if (empty($post)) {
            Flash::error(__('messages.not_found', ['model' => __('models/posts.singular')]));

            return redirect(route('posts.index'));
        }

        $post = $this->postService->update($request, $id);

        Flash::success(__('messages.updated', ['model' => __('models/posts.singular')]));

        return redirect(route('posts.index'));
    }

    /**
     * Remove the specified Post from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $post = $this->postRepository->find($id);

        if (empty($post)) {
            Flash::error(__('messages.not_found', ['model' => __('models/posts.singular')]));

            return redirect(route('posts.index'));
        }

        $this->postService->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/posts.singular')]));

        return redirect(route('posts.index'));
    }
}
