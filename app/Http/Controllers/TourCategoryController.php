<?php

namespace App\Http\Controllers;

use App\DataTables\TourCategoryDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTourCategoryRequest;
use App\Http\Requests\UpdateTourCategoryRequest;
use App\Repositories\TourCategoryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;

class TourCategoryController extends AppBaseController
{
    /** @var  TourCategoryRepository */
    private $tourCategoryRepository;

    public function __construct(TourCategoryRepository $tourCategoryRepo)
    {
        $this->tourCategoryRepository = $tourCategoryRepo;
    }


    public function index(Request $request)
    {
        $tourCategories = $this->tourCategoryRepository->search($request->all());
        return view('tour_categories.index', compact('tourCategories'));
    }

    /**
     * Show the form for creating a new TourCategory.
     *
     * @return Response
     */
    public function create()
    {
        $tours = $this->tourCategoryRepository->getRootCategory();
        return view('tour_categories.create', compact('tours'));
    }

    /**
     * Store a newly created TourCategory in storage.
     *
     * @param CreateTourCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateTourCategoryRequest $request)
    {
        $input = $request->all();

        $tourCategory = $this->tourCategoryRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/tourCategories.singular')]));

        return redirect(route('tourCategories.index'));
    }

    /**
     * Display the specified TourCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tourCategory = $this->tourCategoryRepository->find($id);

        if (empty($tourCategory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tourCategories.singular')]));

            return redirect(route('tourCategories.index'));
        }

        return view('tour_categories.show')->with('tourCategory', $tourCategory);
    }

    /**
     * Show the form for editing the specified TourCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tourCategory = $this->tourCategoryRepository->find($id);
        $tours = $this->tourCategoryRepository->getRootCategory($id);

        if (empty($tourCategory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tourCategories.singular')]));

            return redirect(route('tourCategories.index'));
        }

        return view('tour_categories.edit', compact('tourCategory', 'tours'));
    }

    /**
     * Update the specified TourCategory in storage.
     *
     * @param  int              $id
     * @param UpdateTourCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTourCategoryRequest $request)
    {
        $tourCategory = $this->tourCategoryRepository->find($id);

        if (empty($tourCategory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tourCategories.singular')]));

            return redirect(route('tourCategories.index'));
        }

        $tourCategory = $this->tourCategoryRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/tourCategories.singular')]));

        return redirect(route('tourCategories.index'));
    }

    /**
     * Remove the specified TourCategory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tourCategory = $this->tourCategoryRepository->find($id);

        if (empty($tourCategory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tourCategories.singular')]));

            return redirect(route('tourCategories.index'));
        }

        $this->tourCategoryRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/tourCategories.singular')]));

        return redirect(route('tourCategories.index'));
    }
}
