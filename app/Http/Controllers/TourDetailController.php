<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTourDetailRequest;
use App\Http\Requests\UpdateTourDetailRequest;
use App\Repositories\TourDetailRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\TourRepository;
use App\Services\TourDetailService;
use Illuminate\Http\Request;
use Flash;
use Response;

class TourDetailController extends AppBaseController
{
    /** @var  TourDetailRepository */
    private $tourDetailRepository;
    protected $tourRepository;
    protected $tourDetailService;

    public function __construct(
        TourDetailRepository $tourDetailRepo,
        TourRepository $tourRepository,
        TourDetailService $tourDetailService
    ) {
        $this->tourDetailRepository = $tourDetailRepo;
        $this->tourRepository = $tourRepository;
        $this->tourDetailService = $tourDetailService;
    }

    public function index(Request $request)
    {
        $tourDetails = $this->tourDetailService->search($request);
        $tours = $this->tourRepository->all();

        return view('tour_details.index', compact('tourDetails', 'tours'));
    }

    public function create()
    {
        $tours = $this->tourRepository->all();
        return view('tour_details.create', compact('tours'));
    }

    /**
     * Store a newly created TourDetail in storage.
     *
     * @param CreateTourDetailRequest $request
     *
     * @return Response
     */
    public function store(CreateTourDetailRequest $request)
    {
        $input = $request->all();

        $tourDetail = $this->tourDetailRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/tourDetails.singular')]));

        return redirect(route('tourDetails.index'));
    }

    /**
     * Display the specified TourDetail.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tourDetail = $this->tourDetailRepository->find($id);

        if (empty($tourDetail)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tourDetails.singular')]));

            return redirect(route('tourDetails.index'));
        }

        return view('tour_details.show')->with('tourDetail', $tourDetail);
    }

    public function edit($id)
    {
        $tourDetail = $this->tourDetailRepository->find($id);
        $tours = $this->tourRepository->all();

        if (empty($tourDetail)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tourDetails.singular')]));

            return redirect(route('tourDetails.index'));
        }

        return view('tour_details.edit', compact('tours'))->with('tourDetail', $tourDetail);
    }

    /**
     * Update the specified TourDetail in storage.
     *
     * @param int $id
     * @param UpdateTourDetailRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTourDetailRequest $request)
    {
        $tourDetail = $this->tourDetailRepository->find($id);

        if (empty($tourDetail)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tourDetails.singular')]));

            return redirect(route('tourDetails.index'));
        }

        $tourDetail = $this->tourDetailRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/tourDetails.singular')]));

        return redirect(route('tourDetails.index'));
    }

    /**
     * Remove the specified TourDetail from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tourDetail = $this->tourDetailRepository->find($id);

        if (empty($tourDetail)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tourDetails.singular')]));

            return redirect(route('tourDetails.index'));
        }

        $this->tourDetailRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/tourDetails.singular')]));

        return redirect(route('tourDetails.index'));
    }
}
