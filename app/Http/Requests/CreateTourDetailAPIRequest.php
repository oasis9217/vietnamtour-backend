<?php

namespace App\Http\Requests;

use App\Traits\HandleValidateFormRequestFail;
use Illuminate\Foundation\Http\FormRequest;

class CreateTourDetailAPIRequest extends FormRequest
{
    use HandleValidateFormRequestFail;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'desc' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên Không được để trống.',
            'desc.required' => 'Nội Dung Ngày Tour Không được để trống.',
        ];
    }
}
