<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCustomerContactWhenBookTour extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'name' => 'required',
            'email' => 'required |email',
            'departure_date' => 'required',
            'country' =>'required'
        ];
    }

    public function messages()
    {
        return [
          'name.required' => 'Name is required',
            'email.required' => 'Email is required',
            'email.email' => 'Email invalidate',
            'departure_date.required' => 'Departure date is required',
            'country.required' => 'Country is required',
        ];
    }
}
