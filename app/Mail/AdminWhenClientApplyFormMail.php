<?php

namespace App\Mail;

use App\Models\Tours;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AdminWhenClientApplyFormMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $customerContact;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customerContact)
    {
        $this->customerContact = $customerContact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $tour = Tours::find($this->customerContact->tour_id);
        $subject = 'VIETNAM TOUR - New customer registered for the tour';
        $subject .= $tour ? ": ".$tour->name : '';
        return $this->view('mails.admin-mail-when-client-apply')
            ->subject($subject)
            ->with(['customerContact' => $this->customerContact,'tour' => $tour]);

    }
}
