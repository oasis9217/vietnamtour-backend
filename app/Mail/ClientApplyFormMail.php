<?php

namespace App\Mail;

use App\Models\Tours;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ClientApplyFormMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $customerContact;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customerContact)
    {
        $this->customerContact = $customerContact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $tour = Tours::find($this->customerContact?->tour_id);
        $subject = 'VIETNAM TOUR - Successfully registered for the tour';
        $subject .= $tour ?": ". $tour->name : '';
        return $this->view('mails.client-apply-form')
            ->subject($subject)
            ->with(['customerContact' => $this->customerContact,'tour' => $tour]);
    }
}
