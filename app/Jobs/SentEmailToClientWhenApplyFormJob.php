<?php

namespace App\Jobs;

use App\Mail\ClientApplyFormMail;
use App\Traits\Emailable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class SentEmailToClientWhenApplyFormJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use Emailable;

    protected $customerContact;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customerContact)
    {
        $this->customerContact = $customerContact;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->getEmail($this->customerContact->email))
            ->send(new ClientApplyFormMail($this->customerContact));

    }
}
