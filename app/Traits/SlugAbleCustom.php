<?php

namespace App\Traits;

use App\Models\Slug;
use Illuminate\Support\Str;

trait SlugAbleCustom
{

    public function slugCustom()
    {
        return $this->morphOne(Slug::class, 'slugable');
    }

    public function createSlug($name = null)
    {
        $slug = Str::slug($name ? $name : $this->name);
        $slugRandom = $slug . rand(1, 10000);
        return !Slug::whereName($slug)->exists() ?
            $this->slugCustom()->create(['name' => $slug])
            : $this->slugCustom()->create(['name' => $slugRandom]);

    }

    public function updateSlug($name ='')
    {
        if ($this->slugCustom)
        {
                $this->slugCustom()->delete();
        }
        return  $this->createSlug($name ? $name : $this->name);

    }

    public function deleteSlug()
    {

        if ($this->slugCustom) {
            $this->slugCustom()->delete();
        }
    }

    public static function boot()
    {

        parent::boot();
        static::deleted(function ($item) {
            $item->deleteSlug();
        });

        static::created(function ($item) {

            $item->createSlug($item?->name ?? $item?->title);
        });

        static::updated(function ($item) {

            $item->updateSlug($item?->name ?? $item?->title);
        });
    }

    public function getSlugCustomNameAttribute()
    {
        return $this?->slugCustom?->name;
    }

    public function getSlugLinkAttribute()
    {
        return route('client.slug',$this?->slugCustom?->name ?? 'not-found');
    }

}
