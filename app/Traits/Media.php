<?php

namespace App\Traits;

use Illuminate\Support\Facades\File;
    use Illuminate\Support\Facades\Storage;
    use Illuminate\Support\Str;
    use Intervention\Image\Facades\Image;
    use League\Flysystem\Exception;

trait Media
{
    protected $width = 500;
    protected $height = 500;
    protected $path = 'upload/';
    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @param int $width
     */
    public function setWidth(int $width): void
    {
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight(int $height): void
    {
        $this->height = $height;
    }

    public function verifyImage($request, $name = 'image')
    {
        return $request->file($name);
    }

    public function checkFolderExist($path)
    {
        return !\File::isDirectory($path) ? \File::makeDirectory($path, 0777, true, true) : '';
    }


    public function uploadFile($file, $path): array
    {
        if ($file) {
            $fileName = time() . $file->getClientOriginalName();
            Storage::disk('public')->put($path . $fileName, File::get($file));
            $file_name = $file->getClientOriginalName();
            $file_type = $file->getClientOriginalExtension();
            $filePath = $fileName;

            return [
                'fileName' => $file_name,
                'fileType' => $file_type,
                'filePath' => $filePath,
                'fileSize' => $this->fileSize($file)
            ];
        }
    }

    public function createFile($request, $path, $name = 'image', $width = null, $height = null)
    {
        $data = [
            'fileName' => '',
            'fileType' => '',
            'filePath' => '',
            'fileSize' => ''
        ];
        $widthIn = $width ? $width : $this->width;
        $heightIn = $width ? $height : $this->height;
        if ($this->verifyImage($request, $name)) {
            $file = $request->file($name);
            $image = Image::make($file);
//            $image = Image::make($file)->resize($widthIn, $heightIn);
            $file_name = $file->getClientOriginalName();
            $file_type = $file->getClientOriginalExtension();
            $baseName = substr($file_name, 0, strlen($file_name) - (strlen($file_type)+1));
            $dir = public_path($path);
            $files = glob($dir .$baseName.'*');
            $fileName = $baseName.'.'.$file_type;

            if(count($files) >0)
            {
                $i = 1;
                while (file_exists($dir . $fileName)) {
                    $fileName = $baseName.'-' . $i . '.' . $file_type;
                    $i++;
                }
            }

            $image->save($path . $fileName);

            $filePath = $fileName;

            $data = [
                'fileName' => $file_name,
                'fileType' => $file_type,
                'filePath' => $filePath,
                'fileSize' => $this->fileSize($file)
            ];
        }
        return $data;
    }

    public function updateImage($request, $path, $current,$name = 'image', $width = null, $height = null)
    {
        if  ($this->verifyImage($request, $name)) {
            $this->deleteFile($path , $current);
            return $this->createFile($request, $path, $name,$width,$height);
        }

        return [
            'fileName' => $current,
            'fileType' => '',
            'filePath' => $current,
            'fileSize' => ''
        ];
    }

    public function deleteFile($path, $image)
    {
        if ($image && file_exists($path.$image)) {
            unlink($path.$image);
        }
    }

    public function fileSize($file, $precision = 2)
    {
        $size = $file->getSize();

        if ($size > 0) {
            $size = (int)$size;
            $base = log($size) / log(1024);
            $suffixes = array(' bytes', ' KB', ' MB', ' GB', ' TB');
            return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
        }

        return $size;
    }

}
