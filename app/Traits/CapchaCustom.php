<?php

namespace App\Traits;

use Illuminate\Support\Facades\Request;

trait CapchaCustom
{
    public function verifyCapcha($request)
    {
        $token = $request->get('token');
        $recaptcha = new \ReCaptcha\ReCaptcha(config('data.capcha_secret'));
        $resp = $recaptcha->setExpectedHostname($request->getHost())
            ->setExpectedAction('register')
            ->verify($token, $request->ip());
       return $resp->isSuccess();
    }
}
