<?php

namespace App\Traits;

trait Emailable
{
    public function getEmail($email)
    {
        if (config('app.env') == 'production') {
            return $email;
        } else {
            return 'haqhuy1999@gmail.com';
        }
    }
}
