<?php

namespace App\Providers;

use App\Composers\Clients\CategoryComposer;
use App\Composers\Clients\FooterSettingComposer;
use App\Composers\Clients\PostCategoryComposer;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ComposeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(
            ['client.layouts.fixed-header'],
            CategoryComposer::class
        );

        View::composer(
            ['client.layouts.fixed-header'],
            PostCategoryComposer::class
        );
        View::composer(
            ['client.layouts.footer'],
            FooterSettingComposer::class
        );
    }
}
