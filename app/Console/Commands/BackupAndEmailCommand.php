<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Spatie\Backup\Helpers\Format;
use Illuminate\Mail\Message;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Storage;

class BackupAndEmailCommand extends Command
{
    protected $signature = 'backup:email';

    protected $description = 'Create a database backup and send it via email';

    public function handle()
    {
        // Run the backup and store the backup file name
        $this->call('backup:run');
        $date = date('Y-m-d');
        $path = "app/VietNamTour/";
        $files = glob(storage_path($path.$date.'*.zip'));

        if (!empty($files)) {
            $backupFile = $files[0];
        } else {
            $backupFile = null;
        }
        if($backupFile)
        {
            Mail::send([], [], function (Message $message) use ($backupFile,$date) {
                $message->to(env('MAIL_USERNAME','huyhq.developer@gmail.com'))
                    ->subject('[VietNamTour] - Database backup-'.$date)
                    ->attach($backupFile, [
                        'as' => 'database_backup_' . $date . '.zip',
                        'mime' => 'application/zip',
		    ]);
		   // ->withSwiftMessage(function ($message) {
                   // $headers = $message->getHeaders();
                 //   $headers->addTextHeader('X-Gmail-Labels', 'Backup');
               // });
            });


            // Delete old backups
            $files = Storage::files('VietNamTour', true);
            $filteredFiles = array_filter($files, function ($file) {
                return Storage::lastModified($file) < strtotime('-10 days');
            });

            Storage::delete($filteredFiles);

            $this->info('Database backup has been created and sent via email.');
        }
    }
}
