<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ClearFilesCommand extends Command
{
    protected $signature = 'files:clear-backup';

    protected $description = 'Clear all files except \'2023-03-04*.zio\' pattern';

    public function handle()
    {
        // Đường dẫn đến thư mục lưu trữ
        $date = date('Y-m-d');
        $path = "VietNamTour";
        $files =  Storage::files($path);

        $zipFiles = array_filter($files, function ($file) use ($date) {
            return !str_contains($file, $date) && pathinfo($file, PATHINFO_EXTENSION) === 'zip'; // Lọc ra những file có đuôi mở rộng là ".zip"
        });
        foreach ($zipFiles as $zipFile) {
            Storage::delete($zipFile);
        }
        // Thông báo khi xóa hoàn tất
        $this->info('All files have been cleared, except '.$date.'*.zip\' pattern.');
    }
}
