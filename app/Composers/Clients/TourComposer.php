<?php
namespace App\Composers\Clients;

use App\Repositories\TourCategoryRepository;
use App\Repositories\TourRepository;
use Illuminate\View\View;

class TourComposer
{

    protected $tourRepository;

    /**
     * Create a new profile composer.
     *
     * @param  \App\Repositories\UserRepository  $users
     * @return void
     */
    public function __construct(TourRepository $tourRepository)
    {
        // Dependencies are automatically resolved by the service container...
        $this->tourRepository = $tourRepository;
    }

    public function getTours()
    {
        return $this->tourRepository->getModel()->all();
    }
    public function compose(View $view)
    {
        $view->with('tours', $this->getTours());
    }
}
