<?php
namespace App\Composers\Clients;

use App\Repositories\SettingRepository;
use App\Repositories\TourCategoryRepository;
use App\Repositories\TourRepository;
use App\Services\HomeSlideService;
use App\Services\PostService;
use App\Services\SettingService;
use App\Services\TourCategoryService;
use App\Services\TourService;
use Illuminate\View\View;

class FooterSettingComposer
{

    protected TourCategoryService $tourCategoryService;

    /**

     * @param TourCategoryService $tourCategoryService
     */
    public function __construct(

        TourCategoryService $tourCategoryService,

    ) {
        $this->tourCategoryService = $tourCategoryService;

    }

    public function getFooter()
    {
       return  $this->tourCategoryService->getTourCategoryByStyleWithTour();

    }
    public function compose(View $view)
    {
        $view->with('categoriesTour', $this->getFooter());
    }
}
