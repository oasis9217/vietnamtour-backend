<?php
namespace App\Composers\Clients;

use App\Repositories\TourCategoryRepository;
use Illuminate\View\View;

class CategoryComposer
{

    protected $tourCategory;

    /**
     * Create a new profile composer.
     *
     * @param  \App\Repositories\UserRepository  $users
     * @return void
     */
    public function __construct(TourCategoryRepository $tourCategory)
    {
        // Dependencies are automatically resolved by the service container...
        $this->tourCategory = $tourCategory;
    }

    public function getTourCategoryInClient()
    {
        return $this->tourCategory->getModel()->with('childrens')->whereIn('id',[1,2])->get();
    }
    public function compose(View $view)
    {
        $view->with('categories', $this->getTourCategoryInClient());
    }
}
