<?php
namespace App\Composers\Clients;

use App\Models\PostCategory;
use App\Repositories\PostCategoryRepository;
use App\Repositories\TourCategoryRepository;
use Illuminate\View\View;

class PostCategoryComposer
{

    protected PostCategoryRepository $postCategoryRepository;

    /**
     * @param PostCategoryRepository $postCategoryRepository
     */
    public function __construct(PostCategoryRepository $postCategoryRepository)
    {
        $this->postCategoryRepository = $postCategoryRepository;
    }


    public function getPostCategoryInClient()
    {
        return $this->postCategoryRepository->getModel()->first();
    }
    public function compose(View $view)
    {
        $view->with('postCategory', $this->getPostCategoryInClient());
    }
}
