<?php

namespace App\Repositories;

use App\Models\Slug;
use App\Repositories\BaseRepository;

/**
 * Class SlugRepository
 * @package App\Repositories
 * @version March 5, 2023, 3:09 pm +07
*/

class SlugRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slugable_id',
        'slugable_type'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Slug::class;
    }

    public function findByName($slug)
    {
        return $this->model->whereName($slug)->firstOrFail();
    }

    public function search(array $data)
    {
        return $this->model->when(isset($data['name']), fn($q) => $q->where('name', 'like', '%' . $data['name'] . '%'))
            ->paginate(10);
    }
}
