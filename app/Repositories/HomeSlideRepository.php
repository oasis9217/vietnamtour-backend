<?php

namespace App\Repositories;

use App\Models\HomeSlide;
use App\Repositories\BaseRepository;

/**
 * Class HomeSlideRepository
 * @package App\Repositories
 * @version April 25, 2023, 2:50 pm +07
*/

class HomeSlideRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'is_public',
        'image',
        'content'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return HomeSlide::class;
    }

    public function search($data)
    {
        return $this->model->paginate(10);
    }

    public function allPublic()
    {
        return $this->model->where('is_public',1)->get(['image','content']);
    }

    public function findOrFail($id)
    {
        return $this->model->findOrFail($id);
    }
}
