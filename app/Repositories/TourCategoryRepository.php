<?php

namespace App\Repositories;

use App\Models\TourCategory;

/**
 * Class TourCategoryRepository
 * @package App\Repositories
 * @version February 17, 2023, 3:05 pm +07
 */
class TourCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug',
        'parent_id',
        'sort'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TourCategory::class;
    }

    public function getRootCategory($id = null)
    {
        return $this->model
            ->when($id, function ($query) use ($id) {
                $query->where('id', '!=', $id);
            })
            ->where('sort', null)
            ->where('parent_id', null)
            ->get();
    }

    public function findBySlug($slug)
    {
        return $this->model->whereSlug($slug)->firstOrFail();
    }

    public function getTourCategoryByStyleWithTour()
    {
        return $this->model->with([
            'tours' => fn($q) => $q->where('is_public', 1)->where('is_show_in_category_home', 1)
        ])->where('name','like','%Vietnam%')
            ->latest('id')
            ->limit(6)
            ->whereParentId(1)->get();
    }

    public function search(array $data)
    {
        return $this->model->when(isset($data['name']), fn($q) => $q->where('name', 'like', '%' . $data['name'] . '%'))
            ->paginate(10);
    }

    public function findOrFail($id)
    {
        return $this->model->findOrFail($id);
    }
}
