<?php

namespace App\Repositories;

use App\Models\Post;

/**
 * Class PostRepository
 * @package App\Repositories
 * @version February 4, 2023, 2:23 pm +07
 */
class PostRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug',
        'desc',
        'content',
        'image'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Post::class;
    }

    public function search($input)
    {
        return $this->model
            ->searchByName($input['name'] ?? null)
            ->searchByCategory($input['post_category_id'] ?? null)
            ->latest()
            ->paginate(10);
    }

    public function findBySlug($slug)
    {
        return $this->model->whereSlug($slug)->firstOrFail();
    }

    public function findOrFail($id)
    {
        return $this->model->findOrFail($id);
    }

    public function getWithPaginate($quantity, $postCategoryId)
    {
        return $this->model->withPostCategoryId($postCategoryId)->latest()->paginate($quantity);
    }

    public function getWithPaginateClient($quantity = 6, $postCategoryId)
    {
        $list = $this->model->withPostCategoryId($postCategoryId)->latest('id')->pluck('id')->toArray();

        $totalItems = count($list);
        $current = isset($_GET['page']) ? $_GET['page'] : 1;
        $perPage = 6;
        $start  = 0;
        if($current > 1) {
            $perPage = 12;
            $start = (($current - 1) * $perPage) - 6;
        }

        $pagedDataIDs = array_slice($list, $start, $perPage);
        $data = collect();

        $totalPages = 1;
        $totalPages += (int)ceil(($totalItems - 6 )/ 12);
        $dataPosts = $this->model->whereIn('id',$pagedDataIDs)->get();

        $data->put('totalPage',$totalPages);
        $data->put('currentPage',$current);
        $data->put('posts',$dataPosts);
        $meta['first'] = $totalPages > 1 ? url()->current().'?page=1': null;
        $meta['last'] = $totalPages > 1 ? url()->current().'?page='.$totalPages : null;
        $links = [];
        for($index = 1; $index<=$totalPages;$index++)
        {
            $links[] = url()->current().'?page='.$index;
        }
        $data->put('links',$links);
        $data->put('meta',$meta);

        return $data;


    }
    public function getRandomWithPaginateClient($quantity, $postCategoryId)
    {
        return $this->model->withPostCategoryId($postCategoryId)->inRandomOrder()->limit($quantity)->get();
    }

    public function getPostInspirationInDetailPost()
    {
        return $this->model->withIsInspiration()->inRandomOrder()->latest()->limit(3)->get();
    }

    public function getShowFirstInHomeClient()
    {
        return $this->model->where('is_in_first_home',1)->latest()->first();
    }
    public function getShowInHomeClient($quantity)
    {
        return $this->model->where('is_show_in_home',1)->with('categories')->latest()->paginate($quantity);
    }
}
