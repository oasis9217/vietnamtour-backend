<?php

namespace App\Repositories;

use App\Models\CustomerContact;
use App\Repositories\BaseRepository;

/**
 * Class CustomerContactRepository
 * @package App\Repositories
 * @version February 28, 2023, 5:57 pm +07
*/

class CustomerContactRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'desc',
        'phone_number',
        'is_paid'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CustomerContact::class;
    }
}
