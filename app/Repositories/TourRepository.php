<?php

namespace App\Repositories;

use App\Models\Tours;
use App\Repositories\BaseRepository;

/**
 * Class ToursRepository
 * @package App\Repositories
 * @version February 19, 2023, 1:46 pm +07
*/

class TourRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug',
        'desc',
        'image',
        'price',
        'content',
        'is_public',
        'meta',
        'day',
        'keyword'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Tours::class;
    }

    public function search($input)
    {
        return $this->model
            ->searchByName($input['name'] ?? null)
            ->searchByCategory($input['tour_category_id'] ?? null)
            ->latest()
            ->paginate(10);
    }

    public function findBySlug($slug)
    {
        return $this->model->with('tourDetails')->withPublic()->whereSlug($slug)->firstOrFail();
    }

    public function findOrFail($id)
    {
        return $this->model->findOrFail($id);
    }

    public function getToursSimilarPaginate($quantity)
    {
        return $this->model->withPublic()->withSimilar()->latest()->paginate($quantity);
    }

    public function getToursHighLight()
    {
        return $this->model->withPublic()->withHighLight()->latest()->get();
    }

    public function getTourOfCategory($categoryId)
    {
        return $this->model->withTourCategoryId($categoryId)->latest()->get();
    }

    public function getTourShowInHomeClient()
    {
        return $this->model->withPublic()->withShowInHome()->latest()->paginate(8);
    }

    public function getToursRandomShowInClient($limit = 3)
    {
        return $this->model->inRandomOrder()->latest()->limit($limit)->get();
    }

    public function getToursHighLightBy($limit = 3)
    {
        return $this->model->withPublic()->withHighLight()->latest()->limit($limit)->get();
    }
    public function getToursHighLightInHomeClientBy($limit = 3,$parentCategoryID = null)
    {
        return $this->model->withPublic()->searchByCategoryParent($parentCategoryID)->withHighLight()->latest()->limit($limit)->get();
    }

    public function getToursRandomShowInHomeClient($limit = 3,$parentCategoryID = null)
    {
        return $this->model->inRandomOrder()->searchByCategoryParent($parentCategoryID)->latest()->limit($limit)->get();
    }
}
