<?php

namespace App\Repositories;

use App\Models\PostCategory;
use App\Repositories\BaseRepository;

/**
 * Class PostCategoryRepository
 * @package App\Repositories
 * @version February 4, 2023, 1:43 pm +07
*/

class PostCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PostCategory::class;
    }

    public function getWidthPaginate($quantity)
    {
        return $this->model->paginate($quantity);
    }

    public function search(array $data)
    {
        return $this->model->when(isset($data['name']), fn($q) => $q->where('name', 'like', '%' . $data['name'] . '%'))
            ->paginate(10);
    }
}
