<?php

namespace App\Repositories;

use App\Models\TourDetail;
use App\Repositories\BaseRepository;

/**
 * Class TourDetailRepository
 * @package App\Repositories
 * @version February 19, 2023, 3:29 pm +07
*/

class TourDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tour_id',
        'name',
        'desc',
        'lat',
        'lpg',
        'content',
        'sort'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TourDetail::class;
    }

    public function search(array $input)
    {
        return $this->model->with('tour')->searchByTour($input['tour_id'] ?? null)
            ->latest('id')
            ->paginate(10);
    }
}
