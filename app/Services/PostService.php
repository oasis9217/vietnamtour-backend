<?php


namespace App\Services;


use App\Repositories\PostRepository;
use App\Traits\Media;
use Illuminate\Http\Request;

class

PostService
{
    use Media;

    protected $postRepo;

    public function __construct(PostRepository $postRepo)
    {
        $this->postRepo = $postRepo;
        $this->path = 'posts/';
    }

    public function create($request)
    {
        $data = $request->except(['image','header_image','thumbnail']);

        $data['image'] = $this->createFile($request,$this->path,'image',348,413)['filePath'];
        $data['thumbnail'] = $this->createFile($request,$this->path,'thumbnail',557,343)['filePath'];
        $data['header_image'] = $this->createFile($request,$this->path,'header_image',1366,620)['filePath'];
        $post = $this->postRepo->create($data);
        $post->categories()->sync($request['post_category_id']);
        return $post;
    }

    public function search(Request $request)
    {
        return $this->postRepo->search($request->all());
    }

    public function update($request, int $id)
    {
        $data = $request->except(['image','header_image','thumbnail']);
        $post = $this->postRepo->find($id);
        $data['image'] = $this->updateImage($request,$this->path,$post->image,'image',348,413)['filePath'];
        $data['thumbnail'] = $this->updateImage($request,$this->path,$post->thumbnail,'thumbnail',557,343)['filePath'];
        $data['header_image'] = $this->updateImage($request,$this->path,$post->header_image,'header_image',1366,620)['filePath'];

        $post->update($data);
        $post->categories()->sync($request['post_category_id']);
        return $post;
    }

    public function all()
    {
        return $this->postRepo->all();
    }

    public function findBySlug($slug)
    {
        return $this->postRepo->findBySlug($slug);
    }

    public function findOrFail($slug)
    {
        return $this->postRepo->findOrFail($slug);
    }

    public function getWithPaginate($quantity, $postCategoryId)
    {
        return $this->postRepo->getWithPaginate($quantity,$postCategoryId);
    }

    public function getWithPaginateInClient($quantity, $postCategoryId)
    {
        return $this->postRepo->getWithPaginateClient($quantity,$postCategoryId);
    }

    public function getRandomWithPaginateClient($quantity, $postCategoryId)
    {
        return $this->postRepo->getRandomWithPaginateClient($quantity,$postCategoryId);
    }

    public function getPostInspirationInDetailPost()
    {
        return $this->postRepo->getPostInspirationInDetailPost();
    }

    public function getShowInHomeClient($quantity)
    {
        return $this->postRepo->getShowInHomeClient($quantity);
    }

    public function getShowFirstInHomeClient()
    {
        return $this->postRepo->getShowFirstInHomeClient();
    }

    public function delete($id)
    {
        $post = $this->postRepo->find($id);
        $post->delete();
        $this->deleteFile($this->path,$post->image);
        $this->deleteFile($this->path,$post->thumbnail);
        $this->deleteFile($this->path,$post->header_image);

        return $post;
    }

}
