<?php


namespace App\Services;


use App\Models\TourCategory;
use App\Repositories\PostRepository;
use App\Repositories\TourCategoryRepository;
use App\Traits\Media;
use Illuminate\Http\Request;

class TourCategoryService
{
    use Media;

    protected TourCategoryRepository $tourCategoryRepository;

    /**
     * @param TourCategoryRepository $tourCategoryRepository
     */
    public function __construct(TourCategoryRepository $tourCategoryRepository)
    {
        $this->tourCategoryRepository = $tourCategoryRepository;
    }

    public function findBySlug($slug)
    {
        return $this->tourCategoryRepository->findBySlug($slug);
    }

    public function getTourCategoryByStyleWithTour()
    {
        return $this->tourCategoryRepository->getTourCategoryByStyleWithTour();
    }

    public function first()
    {
        return $this->tourCategoryRepository->getModel()->first();
    }


    public function findOrFail($slug)
    {
        return $this->tourCategoryRepository->findOrFail($slug);
    }
}
