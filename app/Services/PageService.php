<?php


namespace App\Services;


use App\Repositories\PageRepository;
use App\Repositories\PostRepository;
use App\Traits\Media;
use Illuminate\Http\Request;

class PageService
{
    use Media;

    protected $pageRepo;

    public function __construct(PageRepository $pageRepo)
    {
        $this->pageRepo = $pageRepo;
    }

    public function findBySlug($slug)
    {
        return $this->pageRepo->findBySlug($slug);
    }
}
