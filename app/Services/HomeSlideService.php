<?php


namespace App\Services;


use App\Repositories\HomeSlideRepository;
use App\Repositories\TourRepository;
use App\Traits\Media;
use Illuminate\Http\Request;

class HomeSlideService
{
    use Media;

    protected HomeSlideRepository $homeSlideRepository;

    /**
     * @param HomeSlideRepository $homeSlideRepository
     */
    public function __construct(HomeSlideRepository $homeSlideRepository)
    {
        $this->homeSlideRepository = $homeSlideRepository;
        $this->width= 1366;
        $this->height = 620;
        $this->path = 'home_slides/';
    }


    public function create($request)
    {
        $data = $request->except(['image']);
        $data['image'] =  $this->createFile($request,  $this->path)['filePath'];
        return $this->homeSlideRepository->create($data);
    }

    public function update($request, int $id)
    {
        $data = $request->except(['image','header_image','thumbnail']);
        $homeSlide = $this->homeSlideRepository->find($id);
        $data['image'] =  $this->updateImage($request,  $this->path,$homeSlide->image)['filePath'];

        $homeSlide->update($data);
        return $homeSlide;
    }

    public function search($request)
    {
        $data = $request->all();
        return $this->homeSlideRepository->search($data);
    }

    public function allPublic()
    {
        return $this->homeSlideRepository->allPublic();
    }


    public function findOrFail($id)
    {
        return $this->homeSlideRepository->findOrFail($id);
    }


    public function delete($id)
    {
        $homeSlide = $this->homeSlideRepository->find($id);
        $homeSlide->delete();
        $this->deleteFile($this->path, $homeSlide->image);
        return $homeSlide;
    }
}
