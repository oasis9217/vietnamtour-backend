<?php


namespace App\Services;


use App\Repositories\TourRepository;
use App\Traits\Media;
use Illuminate\Http\Request;

class TourService
{
    use Media;

    protected $tourRepo;

    public function __construct(TourRepository $tourRepo)
    {
        $this->tourRepo = $tourRepo;
        $this->path = 'tours/';
    }

    public function create($request)
    {
        $data = $request->except(['image','header_image','thumbnail']);
        $data['image'] = $this->createFile($request,$this->path,'image',291,394)['filePath'];
        $data['thumbnail'] = $this->createFile($request,$this->path,'thumbnail',316,208)['filePath'];
        $data['header_image'] = $this->createFile($request,$this->path,'header_image',1366,620)['filePath'];
        $data['map_image'] = $this->createFile($request,$this->path,'map_image',1359,472)['filePath'];
        $tour = $this->tourRepo->create($data);
        $tour->categories()->sync($request['tour_category_id']);
        return $tour;
    }

    public function search(Request $request)
    {
        return $this->tourRepo->search($request->all());
    }

    public function update($request, int $id)
    {
        $data = $request->except(['image','header_image','thumbnail']);
        $tour = $this->tourRepo->find($id);
        $data['image'] = $this->updateImage($request,$this->path,$tour->image,'image',291,394)['filePath'];
        $data['thumbnail'] = $this->updateImage($request,$this->path,$tour->thumbnail,'thumbnail',316,208)['filePath'];
        $data['header_image'] = $this->updateImage($request,$this->path,$tour->header_image,'header_image',1366,620)['filePath'];
        $data['map_image'] = $this->updateImage($request,$this->path,$tour->map_image,'map_image',1359,472)['filePath'];
        $tour->update($data);
        $tour->categories()->sync($request['tour_category_id']);
        return $tour;
    }

    public function all()
    {
        return $this->tourRepo->all();
    }

    public function getTourShowInHomeClient()
    {
        return $this->tourRepo->getTourShowInHomeClient();
    }

    public function findBySlug($slug)
    {
        return $this->tourRepo->findBySlug($slug);
    }

    public function getToursSimilarPaginate($quantity)
    {
        return $this->tourRepo->getToursSimilarPaginate($quantity);
    }

    public function getToursHighLight($limit = 3)
    {
        return $this->tourRepo->getToursHighLight($limit);
    }

    public function getToursHighLightBy($limit = 3,$id = null)
    {
        return $this->tourRepo->getToursHighLightBy($limit,$id);
    }

    public function getTourOfCategory($categoryId)
    {
        return $this->tourRepo->getTourOfCategory($categoryId);
    }

    public function getToursRandomShowInClient($limit = 3,$id = null)
    {
        return $this->tourRepo->getToursRandomShowInClient($limit);
    }

    public function getToursRandomShowInHomeClient($limit = 3,$id = null)
    {
        return $this->tourRepo->getToursRandomShowInHomeClient($limit, $id);
    }

    public function getToursHighLightInHomeClientBy($limit = 3,$id = null)
    {
        return $this->tourRepo->getToursHighLightInHomeClientBy($limit,$id);
    }
    public function findOrFail($slug)
    {
        return $this->tourRepo->findOrFail($slug);
    }

    public function delete($id)
    {
        $tour = $this->tourRepo->find($id);
       $tour->delete();
       $this->deleteFile($this->path,$tour->image);
       $this->deleteFile($this->path,$tour->thumbnail);
       $this->deleteFile($this->path,$tour->header_image);
       $this->deleteFile($this->path,$tour->map_image);

        return $tour;
    }

}
