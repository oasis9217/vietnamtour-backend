<?php


namespace App\Services;


use App\Repositories\CustomerContactRepository;
use App\Repositories\PostCategoryRepository;
use App\Repositories\PostRepository;
use App\Traits\Media;
use Illuminate\Http\Request;

class CustomerContactService
{

    protected CustomerContactRepository $customerContactRepository;

    /**
     * @param CustomerContactRepository $customerContactRepository
     */
    public function __construct(CustomerContactRepository $customerContactRepository)
    {
        $this->customerContactRepository = $customerContactRepository;
    }

    public function store($request)
    {
        return $this->customerContactRepository->create($request->all());
    }
}
