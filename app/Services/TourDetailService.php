<?php


namespace App\Services;


use App\Repositories\TourDetailRepository;
use Illuminate\Http\Request;

class TourDetailService
{
    protected $tourDetailRepo;

    public function __construct(TourDetailRepository $tourRepo)
    {
        $this->tourDetailRepo = $tourRepo;
    }

    public function search(Request $request)
    {
        return $this->tourDetailRepo->search($request->all());
    }
}
