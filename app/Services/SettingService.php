<?php


namespace App\Services;


use App\Repositories\PostRepository;
use App\Repositories\SettingRepository;
use App\Traits\Media;
use Illuminate\Http\Request;

class SettingService
{

    protected  SettingRepository$settingRepo;

    /**
     * @param SettingRepository $settingRepo
     */
    public function __construct(SettingRepository $settingRepo)
    {
        $this->settingRepo = $settingRepo;
    }

    public function getMultiTourInHome()
    {
        return $this->settingRepo->getMultiTourInHome();
    }

}
