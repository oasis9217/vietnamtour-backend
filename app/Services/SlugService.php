<?php


namespace App\Services;


use App\Repositories\PostRepository;
use App\Repositories\SlugRepository;
use App\Traits\Media;
use Illuminate\Http\Request;

class SlugService
{
    protected SlugRepository $slugRepository;

    /**
     * @param SlugRepository $slugRepository
     */
    public function __construct(SlugRepository $slugRepository)
    {
        $this->slugRepository = $slugRepository;
    }

    public function slug($slug)
    {
        return $this->slugRepository->findByName($slug);
    }
}
