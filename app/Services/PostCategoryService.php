<?php


namespace App\Services;


use App\Repositories\PostCategoryRepository;
use App\Repositories\PostRepository;
use App\Traits\Media;
use Illuminate\Http\Request;

class PostCategoryService
{
    use Media;

    protected PostCategoryRepository $postCategoryRepository;

    /**
     * @param PostCategoryRepository $postCategoryRepository
     */
    public function __construct(PostCategoryRepository $postCategoryRepository)
    {
        $this->postCategoryRepository = $postCategoryRepository;
    }

    public function all()
    {
        return $this->postCategoryRepository->all();
    }

    public function getWidthPaginate($quantity)
    {
        return $this->postCategoryRepository->getWidthPaginate($quantity);
    }
}
