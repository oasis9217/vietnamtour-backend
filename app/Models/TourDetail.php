<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class TourDetail
 * @package App\Models
 * @version February 19, 2023, 3:29 pm +07
 *
 * @property integer $tour_id
 * @property string $name
 * @property string $desc
 * @property number $lat
 * @property number $lpg
 * @property string $content
 * @property integer $sort
 */
class TourDetail extends Model
{
    public $timestamps = false;

    public $table = 'tour_details';

    protected $dates = ['deleted_at'];



    public $fillable = [
        'tour_id',
        'name',
        'desc',
        'lat',
        'lpg',
        'content',
        'sort'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tour_id' => 'integer',
        'name' => 'string',
        'lat' => 'float',
        'lpg' => 'float',
        'sort' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'tour_id' => 'required',
    ];

    public function tour()
    {
        return $this->belongsTo(Tours::class);
    }

    public function scopeSearchByTour($query, $tourId)
    {
        $tourId ? $query->whereHas('tour', function ($q) use ($tourId) {
            $q->where('id', $tourId);
        }) : null;
    }
}
