<?php

namespace App\Models;

use App\Traits\SlugAbleCustom;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Post
 * @package App\Models
 * @version February 4, 2023, 2:23 pm +07
 *
 * @property string $name
 * @property string $slug
 * @property string $desc
 * @property string $content
 * @property string $image
 */
class Post extends Model
{
    use SoftDeletes;

    use SlugDefault;

    use HasFactory;

    use SlugAbleCustom;

    const IMAGE_PATH = 'posts/';

    const ACTIVE_STATUS = 1;

    public $table = 'posts';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'slug',
        'desc',
        'content',
        'image',
        'is_inspiration',
        'is_travel_article',
        'thumbnail',
        'header_image',
        'is_show_in_home',
        'is_in_first_home',
        'meta'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'desc' => 'string',
        'image' => 'string',
        'meta' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => ['required', 'max:255'],
        'desc' => 'required|string',
        'image' => ['nullable', 'mimetypes:image/png,image/jpeg,image/jpg', 'max:4024'],
        'thumbnail' => ['nullable', 'mimetypes:image/png,image/jpeg,image/jpg', 'max:4024'],
        'header_image' => ['nullable', 'mimetypes:image/png,image/jpeg,image/jpg', 'max:4024'],
        'content' => ['nullable'],
        'meta' => ['nullable'],
    ];

    public function categories()
    {
        return $this->belongsToMany(PostCategory::class, 'post_category', 'post_id', 'post_category_id');
    }

    public function syncCategory($id)
    {
        return $this->categories()->sync($id);
    }

    public function scopeSearchByName($query, $name)
    {
        return $name ? $query->where('name', 'like', '%' . $name . '%') : null;
    }


    public function scopeSearchByCategory($query, $postCategoryId)
    {
        if ($postCategoryId > 0 && $postCategoryId !== null) {
            return $query->whereHas('categories', function ($query) use ($postCategoryId) {
                $query->where('id', $postCategoryId);
            });
        }
    }

    public function getImagePathAttribute()
    {
        return asset(self::IMAGE_PATH . $this?->image);
    }

    public function getThumbnailPathAttribute()
    {
        return $this?->thumbnail ? asset(self::IMAGE_PATH . $this?->thumbnail)
            : asset('clients/assets/images/inspiration/Layer 34.png');
    }

    public function getHeaderImagePathAttribute()
    {
        return $this->header_image ? asset(self::IMAGE_PATH . $this?->header_image)
            : asset('clients/assets/images/header_bg.png');
    }

    public function scopeWithIsInspiration($query)
    {
        return $query->where('is_inspiration', self::ACTIVE_STATUS);
    }

    public function scopeWithIsTravelArticle($query)
    {
        return $query->where('is_travel_article', self::ACTIVE_STATUS);
    }

    public function scopeWithShowInHome($query)
    {
        return $query->where('is_show_in_home', self::ACTIVE_STATUS);
    }

    public function scopeWithFirstInHome($query)
    {
        return $query->where('is_in_first_home', self::ACTIVE_STATUS);
    }

    public function scopeWithPostCategoryId($query, $postCategoryId)
    {
        return $query->whereHas('categories', fn($q) =>$q-> where('post_categories.id', $postCategoryId));
    }

    public function getSlugCategoryLinkAttribute()
    {
        return count($this->categories) > 0 ? route('client.slug', $this->categories->first()->slug_custom_name)
            : route('client.home');
    }
}
