<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Page
 * @package App\Models
 * @version March 5, 2023, 2:15 pm +07
 *
 * @property string $name
 * @property string $title
 * @property string $slug
 * @property string $content
 */
class Page extends Model
{
    use SoftDeletes;

    use SlugDefault;


    public $table = 'pages';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'title',
        'slug',
        'content'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'title' => 'string',
        'slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
