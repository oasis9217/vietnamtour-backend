<?php

namespace App\Models;

use Eloquent as Model;



/**
 * Class ContactUs
 * @package App\Models
 * @version May 10, 2023, 10:39 pm +07
 *
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $country
 */
class ContactUs extends Model
{

    public $table = 'contact_us';

    public $fillable = [
        'name',
        'email',
        'phone',
        'country',
        'gender',
        'city',
        'social_media',
        'subject',
        'special_request'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'country' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required',
        'phone' => 'required',
        'country' => 'required'
    ];


}
