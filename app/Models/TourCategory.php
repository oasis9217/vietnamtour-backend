<?php

namespace App\Models;

use App\Traits\SlugAbleCustom;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class TourCategory
 * @package App\Models
 * @version February 17, 2023, 3:05 pm +07
 *
 * @property string $name
 * @property string $slug
 * @property integer $parent_id
 * @property integer $sort
 */
class TourCategory extends Model
{
    use SoftDeletes;

    use SlugDefault;

    use SlugAbleCustom;
    public $table = 'tour_categories';


    protected $dates = ['deleted_at'];

    public $timestamps = false;

    public $fillable = [
        'name',
        'slug',
        'parent_id',
        'sort',
        'content',
        'meta',
        'title'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'parent_id' => 'integer',
        'sort' => 'integer'
    ];


    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function parent()
    {
        return $this->belongsTo(TourCategory::class, 'parent_id', 'id');
    }

    public function childrens()
    {
        return $this->hasMany(TourCategory::class, 'parent_id', 'id');
    }

    public function tours()
    {
        return $this->belongsToMany(Tours::class,'tour_category','tour_category_id','tour_id');
    }
}
