<?php

namespace App\Models;

use Eloquent as Model;



/**
 * Class Slug
 * @package App\Models
 * @version March 5, 2023, 3:09 pm +07
 *
 * @property string $name
 * @property integer $slugable_id
 * @property string $slugable_type
 */
class Slug extends Model
{


    public $table = 'slugs';




    public $fillable = [
        'name',
        'slugable_id',
        'slugable_type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slugable_id' => 'integer',
        'slugable_type' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function slugable()
    {
        return $this->morphTo();
    }

    public function getTypeStrAttribute()
    {
        switch ($this->slugable_type) {
            case Tours::class:
                return 'Tour';
            case PostCategory::class:
                return 'PostCategory';
            case Post::class:
                return 'Post';
            case TourCategory::class:
                return 'TourCategory';
            default:
                return 'System';
        }
    }

    public function getNameTypeAttribute()
    {
        return $this?->slugable?->name;
    }
}
