<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class CustomerContact
 * @package App\Models
 * @version February 28, 2023, 5:57 pm +07
 *
 * @property string $name
 * @property string $email
 * @property string $desc
 * @property string $phone_number
 * @property boolean $is_paid
 */
class CustomerContact extends Model
{
    use SoftDeletes;


    public $table = 'customer_contacts';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'email',
        'desc',
        'phone_number',
        'is_paid',
        'tour_id',
        'country',
        'international_flight',
        'hotel_room_quantity',
        'hotel_class',
        'adult',
        'child',
        'child_baby',
        'departure_date',
        'is_flexible_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'phone_number' => 'string',
        'is_paid' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'email' => 'required|email',
        'name' => 'required',
        'desc' => 'required',
    ];

}
