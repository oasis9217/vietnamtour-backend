<?php

namespace App\Models;

use App\Traits\SlugAbleCustom;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class PostCategory
 * @package App\Models
 * @version February 4, 2023, 1:43 pm +07
 *
 * @property string $name
 * @property string $slug
 */
class PostCategory extends Model
{
    use SoftDeletes;

    use SlugDefault;
    use SlugAbleCustom;

    public $table = 'post_categories';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'slug',
        'meta',
        'title'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
