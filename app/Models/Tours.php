<?php

namespace App\Models;

use App\Traits\SlugAbleCustom;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Tours
 * @package App\Models
 * @version February 19, 2023, 1:46 pm +07
 *
 * @property string $name
 * @property string $slug
 * @property string $desc
 * @property string $image
 * @property number $price
 * @property string $content
 * @property boolean $is_public
 * @property string $meta
 * @property integer $day
 * @property integer $keyword
 */
class Tours extends Model
{
    use SoftDeletes;

    use SlugDefault;
    use SlugAbleCustom;
    public $table = 'tours';

    const IMAGE_PATH = 'tours/';
    const PUBLIC_STATUS = 1;
    const SIMILAR_STATUS = 1;
    const HIGHLIGHT_STATUS = 1;
    const SHOW_IN_CATEGORY_HOME_STATUS = 1;
    const SHOW_IN_HOME_STATUS = 1;

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'slug',
        'desc',
        'image',
        'price',
        'content',
        'is_public',
        'meta',
        'day',
        'keyword',
        'is_highlight',
        'is_similar',
        'thumbnail',
        'header_image',
        'is_show_in_home',
        'is_show_in_category_home',
        'map_image',
        'itinerary',
        'accommodation',
        'meals',
        'transport',
        'travel_team',
        'experiences',
        'visa_arrangement',
        'international_flights'.
        'travel_insurance',
        'meals_not_included',
        'personal_expenses'
    ];

    public $withCount = ['tourDetails'];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'desc' => 'string',
        'image' => 'string',
        'price' => 'float',
        'is_public' => 'boolean',
        'day' => 'integer',
        'keyword' => 'integer',
        'is_highlight' => 'boolean',
        'is_similar' => 'boolean',
        'thumbnail' => 'string',
        'header_image' => 'string',
        'is_show_in_home' => 'boolean',
        'is_show_in_category_home' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => ['required', 'max:255'],
        'desc' => ['nullable', 'string'],
        'image' => ['nullable', 'mimetypes:image/png,image/jpeg,image/jpg', 'max:5024'],
        'header_image' => ['nullable', 'mimetypes:image/png,image/jpeg,image/jpg', 'max:5024'],
        'map_image' => ['nullable', 'mimetypes:image/png,image/jpeg,image/jpg', 'max:5024'],
        'thumbnail' => ['nullable', 'mimetypes:image/png,image/jpeg,image/jpg', 'max:5024'],
        'content' => ['nullable'],
        'price' => ['nullable', 'integer'],
        'day' => ['nullable', 'integer']
    ];


    public function categories()
    {
        return $this->belongsToMany(TourCategory::class, 'tour_category', 'tour_id', 'tour_category_id');
    }

    public function scopeWithTourCategoryId($query, $categoryId)
    {
        return $categoryId ? $query->whereHas('categories',
            fn($q) => $q->where('tour_category_id', $categoryId)) : null;
    }

    public function scopeSearchByName($query, $name)
    {
        return $name ? $query->where('name', 'like', '%' . $name . '%') : null;
    }


    public function scopeSearchByCategory($query, $postCategoryId)
    {
        if ($postCategoryId > 0 && $postCategoryId !== null) {
            return $query->whereHas('categories', function ($query) use ($postCategoryId) {
                $query->where('id', $postCategoryId);
            });
        }
    }

    public function scopeSearchByCategoryParent($query, $postCategoryId)
    {
        if ($postCategoryId > 0 && $postCategoryId !== null) {
            return $query->whereHas('categories', function ($query) use ($postCategoryId) {
                $query->where('id', $postCategoryId)->orWhere('parent_id', $postCategoryId);
            });
        }
    }

    public function getImagePathAttribute()
    {
        return $this?->image ?  asset(self::IMAGE_PATH . $this?->image)
           : asset('clients/assets/images/tours/bestofvitnam.png');
    }

    public function getThumbnailPathAttribute()
    {
        return $this?->thumbnail ? asset(self::IMAGE_PATH . $this?->thumbnail)
            : asset('clients/assets/images/tour-hightligh/tourhighlight.png');
    }

    public function getHeaderImagePathAttribute()
    {
        return $this->header_image ? asset(self::IMAGE_PATH . $this?->header_image)
            : asset('clients/assets/images/tour-detail/banner.png');
    }

    public function getMapImagePathAttribute()
    {
        return $this->map_image ? asset(self::IMAGE_PATH . $this?->map_image)
            : asset('clients/assets/images/tour-detail/map.png');
    }

    public function tourDetails()
    {
        return $this->hasMany(TourDetail::class, 'tour_id');
    }

    public function scopeWithPublic($query)
    {
        return $query->where('is_public', self::PUBLIC_STATUS);
    }

    public function scopeWithSimilar($query)
    {
        return $query->where('is_similar', self::SIMILAR_STATUS);
    }

    public function scopeWithHighLight($query)
    {
        return $query->where('is_highlight', self::HIGHLIGHT_STATUS);
    }

    public function scopeWithShowInCategoryHome($query)
    {
        return $query->where('is_show_in_category_home', self::SHOW_IN_CATEGORY_HOME_STATUS);
    }

    public function scopeWithShowInHome($query)
    {
        return $query->where('is_show_in_home', self::SHOW_IN_HOME_STATUS);
    }
}
