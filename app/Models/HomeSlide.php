<?php

namespace App\Models;

use Eloquent as Model;



/**
 * Class HomeSlide
 * @package App\Models
 * @version April 25, 2023, 2:50 pm +07
 *
 * @property boolean $is_public
 * @property string $image
 * @property string $content
 */
class HomeSlide extends Model
{


    public $table = 'home_slides';

const IMAGE_PATH = 'home_slides/';


    public $fillable = [
        'is_public',
        'image',
        'content'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'is_public' => 'boolean',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'image' => ['nullable', 'mimetypes:image/png,image/jpeg,image/jpg', 'max:4024'],
    ];

    public function getImagePathAttribute()
    {
        return file_exists(self::IMAGE_PATH . $this?->image)?
            asset(self::IMAGE_PATH . $this?->image)
            :asset('clients/assets/images/header_bg.png');
    }

}
