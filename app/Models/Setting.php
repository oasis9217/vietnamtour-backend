<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Setting
 * @package App\Models
 * @version March 1, 2023, 5:08 pm +07
 *
 * @property string $name
 * @property string $content
 */
class Setting extends Model
{
    use SoftDeletes;


    public $table = 'settings';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'content',
        'name_vi'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'name_vi' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
