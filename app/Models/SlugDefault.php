<?php


namespace App\Models;


use Spatie\Sluggable\SlugOptions;
use Spatie\Sluggable\HasSlug;

trait SlugDefault
{
    use HasSlug;

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }
}
