<?php

use App\Http\Controllers\Client\HomeController;
use App\Mail\AdminWhenClientApplyFormMail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'App\Http\Controllers\LanguageController@switchLang']);
Auth::routes();


Route::controller(\App\Http\Controllers\Client\HomeController::class)->group(function () {
    Route::get('/', 'index')->name('client.home');
    Route::get('/customize-tour', 'getCustomizeTour')->name('client.customize_tour');

});

Route::controller(\App\Http\Controllers\Client\SlugController::class)
    ->group(function () {
        Route::get('/{slug}', 'slug')->name('client.slug');
    });


Route::controller(\App\Http\Controllers\Client\TourController::class)
    ->name('client.tours.')->group(function () {
        Route::get('/bookingtour-{slug}', 'bookTourForm')->name('book_tour_form');
        Route::post('/book-tour/store', 'bookTour')->name('book_tour');
    });

Route::controller(\App\Http\Controllers\Client\CustomerContactController::class)
    ->name('client.customer_contacts.')->group(function () {
        Route::post('customer-contacts-create', 'store')->name('store');

    });

//
Route::post('contactuses-store', [App\Http\Controllers\ContactUsController::class,'store'])->name('contactuses-store');

Route::get('/checkOnline', function (App\Repositories\AttendanceRepository $attendanceRepo) {
    if (Auth::check()) {
    }
    return $attendanceRepo->CountUserOnline();
})->name('checkOnline');

