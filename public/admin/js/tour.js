$(function(){
    let tourId ;
        let tourName;
        let messageCreate = $('#messageCreateWrap');
   $(document).on('click','.add-tour-detail', function (){
       $('#createTourDetailModal').modal('show')
       let form = $('#createFormTourDetail');
       form.trigger('reset')
        tourId = $(this).data('id');
        tourName = $(this).data('name');
       $('#createTourName').html(tourName)
       clearError()
       messageCreate.hide();
   })
    $(document).on('click','.btn-create-tour-detail', function (){
       let form = $('#createFormTourDetail');
       $('#createTourName').html(tourName)
       $('#tourCreateId').val(tourId)
       let url = form.attr('action');
       let data = form.serialize();
        $('#message-create').html('')
       clearError()
        ajax(url,data,'post')
            .then(res=>{
                messageCreate.show();
                $('#message-create').html(res.message)
                form.trigger('reset')
            }).catch(res=>{
            let errors = res?.responseJSON?.errors;
            messageCreate.hide();
            renderError(errors)
        })
   })
});

function renderError(errors)
{
    if (errors)
    {
        for(let key in errors)
        {
            $(`.error-${key}`).html(errors[key]?.[0]);
        }
    }
}

function clearError()
{

        $(`.errors`).html('');
}
function ajax(url,data={},method='get')
{
    return $.ajax({
        url,
        data,
        method
    })
}
