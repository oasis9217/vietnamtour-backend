
function setLineOfMap(map,markers){

    const flightPlanCoordinates = markers.map(item=>{
        return {
            lat:item[1],
            lng:item[2]
        }
    })
    const flightPath = new google.maps.Polyline({
        path: flightPlanCoordinates,
        geodesic: true,
        strokeColor: "#FF0000",
        strokeOpacity: 1.0,
        strokeWeight: 2,
    });

    flightPath.setMap(map);
}
function initMap() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap',
        center: { lat: 14.0583, lng: 108.2772 }, // Coordinates for Vietnam (Ho Chi Minh City)
        zoom: 8
    };

    // Display a map on the web page
    map = new google.maps.Map(document.getElementById("map-info"), mapOptions);
    map.setTilt(50);

    // Multiple markers location, latitude, and longitude
    if(tourDetails.length ==0)
    {
        tourDetails = [{
            name:'Ha Noi',
            lat :  21.0205755122339,
            lpg : 105.83601941504449,
        }]
    }
    var markers = tourDetails.map(item=>{
        return [
            item.name,
            item.lat ??  21.0205755122339,
            item.lpg ?? 105.83601941504449,
        ]
    })

    // Info window content

    var infoWindowContent = tourDetails.map(item=>{
        return [
           ` <div class="info_content">
            <h4>${item.name}</h4>
            ${item.content ??''}
            </div>`
        ]
    })
    // Add multiple markers to map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    // Place each marker on the map
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            label: `${i+1}`,
            title: markers[i][0]
        });

        // Add info window to marker
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // google.maps.event.addDomListener(document.querySelector(`.btn-${i}`),'click', (function(marker, i) {
        //     setLineOfMap(map)
        //     return function() {
        //         infoWindow.setContent(infoWindowContent[i][0]);
        //         infoWindow.open(map, marker);
        //     }
        // })(marker, i));
        // Center the map to fit all markers on the screen
        map.fitBounds(bounds);
    }
    setLineOfMap(map, markers)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(5);
        google.maps.event.removeListener(boundsListener);
    });
}
initMap()
window.initMap = initMap;
