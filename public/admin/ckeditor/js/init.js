const InitCkeditor = (function () {
    let modules = {};
    let sizeOption = [];

    for (let i = 1; i <= 100; i++) {
        sizeOption.push(i);
        if (i==17)
        {
            sizeOption.push('default')
        }
    }
    modules.createInlineEditor = function  (selector) {
        return new Promise(((resolve, reject) => {
            ClassicEditor.create(document.querySelector(selector), {
                fontFamily:{
                    // supportAllValues: true,
                    options: [
                        'default',
                        "Montserrat-Regular",
                        "Montserrat-Bold",
                        "Montserrat-SemiBold",
                        "Montserrat-Medium",
                        "Dancing Script",

                        'Arial, Helvetica, sans-serif',
                        'Courier New, Courier, monospace',
                        'Georgia, serif',
                        'Lucida Sans Unicode, Lucida Grande, sans-serif',
                        'Tahoma, Geneva, sans-serif',
                        'Times New Roman, Times, serif',
                        'Trebuchet MS, Helvetica, sans-serif',
                        'Verdana, Geneva, sans-serif'
                    ]
                },
                fontSize: {
                    options: [
                        9,
                        11,
                        13,
                        'default',
                        17,
                        19,
                        21,
                        24,
                        30,


                    ]
                },
                image: {
                    toolbar: [
                        'imageTextAlternative',
                        'imageStyle:full',
                        'imageStyle:side'
                    ]
                },
                heading: {
                    options: [
                        { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                        { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                        { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
                        { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
                        { model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' },
                        { model: 'heading5', view: 'h5', title: 'Heading 5', class: 'ck-heading_heading5' }
                    ]
                },
                ckfinder: {
                    openerMethod: 'popup',
                    uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
                    options: {
                        resourceType: 'Images',
                    }
                }
            }).then(data => resolve(data)).catch(error => reject(error))
                .catch( function( error ) {
                    console.error( error );
                } );

        }))
    };
    modules.init = async function (selector = '.editor') {
       await modules.createInlineEditor(selector)
    }
    return modules;
}(window.jQuery, window, document));
InitCkeditor.init();


$(document).on('change','#tourDetailLat', function () {
    let location =  $(this).val() ? $(this).val().split('-') : '';
    if (location)
    {
        $('.lat').val(location[0])
        $('.lpg').val(location[1])
    }
})

