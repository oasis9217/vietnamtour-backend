(function (slide) {
  slide(window.jQuery, window, document);
})(function ($, window, document) {
  $(function () {
    $("#carouselTourRecoment .carousel-control-prev").click(function () {
      $("#carouselTourRecoment").carousel("prev");
    });

    $("#carouselTourRecoment .carousel-control-next").click(function () {
      $("#carouselTourRecoment").carousel("next");
    });

    $("#carouselTravelChoise .carousel-control-prev").click(function () {
        $("#carouselTravelChoise").carousel("prev");
      });
  
      $("#carouselTravelChoise .carousel-control-next").click(function () {
        $("#carouselTravelChoise").carousel("next");
      });
  });

});

