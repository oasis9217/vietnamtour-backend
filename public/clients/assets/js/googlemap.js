
const markers = [
    ['Hà Nội', 21.0205755122339, 105.83601941504449],
    ['Huế', 16.472813136755555, 107.5625213780196],
    ['Đà Nẵng', 16.127839294572674, 108.00189902848238],
    ['Cần Thơ', 10.200936286007142, 105.40688534827954]
]; 


const flightPlanCoordinates = markers.map(item=>{
    return {
        lat: item[1], lng: item[2]
    };
})

function setLineOfMap(map){
    const flightPlanCoordinates = [
        { lat: 21.0205755122339, lng: 105.83601941504449 },
        { lat: 16.472813136755555, lng: 107.5625213780196 },
        { lat: 16.127839294572674, lng: 108.00189902848238 },
        { lat: 10.200936286007142, lng: 105.40688534827954 },
      ];

     
    const flightPath = new google.maps.Polyline({
        path: flightPlanCoordinates,
        geodesic: true,
        strokeColor: "#FF0000",
        strokeOpacity: 1.0,
        strokeWeight: 2,
      });
    
      flightPath.setMap(map);
}
function initMap() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap'
    };
                    
    // Display a map on the web page
    map = new google.maps.Map(document.getElementById("map-info"), mapOptions);
    map.setTilt(50);
        
    // Multiple markers location, latitude, and longitude
    var markers = [
        ['Hà Nội', 21.0205755122339, 105.83601941504449],
        ['Huế', 16.472813136755555, 107.5625213780196],
        ['Đà Nẵng', 16.127839294572674, 108.00189902848238],
        ['Cần Thơ', 10.200936286007142, 105.40688534827954]
    ];
                        
    // Info window content
    var infoWindowContent = [
        ['<div class="info_content">' +
        '<h2>Brooklyn Museum</h2>' +
        '<h3>200 Eastern Pkwy, Brooklyn, NY 11238</h3>' +
        '<p>The Brooklyn Museum is an art museum located in the New York City borough of Brooklyn.</p>' + 
        '</div>'],
        ['<div class="info_content">' +
        '<h2>Central Library</h2>' +
        '<h3>10 Grand Army Plaza, Brooklyn, NY 11238</h3>' +
        '<p>The Central Library is the main branch of the Brooklyn Public Library, located at Flatbush Avenue.</p>' +
        '</div>'],
        ['<div class="info_content">' +
        '<h2>Prospect Park Zoo</h2>' +
        '<h3>450 Flatbush Ave, Brooklyn, NY 11225</h3>' +
        '<p>The Prospect Park Zoo is a 12-acre zoo located off Flatbush Avenue on the eastern side of Prospect Park, Brooklyn, New York City.</p>' +
        '</div>'],
        ['<div class="info_content">' +
        '<h2>Barclays Center</h2>' +
        '<h3>620 Atlantic Ave, Brooklyn, NY 11217</h3>' +
        '<p>Barclays Center is a multi-purpose indoor arena in the New York City borough of Brooklyn.</p>' +
        '</div>']
    ];
        
    // Add multiple markers to map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    // Place each marker on the map  
    for( i = 0; i < markers.length; i++ ) {
        console.log(i);
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            label: `${i+1}`,
            title: markers[i][0]
        });
        
        // Add info window to marker    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        google.maps.event.addDomListener(document.querySelector(`.btn-${i}`),'click', (function(marker, i) {
            setLineOfMap(map)
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));
        // Center the map to fit all markers on the screen
        map.fitBounds(bounds);
    }
    setLineOfMap(map)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(5);
        google.maps.event.removeListener(boundsListener);
    });
}
initMap()
window.initMap = initMap;